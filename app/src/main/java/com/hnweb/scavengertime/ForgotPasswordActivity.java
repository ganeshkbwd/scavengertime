package com.hnweb.scavengertime;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    EditText emailET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        emailET = (EditText) findViewById(R.id.fp_email_idET);


    }


    public void callForgotPasswordWebService(final String email) {

        Constants.showProgress(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.forgotPassword,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            if (Constants.MESSAGE_CODE == 1) {
                                Constants.MESSAGE = jobj.getString("message");
                                forgotPasswordDialog(Constants.MESSAGE);
//                                Toast.makeText(ForgotPasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();


                            } else {

                                Constants.progressDialog.dismiss();
                                Constants.toastDialog(ForgotPasswordActivity.this, "Your entered email id not registered.");
//                                Toast.makeText(ForgotPasswordActivity.this, "Your entered email id not registered.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(ForgotPasswordActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(ForgotPasswordActivity.this, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email_address", email);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void forgotPasswordDialog(String MESSAGE) {
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.toast_dialog
                , null));
        settingsDialog.setCancelable(true);
        final TextView fppassTV = (TextView) settingsDialog.findViewById(R.id.msgTV);
        fppassTV.setText(MESSAGE);

        TextView submitBTN = (TextView) settingsDialog.findViewById(R.id.okTV);
        submitBTN.setText("OK");
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailET.setText("");
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fpsubmitBTN:
                String email = emailET.getText().toString().trim();
                if (Validations.emailCheck(email)) {
                    if (CheckConnectivity.checkInternetConnection(ForgotPasswordActivity.this)) {
                        callForgotPasswordWebService(email);

                    }
                } else {
                    emailET.setError(getResources().getString(R.string.plz_email));
                }
                break;
        }
    }
}
