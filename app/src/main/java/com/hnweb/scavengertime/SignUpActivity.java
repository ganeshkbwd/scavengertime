package com.hnweb.scavengertime;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;
import com.hnweb.scavengertime.hostsActivities.DashboardActivity;
import com.hnweb.scavengertime.playersActivities.JoinHuntActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    EditText fullNameET, emailIdET, passwordET, usernameET, confirmPasswordET;
    String fullName, emailId, password, username, confirmPassword;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    private CallbackManager mCallbackManager;
    private Profile profile;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    SharedPreferences sharedPreferences;
    public GoogleApiClient mGoogleApiClient;
    private SignInButton btnSignIn;
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_up);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        init();


    }

    public void init() {
        fullNameET = (EditText) findViewById(R.id.fullNameET);
        emailIdET = (EditText) findViewById(R.id.emailIdET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        usernameET = (EditText) findViewById(R.id.usernameET);
        confirmPasswordET = (EditText) findViewById(R.id.confirmPasswordET);
        btnSignIn = (SignInButton) findViewById(R.id.gpBTN);
//        btnSignIn.setOnClickListener(this);

//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
//
//        // Customizing G+ button
//        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
//        btnSignIn.setScopes(gso.getScopeArray());
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.signUpBTN:
                if (checkValidation()) {
                    if (CheckConnectivity.checkInternetConnection(this)) {
                        //callWebService
                        callSignUpWebservice(fullName, emailId, username, password);
                    }
                }

                break;
            case R.id.fbBTN:
//                String image_path = "";
//                File file = new File(image_path);
//                Uri uri = Uri.fromFile(file);
//                Uri uri = Uri.parse("android.resource://com.hnweb.scavengertime/drawable/logo_icon");
//                Intent in = new Intent(Intent.ACTION_SEND);
//                in.setType("image/*");
//                in.putExtra(Intent.EXTRA_STREAM, uri);
//                startActivity(in);
                if (CheckConnectivity.checkInternetConnection(this)) {
                    //callWebService
                    fbadd();
//                    Constants.underDevelopment(this);
                }
                break;
            case R.id.gpBTN:
                if (CheckConnectivity.checkInternetConnection(this)) {
                    //callWebService
//                    signIn();
                    Constants.underDevelopment(this);
                }
                break;
            case R.id.signInBTN:
                intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public boolean checkValidation() {
        getUserInputs();
        if (Validations.strslength(fullName)) {
            if (Validations.emailCheck(emailId)) {
                if (Validations.strslength(username)) {
                    if (Validations.strslength(password)) {
                        if (Validations.confirmPass(password, confirmPassword)) {
                            return true;
                        } else {
                            confirmPasswordET.setError(getResources().getString(R.string.plz_confirmpass));
                            return false;
                        }
                    } else {
                        passwordET.setError(getResources().getString(R.string.plz_pass));
                        return false;
                    }
                } else {
                    usernameET.setError(getResources().getString(R.string.plz_username));
                    return false;
                }
            } else {
                emailIdET.setError(getResources().getString(R.string.plz_email));
                return false;
            }
        } else {
            fullNameET.setError(getResources().getString(R.string.plz_name));
            return false;
        }

    }

    public void getUserInputs() {
        fullName = fullNameET.getText().toString().trim();
        emailId = emailIdET.getText().toString().trim();
        username = usernameET.getText().toString().trim();
        password = passwordET.getText().toString().trim();
        confirmPassword = confirmPasswordET.getText().toString().trim();
    }


    public void callSignUpWebservice(final String name, final String email, final String username, final String pass) {

        Constants.showProgress(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.signUpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");
                            if (Constants.MESSAGE_CODE == 1) {


//                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
                                toastDialog(Constants.MESSAGE);


                            } else {

                                Constants.progressDialog.dismiss();//
                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(SignUpActivity.this, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("full_name", name);
                params.put("email_address", email);
                params.put("username", username);
                params.put("password", pass);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void toastDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                        Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                        startActivity(intent);
                        finish();
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void toastDialog1(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
                            Constants.OK = 0;
                            Intent intent = new Intent(SignUpActivity.this, DashboardActivity.class);
//                                    intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        } else {
                            Constants.OK = 0;
                            Intent intent = new Intent(SignUpActivity.this, JoinHuntActivity.class);
//                                    intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /////////// Facebook //////////////


    public void fbadd() {
        //        callbackManager = CallbackManager.Factory.create();
        /////////////////////////////////////////
        keyHash();
        mCallbackManager = CallbackManager.Factory.create();
        //   LoginManager.getInstance().logOut();
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                //  textView.setText(displayMessage(profile1));
                // System.out.println(displayMessage(profile1));
            }
        };

        tokenTracker.startTracking();
        profileTracker.startTracking();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_friends"));
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(mCallbackManager, mFacebookCallback);
//        LoginManager.getInstance().ppublish(feed, true, onPublishListener);
        //////////////////////////////////////////////
    }

    public void keyHash() {
        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("com.hnweb.scavengertime", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            profile = Profile.getCurrentProfile();


            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.v("LoginActivity Response ", response.toString());
                            System.out.println("arshres" + response.toString());

                            try {
                                String id = object.getString("id");
                                String Name = object.getString("name");

                                String FEmail = object.getString("email");
                                Log.v("Email = ", id + "  " + FEmail + "  " + Name);
                                // Toast.makeText(getApplicationContext(), "Name " + Name+FEmail, Toast.LENGTH_LONG).show();
                                if (CheckConnectivity.checkInternetConnection(SignUpActivity.this)) {
//                                sharePhotoToFacebook();
                                    fbSignInWebservice(id, Name, FEmail);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();


            //textView.setText(displayMessage(profile));
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };


    private void sharePhotoToFacebook() {

        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("Give me my codez or I will ... you know, do that thing you don't like!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

//        ShareApi.share(content, null);
        ShareDialog.show(this, content);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 140) {
////            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
//        } else {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

//        }


    }

    @Override
    public void onResume() {
        super.onResume();
//        Profile profile = Profile.getCurrentProfile();
        //   textView.setText(displayMessage(profile));
    }

    @Override
    public void onStop() {

        try {
            super.onStop();
            profileTracker.stopTracking();
            tokenTracker.stopTracking();
        } catch (Exception e) {
//            e.printStackTrace();
        }

    }


    public void fbSignInWebservice(final String id, final String name, final String email) {

        Constants.showProgress(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/scavengertime/api/login_with_facebook_credentials.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");
                            if (Constants.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putInt("login", 1);
                                editor.putInt("user_id", user_id);
                                editor.putInt("logWith", 1);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("user_type", Constants.USER_TYPE);
                                editor.commit();

//                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
//                                if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
//                                    Constants.OK = 0;
//                                    Intent intent = new Intent(SignInActivity.this, DashboardActivity.class);
////                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                    finish();
//                                } else {
//                                    Constants.OK = 0;
//                                    Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
////                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                    finish();
//                                }
                                toastDialog1(Constants.MESSAGE);


                            } else {

                                Constants.progressDialog.dismiss();//
                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(SignUpActivity.this, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("facebook_user_id", id);
                params.put("email_address", email);
                params.put("full_name", name);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    ///////// GooglePlus

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.e("Status", String.valueOf(status));
//                        updateUI(false);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
//                        updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getId());

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            String accountId = String.valueOf(acct.getId());

            gpSignInWebservice(accountId, personName, email);

            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

//            txtName.setText(personName);
//            txtEmail.setText(email);
//            Glide.with(getApplicationContext()).load(personPhotoUrl)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(imgProfilePic);

//            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            Log.d(TAG, "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
////            showProgressDialog();
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
////                    hideProgressDialog();
//                    handleSignInResult(googleSignInResult);
//                }
//            });
//        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }


    public void gpSignInWebservice(final String id, final String name, final String email) {

        Constants.showProgress(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/scavengertime/api/login_with_google_credentials.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");
                            if (Constants.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putInt("login", 1);
                                editor.putInt("user_id", user_id);
                                editor.putInt("logWith", 2);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("user_type", Constants.USER_TYPE);
                                editor.commit();

//                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
//                                if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
//                                    Constants.OK = 0;
//                                    Intent intent = new Intent(SignInActivity.this, DashboardActivity.class);
////                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                    finish();
//                                } else {
//                                    Constants.OK = 0;
//                                    Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
////                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                    finish();
//                                }
                                toastDialog1(Constants.MESSAGE);


                            } else {

                                Constants.progressDialog.dismiss();//
                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(SignUpActivity.this, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("google_id", id);
                params.put("email_address", email);
                params.put("full_name", name);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
