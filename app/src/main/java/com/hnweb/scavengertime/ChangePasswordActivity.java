package com.hnweb.scavengertime;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    EditText oldPassET, newPassET, confirmPassET;
    String oldPass, newPass, confirmPass;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        setToolbarDrawer();
        init();
    }

    public void init() {
        oldPassET = (EditText) findViewById(R.id.oldPassET);
        newPassET = (EditText) findViewById(R.id.newPassET);
        confirmPassET = (EditText) findViewById(R.id.reenterPassET);
    }

    public void getUserInput() {
        oldPass = oldPassET.getText().toString().trim();
        newPass = newPassET.getText().toString().trim();
        confirmPass = confirmPassET.getText().toString().trim();
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");
//
//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();
//
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changePassBTN:
                getUserInput();
                if (Validations.strslength(oldPass)) {
                    if (Validations.strslength(newPass)) {
                        if (Validations.confirmPass(newPass, confirmPass)) {
                            if (CheckConnectivity.checkInternetConnection(this))
                                changePassWebservice(oldPass, newPass, confirmPass);
                        } else {
                            confirmPassET.setError("New Password and Confirm new password not same.");
                        }
                    } else {
                        newPassET.setError("Please enter New Password.");
                    }
                }else {
                    oldPassET.setError("Please enter old password.");
                }
                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.logoutBTN:
                Constants.logout_fun(this);
                break;

        }
    }

    public void changePassWebservice(final String oldPass, final String newPass, final String confirmPass) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.changePassword,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

//                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
                                Toast.makeText(ChangePasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ChangePasswordActivity.this,MyProfileActivity.class);
                                startActivity(intent);
                                finish();
                                Constants.progressDialog.dismiss();


                            } else {

                                Constants.progressDialog.dismiss();//
                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
//                                Toast.makeText(ChangePasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(ChangePasswordActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(ChangePasswordActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", String.valueOf(user_id));
                params.put("old_password", oldPass);
                params.put("new_password", newPass);
                params.put("new_cpassword", confirmPass);


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
