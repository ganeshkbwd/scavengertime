package com.hnweb.scavengertime;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.hnweb.scavengertime.adapter.ActiveHuntsAdapter;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.hostsActivities.CreateHuntsActivity;
import com.hnweb.scavengertime.hostsActivities.DashboardActivity;
import com.hnweb.scavengertime.playersActivities.JoinHuntActivity;
import com.hnweb.scavengertime.pojo.ActiveHunts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class MyHunts_Activity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    TextView activeTV, pastTV, upcomingTV, noRecordTV;
    RadioGroup huntsRG;
    RadioButton hostedHuntBTN, joinedHuntBTN;
    int service = 0;

    ArrayList<ActiveHunts> actList = new ArrayList<ActiveHunts>();
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String user_type;
    RecyclerView recyclerView;
    ActiveHuntsAdapter adapter;
    ImageView fab;
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView headingTV;
    private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_hunts_);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
        setToolbarDrawer();
        init();
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        if (user_type.equalsIgnoreCase("PLAYER")) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
            settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
            final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
            final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);

            drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }

            };
            drawerLayout.setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }


    }

    public void init() {
        headingTV = (TextView) findViewById(R.id.headingTV);
        activeTV = (TextView) findViewById(R.id.activeTV);
        activeTV.setBackgroundColor(Color.RED);
        upcomingTV = (TextView) findViewById(R.id.upcomingTV);
        pastTV = (TextView) findViewById(R.id.pastTV);
        hostedHuntBTN = (RadioButton) findViewById(R.id.hostedHuntBTN);
        joinedHuntBTN = (RadioButton) findViewById(R.id.joinedHuntBTN);
        huntsRG = (RadioGroup) findViewById(R.id.huntRG);
        huntsRG.setOnCheckedChangeListener(this);
        noRecordTV = (TextView) findViewById(R.id.noRecordTV);
        fab = (ImageView) findViewById(R.id.fab);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyHunts_Activity.this));
        if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Invite")) {
            headingTV.setText("Invite friends");
            service = 1;
            Constants.SELECTED = "UpComing";
            if (hostedHuntBTN.isChecked()) {
                callWebservice();
            } else {
                hostedHuntBTN.setChecked(true);
                joinedHuntBTN.setChecked(false);
            }

            activeTV.setBackgroundColor(Color.WHITE);
            activeTV.setTextColor(Color.BLACK);
            upcomingTV.setBackgroundColor(Color.RED);
            upcomingTV.setTextColor(Color.WHITE);
            pastTV.setBackgroundColor(Color.WHITE);
            pastTV.setTextColor(Color.BLACK);
            fab.setVisibility(View.VISIBLE);

//            callWebservice();
//            if (CheckConnectivity.checkInternetConnection(this))
//                huntsWebservice(Urls.myJoinedHunts, Constants.SELECTED, service);
        } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Manage")) {
            headingTV.setText("Manage hunts");
            service = 1;
            Constants.SELECTED = "UpComing";
            if (hostedHuntBTN.isChecked()) {
                callWebservice();
            } else {
                hostedHuntBTN.setChecked(true);
                joinedHuntBTN.setChecked(false);
            }

            activeTV.setBackgroundColor(Color.WHITE);
            activeTV.setTextColor(Color.BLACK);
            upcomingTV.setBackgroundColor(Color.RED);
            upcomingTV.setTextColor(Color.WHITE);
            pastTV.setBackgroundColor(Color.WHITE);
            pastTV.setTextColor(Color.BLACK);
            fab.setVisibility(View.VISIBLE);

//            callWebservice();
//            if (CheckConnectivity.checkInternetConnection(this))
//                huntsWebservice(Urls.myHostedHunts, Constants.SELECTED, service);
        } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("")) {
            headingTV.setText("My hunts");
            if (user_type.equalsIgnoreCase("HOST")) {
                hostedHuntBTN.setChecked(true);
                joinedHuntBTN.setChecked(false);
                fab.setVisibility(View.VISIBLE);
                if (Constants.SELECTED.equalsIgnoreCase("UpComing")) {
                    service = 0;
//                    hostedHuntBTN.setChecked(true);
//                    joinedHuntBTN.setChecked(false);
                    activeTV.setBackgroundColor(Color.WHITE);
                    activeTV.setTextColor(Color.BLACK);
                    upcomingTV.setBackgroundColor(Color.RED);
                    upcomingTV.setTextColor(Color.WHITE);
                    pastTV.setBackgroundColor(Color.WHITE);
                    pastTV.setTextColor(Color.BLACK);
                    fab.setVisibility(View.VISIBLE);
                }
//                callWebservice();
                if (CheckConnectivity.checkInternetConnection(this))
                    huntsWebservice(Urls.myHostedHunts, Constants.SELECTED, service, MyHunts_Activity.this);
            } else if (user_type.equalsIgnoreCase("PLAYER")) {


                if (joinedHuntBTN.isChecked()) {
                    callWebservice();
                } else {
                    joinedHuntBTN.setChecked(true);
                    hostedHuntBTN.setChecked(false);
                }

                fab.setVisibility(View.GONE);
//                if (Constants.SELECTED.equalsIgnoreCase("UpComing")) {
//                    service = 1;
////                    hostedHuntBTN.setChecked(true);
////                    joinedHuntBTN.setChecked(false);
//                    activeTV.setBackgroundColor(Color.WHITE);
//                    activeTV.setTextColor(Color.BLACK);
//                    upcomingTV.setBackgroundColor(Color.RED);
//                    upcomingTV.setTextColor(Color.WHITE);
//                    pastTV.setBackgroundColor(Color.WHITE);
//                    pastTV.setTextColor(Color.BLACK);
//                    fab.setVisibility(View.VISIBLE);
//                }
//                if (CheckConnectivity.checkInternetConnection(this))
//                    huntsWebservice(Urls.joinHunt, Constants.SELECTED, service);
//                callWebservice();
            }

        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activeTV:
                activeTV.setBackgroundColor(Color.RED);
                activeTV.setTextColor(Color.WHITE);
                upcomingTV.setBackgroundColor(Color.WHITE);
                upcomingTV.setTextColor(Color.BLACK);
                pastTV.setBackgroundColor(Color.WHITE);
                pastTV.setTextColor(Color.BLACK);
                Constants.SELECTED = "Active";
                callWebservice();
                break;
            case R.id.upcomingTV:
                activeTV.setBackgroundColor(Color.WHITE);
                activeTV.setTextColor(Color.BLACK);
                upcomingTV.setBackgroundColor(Color.RED);
                upcomingTV.setTextColor(Color.WHITE);
                pastTV.setBackgroundColor(Color.WHITE);
                pastTV.setTextColor(Color.BLACK);
                Constants.SELECTED = "UpComing";
                callWebservice();

                break;
            case R.id.pastTV:
                activeTV.setBackgroundColor(Color.WHITE);
                activeTV.setTextColor(Color.BLACK);
                upcomingTV.setBackgroundColor(Color.WHITE);
                upcomingTV.setTextColor(Color.BLACK);
                pastTV.setBackgroundColor(Color.RED);
                pastTV.setTextColor(Color.WHITE);
                Constants.SELECTED = "Past";
                callWebservice();

                break;
            case R.id.fab:
                Intent intent = new Intent(MyHunts_Activity.this, CreateHuntsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }


    public void callWebservice() {
        if (hostedHuntBTN.isChecked()) {
            if (CheckConnectivity.checkInternetConnection(this))
                huntsWebservice(Urls.myHostedHunts, Constants.SELECTED, service, MyHunts_Activity.this);
//            Toast.makeText(this, "HOSTED " + service, Toast.LENGTH_SHORT).show();
        } else if (joinedHuntBTN.isChecked()) {
            if (CheckConnectivity.checkInternetConnection(this))
                huntsWebservice(Urls.myJoinedHunts, Constants.SELECTED, service, MyHunts_Activity.this);
//            Toast.makeText(this, "JOINED " + service, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.hostedHuntBTN:
                service = 0;
                if (CheckConnectivity.checkInternetConnection(this))
                    huntsWebservice(Urls.myHostedHunts, Constants.SELECTED, service, MyHunts_Activity.this);
                break;

            case R.id.joinedHuntBTN:
                service = 1;
                if (CheckConnectivity.checkInternetConnection(this))
                    huntsWebservice(Urls.myJoinedHunts, Constants.SELECTED, service, MyHunts_Activity.this);
                break;
        }
    }


    public void huntsWebservice(String urlHunts, final String status, final int service, final Activity context) {

        Constants.showProgress(context);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlHunts,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");


                            if (Constants.MESSAGE_CODE == 1) {
                                JSONArray jarr = jobj.getJSONArray("response");
//                                if (actList.size() != 0) {
                                actList.clear();

//                                    adapter.notifyDataSetChanged();

//                                }
                                for (int i = 0; i < jarr.length(); i++) {
                                    ActiveHunts act = new ActiveHunts();
                                    act.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
                                    act.setHunt_title(jarr.getJSONObject(i).getString("hunt_title"));
                                    act.setHunt_description(jarr.getJSONObject(i).getString("hunt_description"));
                                    act.setIs_timed_hunt(jarr.getJSONObject(i).getString("is_timed_hunt"));
                                    act.setStart_date_time(jarr.getJSONObject(i).getString("start_date_time"));
                                    act.setEnd_date_time(jarr.getJSONObject(i).getString("end_date_time"));

                                    if (service == 1) {
                                        if (status.equalsIgnoreCase("Active")) {
                                            act.setIs_finished(jarr.getJSONObject(i).getString("is_finished"));
                                        } else if (status.equalsIgnoreCase("Past")) {
                                            act.setIs_winner_declared(jarr.getJSONObject(i).getString("is_winner_declared"));
                                        } else {

                                        }

//                                        act.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
                                    } else if (service == 0) {

                                        act.setCurrent_status(jarr.getJSONObject(i).getString("current_status"));
                                        act.setFirst_prize_text(jarr.getJSONObject(i).getString("first_prize_text"));
                                        act.setFirst_prize_upload_file_path(jarr.getJSONObject(i).getString("first_prize_upload_file_path"));
                                        act.setSecond_prize_text(jarr.getJSONObject(i).getString("second_prize_text"));
                                        act.setSecond_prize_upload_file_path(jarr.getJSONObject(i).getString("second_prize_upload_file_path"));
                                        act.setThird_prize_text(jarr.getJSONObject(i).getString("third_prize_text"));
                                        act.setThird_prize_upload_file_path(jarr.getJSONObject(i).getString("third_prize_upload_file_path"));
                                        act.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                        act.setHunt_is_ready(jarr.getJSONObject(i).getString("hunt_is_ready"));
                                        act.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
                                        act.setUser_id(jarr.getJSONObject(i).getString("user_id"));
                                        act.setPrebuilt_hunt_id(jarr.getJSONObject(i).getString("prebuilt_hunt_id"));
                                        act.setIs_winner_declared(jarr.getJSONObject(i).getString("is_winner_declared"));
                                    }


                                    actList.add(act);
//                                    Collections.sort(actList, new Comparator<ActiveHunts>() {
//                                        @Override
//                                        public int compare(ActiveHunts lhs, ActiveHunts rhs) {
//
//                                            return lhs.getHunt_title().compareTo(rhs.getHunt_title());
//                                        }
//
//
//                                    });
//                                    for (int l = 0; l < actList.size(); l++) {
//                                        Log.e("SORTED LIST", actList.get(i).getHunt_title().toString());
//                                    }


                                }
                                if (actList.size() == 0) {
                                    recyclerView.setVisibility(View.GONE);
                                    noRecordTV.setVisibility(View.VISIBLE);
                                } else {
                                    noRecordTV.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    adapter = new ActiveHuntsAdapter(MyHunts_Activity.this, actList, user_type, status, service, user_id);
                                    recyclerView.setAdapter(adapter);
                                }


                            } else {
                                actList.clear();
                                recyclerView.setVisibility(View.GONE);
                                noRecordTV.setVisibility(View.VISIBLE);
                                Constants.MESSAGE = jobj.getString("message");

                            }
                            Constants.dismissProgressDialog(context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.dismissProgressDialog(context);
                Constants.toastDialog((Activity) context, "Please Check Internet Connection..!!!");
//                Toast.makeText(MyHunts_Activity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("filter_by", status);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);


    }


    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        //Field to fill with the selected phone number
//        final EditText textPhone = (EditText) findViewById(R.id.Phone);
        if (reqCode == 200) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    //Get the phone number id from the Uri
                    String id = uri.getLastPathSegment();
                    //Query the phone numbers for the selected phone number id
                    Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone._ID + "=?", new String[]{id}, null);
                    int phoneIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    //get the only phone number
                    if (c.moveToFirst()) {
                        String phone_no = c.getString(phoneIdx);
                        Log.e("phone", c.getString(phoneIdx));
                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        sendIntent.putExtra("sms_body", "Please join the " + Constants.HUNT_TITLE_SHARE + ".Your Code is " + Constants.HUNT_CODE_SHARE);
                        sendIntent.putExtra("address", phone_no);
                        sendIntent.setType("vnd.android-dir/mms-sms");
                        startActivityForResult(sendIntent, 300);

                    } else {
                        //no result
                        Toast noResultFound = Toast.makeText(MyHunts_Activity.this, "No phone number found", Toast.LENGTH_SHORT);
                        noResultFound.show();
                    }
                    c.close();
                }

            }

        } else if (reqCode == 300) {
            Constants.HUNT_TYPE = 0;
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }


    }

    public void onDrawerClick(View v) {
//        if (v.getId()==R.id.hometBTN){
//        Constants.drawerClick(this, v.getId());
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        Intent intent;
        switch (v.getId()) {
            case R.id.hometBTN:
                if (user_type.equalsIgnoreCase("Host")) {
                    intent = new Intent(this, DashboardActivity.class);
                    startActivity(intent);
                    finish();
                } else if (user_type.equalsIgnoreCase("Player")) {
                    intent = new Intent(this, MyHunts_Activity.class);
                    startActivity(intent);
                    finish();
                }


                break;
            case R.id.inviteBTN:
                intent = new Intent(this, MyHunts_Activity.class);
                intent.putExtra("ComeFrom", "Invite");
                startActivity(intent);
                finish();
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                intent = new Intent(this, MyProfileActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.logoutBTN:
                if (sharedPreferences.getInt("logWith", 0) == 1) {
                    signOut();
                } else if (sharedPreferences.getInt("logWith", 0) == 2) {
                    LoginManager.getInstance().logOut();
                }

                Constants.logout_fun(this);
                break;
            case R.id.editBTN:
                intent = new Intent(this, EditProfileActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nullRL:
//no action
                break;
            case R.id.proRL:
                //no action
                break;
        }
//        }else {
//            Toast.makeText(this,"You are already on home.",Toast.LENGTH_SHORT).show();
//        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent;
        if (user_type.equalsIgnoreCase("HOST")) {
            intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
        } else {
            intent = new Intent(this, JoinHuntActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.e("Status", String.valueOf(status));
//                        updateUI(false);
                    }
                });
    }

}
