package com.hnweb.scavengertime;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.PickerUtils;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    ImageView myProfileIV;
    EditText myNameTV, phoneTV, emailTV;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private static final int FROM_GALLARY = 103;
    private static final int REQUEST_CAMERA = 5;
    String encodedImage1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        setToolbarDrawer();
        init();

        String sourceDate = "2016-12-12";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = format.parse(sourceDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        myDate = addDays(myDate, 3);

        Log.e("DATEEEE : ", String.valueOf(format.format(myDate)));
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    public Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }


    public void init() {
        myProfileIV = (ImageView) findViewById(R.id.myProfileIV);
        myNameTV = (EditText) findViewById(R.id.myNameET);
        phoneTV = (EditText) findViewById(R.id.phoneET);
        emailTV = (EditText) findViewById(R.id.emailET);

        if (Validations.strslength(Constants.myProfile)) {
            Glide.with(this).load(Constants.myProfile).into(myProfileIV);
        }
        myNameTV.setText(Constants.myName);
        phoneTV.setText(Constants.phone);
        emailTV.setText(Constants.email);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changePassBTN:
                Intent intent = new Intent(this, ChangePasswordActivity.class);
                startActivity(intent);

                break;
            case R.id.myProfileIV:
                PickerUtils.selectImageDialog(this, REQUEST_CAMERA, FROM_GALLARY);
                break;
            case R.id.updateProfileBTN:
                if (Validations.strslength(myNameTV.getText().toString().trim())) {
                    if (Validations.emailCheck(emailTV.getText().toString().trim())) {
//                        if (Validations.strslength(phoneTV.getText().toString().trim())) {
                        Log.e("IMAGEP", encodedImage1.trim());
                        editProfileWebservice(myNameTV.getText().toString().trim(), emailTV.getText().toString().trim(), phoneTV.getText().toString().trim(), encodedImage1.trim());
//                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FROM_GALLARY) {

                System.out.println("SELECT_IMAGE");

                Uri uri = data.getData();

                encodedImage1 = PickerUtils.gallaryPath(EditProfileActivity.this, uri);
                Glide.with(this).load(new File(encodedImage1)).into(myProfileIV);

            } else if (requestCode == REQUEST_CAMERA) {

                System.out.println("REQUEST_CAMERA");

                encodedImage1 = PickerUtils.cameraPath();
                Glide.with(this).load(new File(encodedImage1)).into(myProfileIV);

            }

        }
    }

    public void onDrawerClick(View v) {
        Constants.drawerClick(this, v.getId());
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(EditProfileActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    public void editProfileWebservice(final String myName, final String email, final String phone, final String profile) {

        Constants.showProgress(this);

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, Urls.editProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {


                                Toast.makeText(EditProfileActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
                                Intent intent = new Intent(EditProfileActivity.this, MyProfileActivity.class);
                                startActivity(intent);
                                finish();


                            } else {

                                Constants.progressDialog.dismiss();//
                                Constants.toastDialog(EditProfileActivity.this, Constants.MESSAGE);
//                                Toast.makeText(EditProfileActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.dismissProgressDialog(EditProfileActivity.this);
                Constants.toastDialog(EditProfileActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(EditProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        });

        smr.addStringParam("user_id", String.valueOf(user_id));
        smr.addStringParam("full_name", myName);
        smr.addStringParam("email_address", email);
        smr.addStringParam("phone_number", phone);
        smr.addFile("profile_photo", profile);
//        {
//            @Override
//            protected Map<String, String> getParams() {
//                //Converting Bitmap to String
//
//                Map<String, String> params = new Hashtable<String, String>();
//
//                params.put("user_id", String.valueOf(user_id));
//                params.put("full_name", myName);
//                params.put("email_address", email);
//                params.put("phone_number", phone);
//                params.put("profile_photo", profile);
//
//                Log.e("PARAMS", params.toString());
//                //returning parameters
//                return params;
//            }
//        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(smr);


    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "EditProfile Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.hnweb.scavengertime/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "EditProfile Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.hnweb.scavengertime/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
