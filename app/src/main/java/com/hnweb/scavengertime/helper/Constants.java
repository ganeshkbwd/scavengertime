package com.hnweb.scavengertime.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.hnweb.scavengertime.EditProfileActivity;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.MyProfileActivity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.SplashActivity;
import com.hnweb.scavengertime.hostsActivities.DashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by neha on 10/21/2016.
 */
public class Constants {

    public static int MESSAGE_CODE;
    public static ProgressDialog progressDialog;
    public static String MESSAGE;
    public static String RESPONSE;
    public static DrawerLayout drawerLayout;
    public static ActionBarDrawerToggle drawerToggle;
    public static LinearLayout settingDrawerLL;
    public static Toolbar toolbar;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    public static String myProfile, myName, phone, email;
    public static int HUNT_TYPE;
    public static String HUNT_CODE_SHARE;
    public static String HUNT_TITLE_SHARE;
    public static String USER_TYPE;
    public static boolean SMS = false;
    public static String SELECTED = "Active";
    public static String user_id_, full_name, email_address, phone_number, profile_photo, balance_hunts_count;

    public static Integer[] rankArr = new Integer[]{1, 2, 3};
    public static Integer[] selectedRankArr = new Integer[]{};
    public static ArrayList<Integer> myRanks;//= new ArrayList<Integer>(Arrays.asList(Constants.rankArr));
    public static int OK = 0;

    // Camera activity request codes
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
    public static Uri fileUri;


//    public static void addRanks() {
//        rankArr.add(1);
//        rankArr.add(2);
//        rankArr.add(3);
//    }

    public static void showProgress(Context context) {
        progressDialog = new ProgressDialog(context);

        progressDialog.setMessage(context.getResources().getString(R.string.wait));
        progressDialog.show();
//        progressDialog.setCancelable(false);
        timer();


    }

    public static void dismissProgressDialog(Context context) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void underDevelopment(Context context) {
        Toast.makeText(context, "Under Development...", Toast.LENGTH_SHORT).show();
    }


    public static void logout_fun(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(context,
                        SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                toastDialog(context, "You are logout successfully");
                Toast.makeText(context,
                        "You are logout successfully", Toast.LENGTH_LONG).show();
                SharedPreferences settings = context.getApplicationContext()
                        .getSharedPreferences(MyPREFERENCES,
                                Context.MODE_PRIVATE);
                settings.edit().clear().commit();


                SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
                if (sharedPreferences.getInt("logWith", 0) == 1) {
//                    if (((SignUpActivity)context).mGoogleApiClient.isConnected()) {
//                        Plus.AccountApi.clearDefaultAccount(((SignUpActivity)context).mGoogleApiClient);
//                        ((SignUpActivity)context).mGoogleApiClient.disconnect();
//                        ((SignUpActivity)context).mGoogleApiClient.connect();
//                    }

//                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                            new ResultCallback<Status>() {
//                                @Override
//                                public void onResult(Status status) {
////                        updateUI(false);
//                                }
//                            });
                } else if (sharedPreferences.getInt("logWith", 0) == 2) {
                    LoginManager.getInstance().logOut();
                } else {

                }
                context.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }

    public static void timer() {
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                progressDialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 5000); // after 2 second (or 2000 miliseconds), the task will be active.
    }

    public static void upgradeDialog(final Activity context) {
        final Dialog settingsDialog = new Dialog(context);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setCancelable(false);
        settingsDialog.setContentView(context.getLayoutInflater().inflate(R.layout.upgrade_dialog
                , null));
        settingsDialog.setCancelable(true);
        final Button upgradeBTN = (Button) settingsDialog.findViewById(R.id.upgradeBTN);
        Button remindBTN = (Button) settingsDialog.findViewById(R.id.remindBTN);

        upgradeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                underDevelopment(context);
            }
        });
        remindBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });
        settingsDialog.show();
    }


    public static void drawerClick(Activity context, int id) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
//        int login = sharedPreferences.getInt("login", 0);
        String user_type = sharedPreferences.getString("user_type", null);
        Intent intent;
        switch (id) {
            case R.id.hometBTN:
                if (user_type.equalsIgnoreCase("Host")) {
                    intent = new Intent(context, DashboardActivity.class);
                    context.startActivity(intent);
                    context.finish();
                } else if (user_type.equalsIgnoreCase("Player")) {
                    intent = new Intent(context, MyHunts_Activity.class);
                    context.startActivity(intent);
                    context.finish();
                }


                break;
            case R.id.inviteBTN:
                intent = new Intent(context, MyHunts_Activity.class);
                intent.putExtra("ComeFrom", "Invite");
                context.startActivity(intent);
                context.finish();
                break;
            case R.id.noyifyBTN:
                underDevelopment(context);
                break;
            case R.id.myProfileBTN:
                intent = new Intent(context, MyProfileActivity.class);
                context.startActivity(intent);
                context.finish();
                break;
            case R.id.settingsBTN:
                underDevelopment(context);
                break;
            case R.id.logoutBTN:
                logout_fun(context);
                break;
            case R.id.editBTN:
                intent = new Intent(context, EditProfileActivity.class);
                context.startActivity(intent);
                context.finish();
                break;
            case R.id.nullRL:
//no action
                break;
            case R.id.proRL:
                //no action
                break;
        }

    }

//    public static void getSharedData(){
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
//        int login = sharedPreferences.getInt("login", 0);
//        int user_id = sharedPreferences.getInt("user_id", 0);
//    }


    public static void profileWebservice(final Activity context, final String userId, final ImageView iv, final TextView nameTV) {


        final SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Constants.showProgress(context);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.viewProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");


                            if (Constants.MESSAGE_CODE == 1) {

//                                user_id_ = jobj.getString("user_id");
                                myName = jobj.getString("full_name");
                                email = jobj.getString("email_address");
                                phone = jobj.getString("phone_number");
                                myProfile = jobj.getString("profile_photo");
                                balance_hunts_count = jobj.getString("balance_hunts_count");

                                if (!myProfile.equalsIgnoreCase(""))
                                    Glide.with(context).load(myProfile).transform(new RoundImageTransform(context)).into(iv);
                                nameTV.setText(myName);
//                                SharedPreferences.Editor editor = sharedPreferences.edit();.transform(new RoundImageTransform(context))


                            } else {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(context, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", userId);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public static String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static void toastDialog(Activity context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
