package com.hnweb.scavengertime.helper;

/**
 * Created by neha on 9/2/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.hnweb.scavengertime.R;


public class CheckConnectivity {


    public static boolean checkInternetConnection(Activity context) {

        ConnectivityManager con_manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (con_manager.getActiveNetworkInfo() != null
                && con_manager.getActiveNetworkInfo().isAvailable()
                && con_manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Toast.makeText(context, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}
