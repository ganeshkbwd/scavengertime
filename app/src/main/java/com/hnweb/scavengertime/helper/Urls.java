package com.hnweb.scavengertime.helper;

/**
 * Created by neha on 10/21/2016.
 */
public class Urls {

    public static String signUpUrl = "http://designer321.com/johnilbwd/scavengertime/api/register.php";
    public static String forgotPassword = "http://designer321.com/johnilbwd/scavengertime/api/forgotpassword.php";
    public static String loginUrl = "http://designer321.com/johnilbwd/scavengertime/api/login.php";

    public static String createHunt = "http://designer321.com/johnilbwd/scavengertime/api/create_hunt.php";
    public static String preBuildHuntList = "http://designer321.com/johnilbwd/scavengertime/api/list_prebuiltin_hunts.php";
    public static String getPreBuildHuntPurchaseDetails = "http://designer321.com/johnilbwd/scavengertime/api/prebuiltin_hunt_details_to_purchase.php";
    public static String addHuntItems = "http://designer321.com/johnilbwd/scavengertime/api/create_hunt_items.php";
    public static String myProfile = "http://designer321.com/johnilbwd/scavengertime/api/view_my_profile.php";
    public static String editProfile = "http://designer321.com/johnilbwd/scavengertime/api/edit_my_profile.php";
    public static String changePassword = "http://designer321.com/johnilbwd/scavengertime/api/change_password.php";
    public static String myHostedHunts = "http://designer321.com/johnilbwd/scavengertime/api/my_hosted_hunts.php";
    public static String myJoinedHunts = "http://designer321.com/johnilbwd/scavengertime/api/my_joined_hunts.php";
    public static String editHuntItem = "http://designer321.com/johnilbwd/scavengertime/api/edit_hunt_item.php";
    public static String huntItems = "http://designer321.com/johnilbwd/scavengertime/api/my_hunt_items.php";
    public static String joinHunt = "http://designer321.com/johnilbwd/scavengertime/api/join_a_hunt.php";
    public static String joinHuntDetails = "http://designer321.com/johnilbwd/scavengertime/api/join_hunt_details.php?";
    public static String viewProfile = "http://designer321.com/johnilbwd/scavengertime/api/view_my_profile.php";
    public static String deleteMyHuntItems = "http://designer321.com/johnilbwd/scavengertime/api/delete_hunt_item.php";

    //"user_id    hunt_id"
    public static String playerFinishHunt = "http://designer321.com/johnilbwd/scavengertime/api/finish_the_played_hunt.php";

    //"Data from POST    1. player_user_id    2. hunt_id    3. hunt_item_id    Data from Files(multipart/form-data)    4. media_file"
    public static String uploadMediaByPlayer = "http://designer321.com/johnilbwd/scavengertime/api/up_media_played_hunt_item.php";

    public static String takeChallenge = "http://designer321.com/johnilbwd/scavengertime/api/take_a_chellange.php";

    public static String participantsOfHunt = "http://designer321.com/johnilbwd/scavengertime/api/participants_of_hunt.php";

    public static String huntWinners = "http://designer321.com/johnilbwd/scavengertime/api/declare_winners_for_a_played_hunt.php";


    public static String winnersList = "http://designer321.com/johnilbwd/scavengertime/api/hunt_winners.php";

    public static String playerMedia = "http://designer321.com/johnilbwd/scavengertime/api/submitted_items_by_player_for_a_hunt.php";

    public static String endHuntAPI = "http://designer321.com/johnilbwd/scavengertime/api/end_the_without_time_hunt_by_host.php";

    public static String allHuntItems = "http://designer321.com/johnilbwd/scavengertime/api/list_of_all_hunts_items_of_host.php";

    public static String addTeam = "http://designer321.com/johnilbwd/scavengertime/api/add_edit_team_details.php";

    public static String getTeamDetails = "http://designer321.com/johnilbwd/scavengertime/api/get_team_details.php";

    public static String addTeamMember = "http://designer321.com/johnilbwd/scavengertime/api/add_team_member.php";

    public static String editMemberName = "http://designer321.com/johnilbwd/scavengertime/api/edit_team_member.php";
}
