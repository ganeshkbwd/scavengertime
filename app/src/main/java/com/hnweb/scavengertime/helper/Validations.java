package com.hnweb.scavengertime.helper;

import android.util.Patterns;

/**
 * Created by neha on 10/21/2016.
 */
public class Validations {

    public static boolean strslength(String word) {
        if (word.length() > 0)
            return true;
        else
            return false;
    }

    public static boolean emailCheck(String email) {
        if (strslength(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return true;
        else
            return false;
    }

    public static boolean confirmPass(String password, String confirmPass) {
        if (strslength(confirmPass) && password.matches(confirmPass))
            return true;
        else
            return false;
    }
}
