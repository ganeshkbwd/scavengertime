package com.hnweb.scavengertime.pojo;

import java.io.Serializable;

/**
 * Created by neha on 5/1/2017.
 */

public class TeamMembers implements Serializable {
    String team_member_id, team_id, createdby_userid, team_member_name;

    public String getTeam_member_id() {
        return team_member_id;
    }

    public void setTeam_member_id(String team_member_id) {
        this.team_member_id = team_member_id;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getCreatedby_userid() {
        return createdby_userid;
    }

    public void setCreatedby_userid(String createdby_userid) {
        this.createdby_userid = createdby_userid;
    }

    public String getTeam_member_name() {
        return team_member_name;
    }

    public void setTeam_member_name(String team_member_name) {
        this.team_member_name = team_member_name;
    }
}
