package com.hnweb.scavengertime.pojo;

import java.io.Serializable;

/**
 * Created by neha on 10/24/2016.
 */
public class PreBuildHuntList implements Serializable{

    int prebuilt_hunt_id;
    String hunt_title;
    String hunt_description;
    String price;
    String created_datetime;
    String hunt_code;

    public int getPrebuilt_hunt_id() {
        return prebuilt_hunt_id;
    }

    public void setPrebuilt_hunt_id(int prebuilt_hunt_id) {
        this.prebuilt_hunt_id = prebuilt_hunt_id;
    }

    public String getHunt_title() {
        return hunt_title;
    }

    public void setHunt_title(String hunt_title) {
        this.hunt_title = hunt_title;
    }

    public String getHunt_description() {
        return hunt_description;
    }

    public void setHunt_description(String hunt_description) {
        this.hunt_description = hunt_description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getHunt_code() {
        return hunt_code;
    }

    public void setHunt_code(String hunt_code) {
        this.hunt_code = hunt_code;
    }
}
