package com.hnweb.scavengertime.pojo;

import java.io.Serializable;

/**
 * Created by neha on 10/28/2016.
 */
public class HuntItems implements Serializable {
    int hunt_item_id, hunt_id;
    String title, description, point_value, uploaded_file_path, uploaded_file_type, created_datetime, price_of_hunt_item, prebuild_hunt_item_id;

    public int getHunt_item_id() {
        return hunt_item_id;
    }

    public void setHunt_item_id(int hunt_item_id) {
        this.hunt_item_id = hunt_item_id;
    }

    public int getHunt_id() {
        return hunt_id;
    }

    public void setHunt_id(int hunt_id) {
        this.hunt_id = hunt_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoint_value() {
        return point_value;
    }

    public void setPoint_value(String point_value) {
        this.point_value = point_value;
    }

    public String getUploaded_file_path() {
        return uploaded_file_path;
    }

    public void setUploaded_file_path(String uploaded_file_path) {
        this.uploaded_file_path = uploaded_file_path;
    }

    public String getUploaded_file_type() {
        return uploaded_file_type;
    }

    public void setUploaded_file_type(String uploaded_file_type) {
        this.uploaded_file_type = uploaded_file_type;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getPrice_of_hunt_item() {
        return price_of_hunt_item;
    }

    public void setPrice_of_hunt_item(String price_of_hunt_item) {
        this.price_of_hunt_item = price_of_hunt_item;
    }

    public String getPrebuild_hunt_item_id() {
        return prebuild_hunt_item_id;
    }

    public void setPrebuild_hunt_item_id(String prebuild_hunt_item_id) {
        this.prebuild_hunt_item_id = prebuild_hunt_item_id;
    }
}
