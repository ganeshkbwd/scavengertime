package com.hnweb.scavengertime.pojo;

import java.io.Serializable;

/**
 * Created by neha on 10/26/2016.
 */
public class PastHunts implements Serializable {

    int hunt_id;
    String hunt_title;
    String hunt_description;
    String is_timed_hunt;
    String start_date_time;
    String end_date_time;
    String current_status;
    String first_prize_text;
    String first_prize_upload_file_path;
    String second_prize_text;
    String second_prize_upload_file_path;
    String third_prize_text;
    String third_prize_upload_file_path;
    String created_datetime;
    String hunt_is_ready;
    String hunt_code;
    String user_id;
    String prebuilt_hunt_id;

    public int getHunt_id() {
        return hunt_id;
    }

    public void setHunt_id(int hunt_id) {
        this.hunt_id = hunt_id;
    }

    public String getHunt_title() {
        return hunt_title;
    }

    public void setHunt_title(String hunt_title) {
        this.hunt_title = hunt_title;
    }

    public String getHunt_description() {
        return hunt_description;
    }

    public void setHunt_description(String hunt_description) {
        this.hunt_description = hunt_description;
    }

    public String getIs_timed_hunt() {
        return is_timed_hunt;
    }

    public void setIs_timed_hunt(String is_timed_hunt) {
        this.is_timed_hunt = is_timed_hunt;
    }

    public String getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(String start_date_time) {
        this.start_date_time = start_date_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public void setCurrent_status(String current_status) {
        this.current_status = current_status;
    }

    public String getFirst_prize_text() {
        return first_prize_text;
    }

    public void setFirst_prize_text(String first_prize_text) {
        this.first_prize_text = first_prize_text;
    }

    public String getFirst_prize_upload_file_path() {
        return first_prize_upload_file_path;
    }

    public void setFirst_prize_upload_file_path(String first_prize_upload_file_path) {
        this.first_prize_upload_file_path = first_prize_upload_file_path;
    }

    public String getSecond_prize_text() {
        return second_prize_text;
    }

    public void setSecond_prize_text(String second_prize_text) {
        this.second_prize_text = second_prize_text;
    }

    public String getSecond_prize_upload_file_path() {
        return second_prize_upload_file_path;
    }

    public void setSecond_prize_upload_file_path(String second_prize_upload_file_path) {
        this.second_prize_upload_file_path = second_prize_upload_file_path;
    }

    public String getThird_prize_text() {
        return third_prize_text;
    }

    public void setThird_prize_text(String third_prize_text) {
        this.third_prize_text = third_prize_text;
    }

    public String getThird_prize_upload_file_path() {
        return third_prize_upload_file_path;
    }

    public void setThird_prize_upload_file_path(String third_prize_upload_file_path) {
        this.third_prize_upload_file_path = third_prize_upload_file_path;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getHunt_is_ready() {
        return hunt_is_ready;
    }

    public void setHunt_is_ready(String hunt_is_ready) {
        this.hunt_is_ready = hunt_is_ready;
    }

    public String getHunt_code() {
        return hunt_code;
    }

    public void setHunt_code(String hunt_code) {
        this.hunt_code = hunt_code;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPrebuilt_hunt_id() {
        return prebuilt_hunt_id;
    }

    public void setPrebuilt_hunt_id(String prebuilt_hunt_id) {
        this.prebuilt_hunt_id = prebuilt_hunt_id;
    }
}
