package com.hnweb.scavengertime.pojo;

import java.io.Serializable;

/**
 * Created by neha on 11/8/2016.
 */
public class JoinedHunts implements Serializable {
    int hunt_id;
    String hunt_title;
    String hunt_description;
    String is_timed_hunt;
    String start_date_time;
    String end_date_time;
    String hunt_hosted_by_user_id;

    public int getHunt_id() {
        return hunt_id;
    }

    public void setHunt_id(int hunt_id) {
        this.hunt_id = hunt_id;
    }

    public String getHunt_title() {
        return hunt_title;
    }

    public void setHunt_title(String hunt_title) {
        this.hunt_title = hunt_title;
    }

    public String getHunt_description() {
        return hunt_description;
    }

    public void setHunt_description(String hunt_description) {
        this.hunt_description = hunt_description;
    }

    public String getIs_timed_hunt() {
        return is_timed_hunt;
    }

    public void setIs_timed_hunt(String is_timed_hunt) {
        this.is_timed_hunt = is_timed_hunt;
    }

    public String getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(String start_date_time) {
        this.start_date_time = start_date_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getHunt_hosted_by_user_id() {
        return hunt_hosted_by_user_id;
    }

    public void setHunt_hosted_by_user_id(String hunt_hosted_by_user_id) {
        this.hunt_hosted_by_user_id = hunt_hosted_by_user_id;
    }
}
