package com.hnweb.scavengertime.pojo;

/**
 * Created by neha on 11/19/2016.
 */
public class Winners {

    String join_hunt_id, hunt_code, hunt_id, user_id, joined_date_time,
            take_datetime, finish_datetime, total_points, collected_items,
            time_used_days, time_used_hours, time_used_minutes, winner_level,
            is_winner, full_name, profile_photo;

    public String getJoin_hunt_id() {
        return join_hunt_id;
    }

    public void setJoin_hunt_id(String join_hunt_id) {
        this.join_hunt_id = join_hunt_id;
    }

    public String getHunt_code() {
        return hunt_code;
    }

    public void setHunt_code(String hunt_code) {
        this.hunt_code = hunt_code;
    }

    public String getHunt_id() {
        return hunt_id;
    }

    public void setHunt_id(String hunt_id) {
        this.hunt_id = hunt_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getJoined_date_time() {
        return joined_date_time;
    }

    public void setJoined_date_time(String joined_date_time) {
        this.joined_date_time = joined_date_time;
    }

    public String getTake_datetime() {
        return take_datetime;
    }

    public void setTake_datetime(String take_datetime) {
        this.take_datetime = take_datetime;
    }

    public String getFinish_datetime() {
        return finish_datetime;
    }

    public void setFinish_datetime(String finish_datetime) {
        this.finish_datetime = finish_datetime;
    }

    public String getTotal_points() {
        return total_points;
    }

    public void setTotal_points(String total_points) {
        this.total_points = total_points;
    }

    public String getCollected_items() {
        return collected_items;
    }

    public void setCollected_items(String collected_items) {
        this.collected_items = collected_items;
    }

    public String getTime_used_days() {
        return time_used_days;
    }

    public void setTime_used_days(String time_used_days) {
        this.time_used_days = time_used_days;
    }

    public String getTime_used_hours() {
        return time_used_hours;
    }

    public void setTime_used_hours(String time_used_hours) {
        this.time_used_hours = time_used_hours;
    }

    public String getTime_used_minutes() {
        return time_used_minutes;
    }

    public void setTime_used_minutes(String time_used_minutes) {
        this.time_used_minutes = time_used_minutes;
    }

    public String getWinner_level() {
        return winner_level;
    }

    public void setWinner_level(String winner_level) {
        this.winner_level = winner_level;
    }

    public String getIs_winner() {
        return is_winner;
    }

    public void setIs_winner(String is_winner) {
        this.is_winner = is_winner;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }
}
