package com.hnweb.scavengertime.pojo;

/**
 * Created by neha on 11/19/2016.
 */
public class PlayersMedia {

    String umfphi_id,player_user_id,hunt_id,hunt_item_id,file_path,file_type,uploaded_datetime,point_value,hunt_item_name;

    public String getUmfphi_id() {
        return umfphi_id;
    }

    public void setUmfphi_id(String umfphi_id) {
        this.umfphi_id = umfphi_id;
    }

    public String getPlayer_user_id() {
        return player_user_id;
    }

    public void setPlayer_user_id(String player_user_id) {
        this.player_user_id = player_user_id;
    }

    public String getHunt_id() {
        return hunt_id;
    }

    public void setHunt_id(String hunt_id) {
        this.hunt_id = hunt_id;
    }

    public String getHunt_item_id() {
        return hunt_item_id;
    }

    public void setHunt_item_id(String hunt_item_id) {
        this.hunt_item_id = hunt_item_id;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getUploaded_datetime() {
        return uploaded_datetime;
    }

    public void setUploaded_datetime(String uploaded_datetime) {
        this.uploaded_datetime = uploaded_datetime;
    }

    public String getPoint_value() {
        return point_value;
    }

    public void setPoint_value(String point_value) {
        this.point_value = point_value;
    }

    public String getHunt_item_name() {
        return hunt_item_name;
    }

    public void setHunt_item_name(String hunt_item_name) {
        this.hunt_item_name = hunt_item_name;
    }
}
