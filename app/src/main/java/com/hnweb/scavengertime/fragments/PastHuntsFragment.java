package com.hnweb.scavengertime.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.PastHuntsAdapter;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.PastHunts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by neha on 10/26/2016.
 */
public class PastHuntsFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<PastHunts> pastList = new ArrayList<PastHunts>();
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    TextView noRecordTV;
    RadioGroup huntsRG;
    int selectedId;
    RadioButton hostedHuntBTN, joinedHuntBTN;
    String user_type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            if (Constants.HUNT_TYPE == 0) {
                hostedHuntBTN.setChecked(true);
                joinedHuntBTN.setChecked(false);
//            try {
            if (CheckConnectivity.checkInternetConnection(getActivity()))
                activeHuntsWebservice(Urls.myHostedHunts);
//            }catch (Exception e){
//                activeHuntsWebservice(Urls.myHostedHunts);
//            }

//            } else {
//                joinedHuntBTN.setChecked(true);
//                hostedHuntBTN.setChecked(false);
//                if (CheckConnectivity.checkInternetConnection(getActivity()))
//                    activeHuntsWebservice(Urls.myJoinedHunts);
//            }
        }
//            activeHuntsWebservice();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_past_hunts, container, false);
        init(v);
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
//        activeHuntsWebservice();

        return v;
    }

    private void init(final View v) {

        noRecordTV = (TextView) v.findViewById(R.id.noRecordTV);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        huntsRG = (RadioGroup) v.findViewById(R.id.huntRG);
        hostedHuntBTN = (RadioButton) v.findViewById(R.id.hostedHuntBTN);
        joinedHuntBTN = (RadioButton) v.findViewById(R.id.joinedHuntBTN);
//        selectedId = huntsRG.getCheckedRadioButtonId();
        huntsRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedId = huntsRG.indexOfChild(v.findViewById(checkedId));
                switch (selectedId) {
                    case 0:
//                        noRecordTV.setVisibility(View.GONE);
//                        recyclerView.setVisibility(View.VISIBLE);
                        if (CheckConnectivity.checkInternetConnection(getActivity()))
                            activeHuntsWebservice(Urls.myHostedHunts);
//                        Constants.HUNT_TYPE = 0;
                        break;
                    case 1:
//                        noRecordTV.setVisibility(View.VISIBLE);
//                        recyclerView.setVisibility(View.GONE);
                        if (CheckConnectivity.checkInternetConnection(getActivity()))
                            activeHuntsWebservice(Urls.myJoinedHunts);
//                        Constants.HUNT_TYPE = 1;
//                        Constants.underDevelopment(getActivity());
                        break;
                }
            }
        });
    }

    public void activeHuntsWebservice(String urlHunts) {

        Constants.showProgress(getActivity());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlHunts,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");


                            if (Constants.MESSAGE_CODE == 1) {
                                JSONArray jarr = jobj.getJSONArray("response");
                                pastList.clear();
                                for (int i = 0; i < jarr.length(); i++) {
                                    PastHunts act = new PastHunts();
                                    act.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
                                    act.setHunt_title(jarr.getJSONObject(i).getString("hunt_title"));
                                    act.setHunt_description(jarr.getJSONObject(i).getString("hunt_description"));
                                    act.setIs_timed_hunt(jarr.getJSONObject(i).getString("is_timed_hunt"));
                                    act.setStart_date_time(jarr.getJSONObject(i).getString("start_date_time"));
                                    act.setEnd_date_time(jarr.getJSONObject(i).getString("end_date_time"));
                                    act.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
//                                    act.setCurrent_status(jarr.getJSONObject(i).getString("current_status"));
//                                    act.setFirst_prize_text(jarr.getJSONObject(i).getString("first_prize_text"));
//                                    act.setFirst_prize_upload_file_path(jarr.getJSONObject(i).getString("first_prize_upload_file_path"));
//                                    act.setSecond_prize_text(jarr.getJSONObject(i).getString("second_prize_text"));
//                                    act.setSecond_prize_upload_file_path(jarr.getJSONObject(i).getString("second_prize_upload_file_path"));
//                                    act.setThird_prize_text(jarr.getJSONObject(i).getString("third_prize_text"));
//                                    act.setThird_prize_upload_file_path(jarr.getJSONObject(i).getString("third_prize_upload_file_path"));
//                                    act.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
//                                    act.setHunt_is_ready(jarr.getJSONObject(i).getString("hunt_is_ready"));
//                                    act.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
//                                    act.setUser_id(jarr.getJSONObject(i).getString("user_id"));
//                                    act.setPrebuilt_hunt_id(jarr.getJSONObject(i).getString("prebuilt_hunt_id"));

                                    pastList.add(act);
                                }
//                                preBuildHuntsAdapter = new PreBuildHuntsAdapter(PreBuildHuntsActivity.this, pbhList);
                                if (pastList.size() == 0) {
                                    noRecordTV.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                } else {
                                    noRecordTV.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    recyclerView.setAdapter(new PastHuntsAdapter(getActivity(), pastList));
                                }

//                                Bundle b = new Bundle();
//                                b.putSerializable("pbhl", pbhList);
//                                Intent intent = new Intent(PreBuildHuntsActivity.this, PurchaseHuntActivity.class);
//                                intent.putExtras(b);
//                                startActivity(intent);

                                Constants.dismissProgressDialog(getActivity());
                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                noRecordTV.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                Constants.dismissProgressDialog(getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("filter_by", "Past");

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Constants.dismissProgressDialog(getActivity());
    }
}
