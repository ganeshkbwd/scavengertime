package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 11/19/2016.
 */
public class PlayersMediaViewHolder extends RecyclerView.ViewHolder {

    public ImageView itemIV;
    public TextView itemNameTV,pointValueTV;

    public PlayersMediaViewHolder(View itemView) {
        super(itemView);
        itemIV = (ImageView) itemView.findViewById(R.id.pmitemIV);
        itemNameTV = (TextView) itemView.findViewById(R.id.pmitemNameTV);
        pointValueTV = (TextView) itemView.findViewById(R.id.pmpointValueTV);
    }
}
