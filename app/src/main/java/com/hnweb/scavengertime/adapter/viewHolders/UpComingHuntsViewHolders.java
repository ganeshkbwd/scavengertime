package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 10/27/2016.
 */

public class UpComingHuntsViewHolders extends RecyclerView.ViewHolder  {
    public TextView huntNameTV, huntStartOnTV, huntEndsOnTV, startTimeTV, endTimeTV;
    public ImageView shareIV, editHuntIV;
    public LinearLayout itemLL;

    public UpComingHuntsViewHolders(View itemView) {
        super(itemView);
        huntNameTV = (TextView) itemView.findViewById(R.id.huntNameTV);
        huntStartOnTV = (TextView) itemView.findViewById(R.id.huntStartOnTV);
        huntEndsOnTV = (TextView) itemView.findViewById(R.id.huntEndsOnTV);
        startTimeTV = (TextView) itemView.findViewById(R.id.startTimeTV);
        endTimeTV = (TextView) itemView.findViewById(R.id.endTimeTV);
        shareIV = (ImageView) itemView.findViewById(R.id.shareIV);
        editHuntIV = (ImageView) itemView.findViewById(R.id.editHuntItemIV);
        itemLL = (LinearLayout) itemView.findViewById(R.id.itemLL);


    }



}