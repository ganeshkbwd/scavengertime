package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 10/22/2016.
 */
public class PreBuildHuntsViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView huntNameTV, priceTV;
    public Button purchaseBTN;

    public PreBuildHuntsViewHolders(View itemView) {
        super(itemView);
        huntNameTV = (TextView) itemView.findViewById(R.id.huntNameTV);
        priceTV = (TextView) itemView.findViewById(R.id.priceTV);
        purchaseBTN = (Button) itemView.findViewById(R.id.purchaseBTN);
        purchaseBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        Intent intent = new Intent(v.getContext(), PurchaseHuntActivity.class);
//
//        v.getContext().startActivity(intent);
    }
}
