package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 11/18/2016.
 */
public class ParticipantsViewHolder extends RecyclerView.ViewHolder {
    public ImageView proIV;
    public TextView nameTV, collectedItemsTV, totalPointsTV, timeUsedTV;
    public Button rankBTN;

    public ParticipantsViewHolder(View itemView) {
        super(itemView);
        proIV = (ImageView) itemView.findViewById(R.id.proIV);
        nameTV = (TextView) itemView.findViewById(R.id.nameTV);
        collectedItemsTV = (TextView) itemView.findViewById(R.id.collectedItemsTV);
        totalPointsTV = (TextView) itemView.findViewById(R.id.totalPointsTV);
        timeUsedTV = (TextView) itemView.findViewById(R.id.timeUsedTV);
        rankBTN = (Button) itemView.findViewById(R.id.rankBTN);
    }
}
