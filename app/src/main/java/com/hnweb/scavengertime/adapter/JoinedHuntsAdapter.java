package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.JoinedHuntsViewHolder;
import com.hnweb.scavengertime.pojo.JoinedHunts;

import java.util.ArrayList;

/**
 * Created by neha on 11/8/2016.
 */
public class JoinedHuntsAdapter extends RecyclerView.Adapter<JoinedHuntsViewHolder> {
    Context context;
    ArrayList<JoinedHunts> actList;
    String user_type;


    public JoinedHuntsAdapter(Activity activity, ArrayList<JoinedHunts> actList, String user_type) {
        this.context = activity;
        this.actList = actList;
        this.user_type = user_type;
    }

    @Override
    public JoinedHuntsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_hunts_item, null);
        JoinedHuntsViewHolder rcv = new JoinedHuntsViewHolder(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(JoinedHuntsViewHolder holder, final int position) {
        holder.huntNameTV.setText(actList.get(position).getHunt_title());
        String[] startDate = actList.get(position).getStart_date_time().split(" ");
        String[] endDate = actList.get(position).getEnd_date_time().split(" ");

        holder.huntStartOnTV.setText(startDate[0].toString());
        holder.huntEndsOnTV.setText(endDate[0].toString());
        String[] time = startDate[1].toString().split(":");
        if (Integer.parseInt(time[0].toString()) > 11) {
            holder.startTimeTV.setText(time[0] + ":" + time[1] + " pm");
        } else {
            holder.startTimeTV.setText(time[0] + ":" + time[1] + " am");
        }
        holder.endTimeTV.setText(endDate[1].toString());

//        if (user_type.equalsIgnoreCase("HOST")) {
//            holder.huntStartIV.setVisibility(View.VISIBLE);
//            holder.huntStopIV.setVisibility(View.VISIBLE);
//        } else {
//            holder.huntStartIV.setVisibility(View.GONE);
//            holder.huntStopIV.setVisibility(View.GONE);
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Constants.underDevelopment(context);
//                    Intent intent = new Intent(context, HuntDetailsActivity.class);
//                    intent.putExtra("From", "Active");
//                    intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
//                    intent.putExtra("end_date", String.valueOf(actList.get(position).getEnd_date_time()));
//                    context.startActivity(intent);
//                    ((Activity) context).finish();
//                }
//            });
//        }

    }

    @Override
    public int getItemCount() {
        return actList.size();
    }
}
