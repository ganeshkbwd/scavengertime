package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 10/28/2016.
 */
public class HuntItemsViewHolder extends RecyclerView.ViewHolder {
    public TextView hunt_titleTV;
    public TextView pointValueTV;
    public Button purchaseBTN;
    public ImageView myProfileIV;


    public HuntItemsViewHolder(View itemView) {
        super(itemView);
        myProfileIV = (ImageView) itemView.findViewById(R.id.myProfileIV);
        hunt_titleTV = (TextView) itemView.findViewById(R.id.hunt_titleTV);
        pointValueTV = (TextView) itemView.findViewById(R.id.pointValueTV);
        purchaseBTN = (Button) itemView.findViewById(R.id.purchaseBTN);

    }


}
