package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.UpComingHuntsViewHolders;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.hostsActivities.HuntItemsActivity;
import com.hnweb.scavengertime.playersActivities.HuntDetailsActivity;
import com.hnweb.scavengertime.pojo.UpComingHunts;

import java.util.ArrayList;

/**
 * Created by neha on 10/27/2016.
 */

public class UpComingHuntsAdapter extends RecyclerView.Adapter<UpComingHuntsViewHolders> {

    Context context;
    ArrayList<UpComingHunts> actList;
    ArrayList<String> contactList;
    Cursor cursor;
    int counter;
    private ListView mListView;
//    public static int PICK_CONTACT = 2;
    String user_type;


    public UpComingHuntsAdapter(Activity activity, ArrayList<UpComingHunts> actList, String user_type) {
        this.context = activity;
        this.actList = actList;
        this.user_type = user_type;
    }

    @Override
    public UpComingHuntsViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_hunts_item, null);
        UpComingHuntsViewHolders rcv = new UpComingHuntsViewHolders(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(UpComingHuntsViewHolders holder, final int position) {

        holder.huntNameTV.setText(actList.get(position).getHunt_title());
        final String[] startDate = actList.get(position).getStart_date_time().split(" ");
        String[] endDate = actList.get(position).getEnd_date_time().split(" ");

        holder.huntStartOnTV.setText(startDate[0].toString());
        holder.huntEndsOnTV.setText(endDate[0].toString());
        String[] time = startDate[1].toString().split(":");
        if (Integer.parseInt(time[0].toString()) > 11) {
            holder.startTimeTV.setText(time[0] + ":" + time[1] + " pm");
        } else {
            holder.startTimeTV.setText(time[0] + ":" + time[1] + " am");
        }

        holder.endTimeTV.setText(endDate[1].toString());
        holder.editHuntIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HuntItemsActivity.class);
                intent.putExtra("huntId", String.valueOf(actList.get(position).getHunt_id()));
                intent.putExtra("huntTitle", actList.get(position).getHunt_title());
//                intent.putExtra("description",actList.get(position).getHunt_description());
//                intent.putExtra("pointValue",actList.get(position).getP)
                context.startActivity(intent);
                ((Activity) context).finish();


            }
        });
        holder.shareIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SMS = true;
                Constants.HUNT_TITLE_SHARE = actList.get(position).getHunt_title();
                Constants.HUNT_CODE_SHARE = actList.get(position).getHunt_code();
                Intent contactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                contactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                ((Activity) context).startActivityForResult(contactIntent, 200);

//                ((Activity) context).finish();

            }
        });

        if (user_type.equalsIgnoreCase("HOST")) {
            holder.shareIV.setVisibility(View.VISIBLE);
            holder.editHuntIV.setVisibility(View.VISIBLE);
        } else {
            holder.shareIV.setVisibility(View.GONE);
            holder.editHuntIV.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Constants.underDevelopment(context);
                    Intent intent = new Intent(context, HuntDetailsActivity.class);
                    intent.putExtra("From", "UpComing");
                    intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return actList.size();
    }


}