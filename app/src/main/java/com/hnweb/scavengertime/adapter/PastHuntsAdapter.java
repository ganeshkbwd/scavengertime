package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.PastHuntsViewHolder;
import com.hnweb.scavengertime.pojo.PastHunts;

import java.util.ArrayList;

/**
 * Created by neha on 10/27/2016.
 */

public class PastHuntsAdapter extends RecyclerView.Adapter<PastHuntsViewHolder> {

    Context context;
    ArrayList<PastHunts> actList;


    public PastHuntsAdapter(Activity activity, ArrayList<PastHunts> actList) {
        this.context = activity;
        this.actList = actList;
    }

    @Override
    public PastHuntsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_hunts_item, null);
        PastHuntsViewHolder rcv = new PastHuntsViewHolder(layoutView);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        return rcv;
    }

    @Override
    public void onBindViewHolder(PastHuntsViewHolder holder, int position) {
        holder.huntNameTV.setText(actList.get(position).getHunt_title());
        String[] startDate = actList.get(position).getStart_date_time().split(" ");
        String[] endDate = actList.get(position).getEnd_date_time().split(" ");

        holder.huntStartOnTV.setText(startDate[0].toString());
        holder.huntEndsOnTV.setText(endDate[0].toString());
        String[] time = startDate[1].toString().split(":");
        if (Integer.parseInt(time[0].toString()) > 11){
            holder.startTimeTV.setText(time[0]+":"+time[1]+" pm");
        }else {
            holder.startTimeTV.setText(time[0]+":"+time[1]+" am");
        }
        holder.endTimeTV.setText(endDate[1].toString());
    }

    @Override
    public int getItemCount() {
        return actList.size();
    }
}
