package com.hnweb.scavengertime.adapter.viewHolders;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.hnweb.scavengertime.ChangePasswordActivity;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.MyProfileActivity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.hostsActivities.AllHuntItemsActivity;
import com.hnweb.scavengertime.hostsActivities.CreateHuntsActivity;
import com.hnweb.scavengertime.hostsActivities.CreateTeamActivity;
import com.hnweb.scavengertime.hostsActivities.InviteFriendsActivity;
import com.hnweb.scavengertime.hostsActivities.ManageTeamsActivity;
import com.hnweb.scavengertime.hostsActivities.PreBuildHuntsActivity;

/**
 * Created by neha on 10/22/2016.
 */
public class DashboardViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView iconIV;

    public DashboardViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        iconIV = (ImageView) itemView.findViewById(R.id.iconIV);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (getPosition()) {
            case 0:
                intent = new Intent(v.getContext(), CreateHuntsActivity.class);
                v.getContext().startActivity(intent);
//                Toast.makeText(v.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
                break;
            case 1:
                intent = new Intent(v.getContext(), MyHunts_Activity.class);
                intent.putExtra("ComeFrom", "");
                v.getContext().startActivity(intent);
                break;
            case 2:

                intent = new Intent(v.getContext(), InviteFriendsActivity.class);
//                intent.putExtra("ComeFrom", "Invite");
                v.getContext().startActivity(intent);
//                Toast.makeText(v.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
                break;
            case 3:
                intent = new Intent(v.getContext(), CreateTeamActivity.class);
                v.getContext().startActivity(intent);
//                Constants.underDevelopment(v.getContext());
                break;
            case 4:
                intent = new Intent(v.getContext(), ManageTeamsActivity.class);
                v.getContext().startActivity(intent);
//                Constants.underDevelopment(v.getContext());
                break;
//            case 5:
//                intent = new Intent(v.getContext(), MyHunts_Activity.class);
//                intent.putExtra("ComeFrom", "");
//                v.getContext().startActivity(intent);
////                Toast.makeText(v.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
//                break;
            case 5:
                intent = new Intent(v.getContext(), ChangePasswordActivity.class);
                v.getContext().startActivity(intent);
//                Constants.underDevelopment(v.getContext());
                break;
            case 6:
                intent = new Intent(v.getContext(), MyProfileActivity.class);
                v.getContext().startActivity(intent);
//                Toast.makeText(v.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
                break;
            case 7:
                intent = new Intent(v.getContext(), PreBuildHuntsActivity.class);
                v.getContext().startActivity(intent);

                break;
            case 8:
//                Constants.underDevelopment(v.getContext());
                intent = new Intent(v.getContext(), AllHuntItemsActivity.class);
                v.getContext().startActivity(intent);
                break;
            case 9:
                Constants.upgradeDialog((Activity) v.getContext());
                break;

        }


    }
}
