package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 10/27/2016.
 */

public class PastHuntsViewHolder extends RecyclerView.ViewHolder {
    public TextView huntNameTV, huntStartOnTV, huntEndsOnTV, startTimeTV, endTimeTV;

    public PastHuntsViewHolder(View itemView) {
        super(itemView);
        huntNameTV = (TextView) itemView.findViewById(R.id.huntNameTV);
        huntStartOnTV = (TextView) itemView.findViewById(R.id.huntStartOnTV);
        huntEndsOnTV = (TextView) itemView.findViewById(R.id.huntEndsOnTV);
        startTimeTV = (TextView) itemView.findViewById(R.id.startTimeTV);
        endTimeTV = (TextView) itemView.findViewById(R.id.endTimeTV);

    }

}
