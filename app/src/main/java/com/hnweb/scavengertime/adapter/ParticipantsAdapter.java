package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.ParticipantsViewHolder;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.hostsActivities.PlayersMediaActivity;
import com.hnweb.scavengertime.pojo.Participants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by neha on 11/18/2016.
 */
public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsViewHolder> {
    Context context;
    ArrayList<Participants> pList;
    HashMap<String, String> rWP;
    Map<String, String> headerMap;

    public ParticipantsAdapter(Activity activity, ArrayList<Participants> pList, HashMap<String, String> rWP, Map<String, String> headerMap) {
        this.context = activity;
        this.pList = pList;
        this.rWP = rWP;
        this.headerMap = headerMap;
    }

    @Override
    public ParticipantsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.participants_list_item, null);
        ParticipantsViewHolder rcv = new ParticipantsViewHolder(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(final ParticipantsViewHolder holder, final int position) {
        if (!pList.get(position).getProfile_photo().equalsIgnoreCase(""))
            Glide.with(context).load(pList.get(position).getProfile_photo()).into(holder.proIV);
        holder.nameTV.setText(pList.get(position).getFull_name());
        holder.collectedItemsTV.setText("Collected Items: " + pList.get(position).getCollected_items());
        holder.timeUsedTV.setText("Time Used: " + pList.get(position).getTime_used_days() + "Days" + pList.get(position).getTime_used_hours() + "Hrs" + pList.get(position).getTime_used_minutes() + "min");
        holder.totalPointsTV.setText("Total Points: " + pList.get(position).getTotal_points());

        holder.rankBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog settingsDialog = new Dialog(context);
                settingsDialog.setContentView(R.layout.rank_list_dialog);
                settingsDialog.setTitle("Please select Rank");
                settingsDialog.setCancelable(true);
//                Constants.addRanks();
                ListView listView = (ListView) settingsDialog.findViewById(R.id.listView);


                final ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(context, android.R.layout.simple_list_item_1, android.R.id.text1, Constants.myRanks);
                listView.setAdapter(adapter);

//                ArrayList<Integer>
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                        TextView r = (TextView) view.findViewById(android.R.id.text1);
                        if (Integer.parseInt(r.getText().toString()) == 1) {
                            Constants.myRanks.remove(Integer.parseInt(r.getText().toString()) - 1);
                            adapter.notifyDataSetChanged();
                            holder.rankBTN.setText(r.getText().toString());
                            Toast.makeText(context, "1---" + pList.get(position).getJoin_hunt_id(), Toast.LENGTH_LONG).show();
                            rWP.put(r.getText().toString(), pList.get(position).getJoin_hunt_id());
                            settingsDialog.dismiss();
                        } else if (Integer.parseInt(r.getText().toString()) == 2) {
                            Constants.myRanks.remove(Integer.parseInt(r.getText().toString()) - 1);
                            adapter.notifyDataSetChanged();
                            Toast.makeText(context, "2---" + pList.get(position).getJoin_hunt_id(), Toast.LENGTH_LONG).show();
                            holder.rankBTN.setText(r.getText().toString());
                            rWP.put(r.getText().toString(), pList.get(position).getJoin_hunt_id());
                            settingsDialog.dismiss();
                        } else if (Integer.parseInt(r.getText().toString()) == 3) {
                            Constants.myRanks.remove(Integer.parseInt(r.getText().toString()) - 1);
                            adapter.notifyDataSetChanged();
                            Toast.makeText(context, "3---" + pList.get(position).getJoin_hunt_id(), Toast.LENGTH_LONG).show();
                            holder.rankBTN.setText(r.getText().toString());
                            rWP.put(r.getText().toString(), pList.get(position).getJoin_hunt_id());
                            settingsDialog.dismiss();
                        }
                    }
                });
                settingsDialog.show();
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PlayersMediaActivity.class);
                intent.putExtra("join_hunt_id",pList.get(position).getJoin_hunt_id());
                intent.putExtra("Map", (Serializable) headerMap);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return pList.size();
    }


}
