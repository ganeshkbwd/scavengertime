package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.PlayersMediaViewHolder;
import com.hnweb.scavengertime.hostsActivities.SelectedMediaActivity;
import com.hnweb.scavengertime.pojo.PlayersMedia;

import java.util.ArrayList;

/**
 * Created by neha on 11/19/2016.
 */
public class PlayersMediaAdapter extends RecyclerView.Adapter<PlayersMediaViewHolder> {
    Context context;
    ArrayList<PlayersMedia> pmList;

    public PlayersMediaAdapter(Activity activity, ArrayList<PlayersMedia> pmList) {
        this.context = activity;
        this.pmList = pmList;
    }

    @Override
    public PlayersMediaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.players_media_list_item, null);
        PlayersMediaViewHolder rcv = new PlayersMediaViewHolder(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(PlayersMediaViewHolder holder, final int position) {
        if (!pmList.get(position).getHunt_item_name().equalsIgnoreCase(""))
            holder.itemNameTV.setText(pmList.get(position).getHunt_item_name());
        else
            holder.itemNameTV.setText(" ");
        holder.pointValueTV.setText(pmList.get(position).getPoint_value());
        if (pmList.get(position).getFile_type().equalsIgnoreCase("image")) {
            Glide.with(context).load(pmList.get(position).getFile_path()).into(holder.itemIV);

            holder.itemIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, SelectedMediaActivity.class);
                    intent.putExtra("type", pmList.get(position).getFile_type());
                    intent.putExtra("Path", pmList.get(position).getFile_path());
                    context.startActivity(intent);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return pmList.size();
    }
}
