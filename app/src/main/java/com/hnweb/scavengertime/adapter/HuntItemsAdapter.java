package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.HuntItemsViewHolder;
import com.hnweb.scavengertime.hostsActivities.EditPointValueActivity;
import com.hnweb.scavengertime.pojo.HuntItems;

import java.util.ArrayList;

/**
 * Created by neha on 10/28/2016.
 */
public class HuntItemsAdapter extends RecyclerView.Adapter<HuntItemsViewHolder> {
    Context context;
    ArrayList<HuntItems> huntItemsList;

    public HuntItemsAdapter(Activity activity, ArrayList<HuntItems> huntItemsList) {
        this.context = activity;
        this.huntItemsList = huntItemsList;

    }

    @Override
    public HuntItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hunt_items_item, null);
        HuntItemsViewHolder rcv = new HuntItemsViewHolder(layoutView);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        return rcv;
    }

    @Override
    public void onBindViewHolder(HuntItemsViewHolder holder, final int position) {

        if (!huntItemsList.get(position).getUploaded_file_path().equalsIgnoreCase(""))
            Glide.with(context).load(huntItemsList.get(position).getUploaded_file_path()).into(holder.myProfileIV);
        holder.hunt_titleTV.setText(huntItemsList.get(position).getTitle());
        holder.pointValueTV.setText("Point Value : " + huntItemsList.get(position).getPoint_value());
        holder.purchaseBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable("hunt_item", huntItemsList);
                Intent intent = new Intent(context, EditPointValueActivity.class);
                intent.putExtras(b);
                intent.putExtra("position", position);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return huntItemsList.size();
    }

    public void removeItem(int position) {
        huntItemsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, huntItemsList.size());
    }
}
