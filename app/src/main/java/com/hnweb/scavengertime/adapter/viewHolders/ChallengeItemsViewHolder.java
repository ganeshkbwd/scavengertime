package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 11/7/2016.
 */
public class ChallengeItemsViewHolder extends RecyclerView.ViewHolder {
    public TextView hunt_titleTV;
    public TextView pointValueTV;
    public ImageView mediaBTN,itemIV;
    public LinearLayout midLL;

    public ChallengeItemsViewHolder(View itemView) {
        super(itemView);
        hunt_titleTV = (TextView) itemView.findViewById(R.id.hunt_titleTV);
        pointValueTV = (TextView) itemView.findViewById(R.id.pointValueTV);
        mediaBTN = (ImageView) itemView.findViewById(R.id.mediaBTN);
        itemIV = (ImageView) itemView.findViewById(R.id.itemIV);
        midLL = (LinearLayout) itemView.findViewById(R.id.midLL);

    }
}
