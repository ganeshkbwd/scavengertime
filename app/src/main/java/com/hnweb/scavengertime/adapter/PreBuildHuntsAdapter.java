package com.hnweb.scavengertime.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.PreBuildHuntsViewHolders;
import com.hnweb.scavengertime.hostsActivities.PreBuildHuntsActivity;
import com.hnweb.scavengertime.hostsActivities.PurchaseHuntActivity;
import com.hnweb.scavengertime.pojo.PreBuildHuntList;

import java.util.ArrayList;

/**
 * Created by neha on 10/22/2016.
 */
public class PreBuildHuntsAdapter extends RecyclerView.Adapter<PreBuildHuntsViewHolders> {
    Context context;
    ArrayList<PreBuildHuntList> pbhList;


    public PreBuildHuntsAdapter(PreBuildHuntsActivity preBuildHuntsActivity, ArrayList<PreBuildHuntList> pbhList) {
        this.context = preBuildHuntsActivity;
        this.pbhList = pbhList;
    }

    @Override
    public PreBuildHuntsViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pre_build_hunts_item, null);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));
        PreBuildHuntsViewHolders rcv = new PreBuildHuntsViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(PreBuildHuntsViewHolders holder, final int position) {
        holder.huntNameTV.setText(pbhList.get(position).getHunt_title());
        holder.priceTV.setText(pbhList.get(position).getPrice());
        holder.purchaseBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PurchaseHuntActivity.class);
                intent.putExtra("ID", pbhList.get(position).getPrebuilt_hunt_id()+"");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pbhList.size();
    }
}
