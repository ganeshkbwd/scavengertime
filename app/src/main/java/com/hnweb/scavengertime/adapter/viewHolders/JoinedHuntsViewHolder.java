package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 11/8/2016.
 */
public class JoinedHuntsViewHolder extends RecyclerView.ViewHolder{
    public TextView huntNameTV, huntStartOnTV, huntEndsOnTV, startTimeTV, endTimeTV;
    public ImageView huntStartIV,huntStopIV;
    LinearLayout itemLL;

    public JoinedHuntsViewHolder(View itemView) {
        super(itemView);
        huntNameTV = (TextView) itemView.findViewById(R.id.huntNameTV);
        huntStartOnTV = (TextView) itemView.findViewById(R.id.huntStartOnTV);
        huntEndsOnTV = (TextView) itemView.findViewById(R.id.huntEndsOnTV);
        startTimeTV = (TextView) itemView.findViewById(R.id.startTimeTV);
        endTimeTV = (TextView) itemView.findViewById(R.id.endTimeTV);
        huntStartIV = (ImageView) itemView.findViewById(R.id.huntStartIV);
        huntStopIV = (ImageView) itemView.findViewById(R.id.huntStopIV);
        itemLL = (LinearLayout) itemView.findViewById(R.id.itemLL);
    }

}
