package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.DashboardViewHolders;

/**
 * Created by neha on 10/22/2016.
 */
public class DashboardAdapter extends RecyclerView.Adapter<DashboardViewHolders> {
    private Context context;
    private int[] dashboardIcons;

    public DashboardAdapter(Activity activity, int[] dashboardIcons) {
        this.context = activity;
        this.dashboardIcons = dashboardIcons;
    }

    @Override
    public DashboardViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item, null);
        DashboardViewHolders rcv = new DashboardViewHolders(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(DashboardViewHolders holder, int position) {
        holder.iconIV.setImageResource(dashboardIcons[position]);

    }

    @Override
    public int getItemCount() {
        return dashboardIcons.length;
    }
}
