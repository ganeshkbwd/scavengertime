package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.WinnersViewHolder;
import com.hnweb.scavengertime.pojo.Winners;

import java.util.ArrayList;

/**
 * Created by neha on 11/19/2016.
 */
public class WinnersAdapter extends RecyclerView.Adapter<WinnersViewHolder> {
    Context context;
    ArrayList<Winners> wList;

    public WinnersAdapter(Activity activity, ArrayList<Winners> wList) {
        this.context = activity;
        this.wList = wList;
    }

    @Override
    public WinnersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.winners_list_item, null);
        WinnersViewHolder rcv = new WinnersViewHolder(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(WinnersViewHolder holder, int position) {

        holder.nameTV.setText(wList.get(position).getFull_name());
        holder.totalPointsTV.setText("Total Points: " + wList.get(position).getTotal_points());
        holder.timeUsedTV.setText("Time Used: " + wList.get(position).getTime_used_days() + "Days " + wList.get(position).getTime_used_hours() + "Hrs " + wList.get(position).getTime_used_minutes() + "min");
        if (!wList.get(position).getProfile_photo().equalsIgnoreCase("")){
            holder.prizeIV.setBackgroundResource(0);
            Glide.with(context).load(wList.get(position).getProfile_photo()).into(holder.proIV);
        }

        if (!wList.get(position).getWinner_level().equalsIgnoreCase("")){
            holder.prizeIV.setBackgroundResource(0);
            Glide.with(context).load(wList.get(position).getWinner_level()).into(holder.prizeIV);

        }

    }

    @Override
    public int getItemCount() {
        return wList.size();
    }
}
