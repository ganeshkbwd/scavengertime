package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.MembersViewHolder;
import com.hnweb.scavengertime.hostsActivities.AddTeamMemberActivity;
import com.hnweb.scavengertime.pojo.TeamMembers;

import java.util.ArrayList;

/**
 * Created by neha on 5/1/2017.
 */

public class MembersListAdapter extends RecyclerView.Adapter<MembersViewHolder> {
    Activity activity;
    ArrayList<TeamMembers> teamMemberseList;

    public MembersListAdapter(Activity activity, ArrayList<TeamMembers> teamMemberseList) {
        this.activity = activity;
        this.teamMemberseList = teamMemberseList;
    }

    @Override
    public MembersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.members_list_item,parent,false);
        MembersViewHolder rcv = new MembersViewHolder(layoutView);

        return rcv;
    }

    @Override
    public void onBindViewHolder(MembersViewHolder holder, final int position) {

        holder.memberNameTV.setText(teamMemberseList.get(position).getTeam_member_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, AddTeamMemberActivity.class);
                intent.putExtra("team_id", teamMemberseList.get(position).getTeam_id());
                intent.putExtra("team_member_name", teamMemberseList.get(position).getTeam_member_name());
                intent.putExtra("team_member_id", teamMemberseList.get(position).getTeam_member_id());
                activity.startActivity(intent);
//                activity.finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return teamMemberseList.size();
    }
}
