package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.ChallengeItemsViewHolder;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.playersActivities.HuntItemDetailsActivity;
import com.hnweb.scavengertime.pojo.HuntItems;

import java.util.ArrayList;

/**
 * Created by neha on 11/7/2016.
 */
public class ChallengeAdapter extends RecyclerView.Adapter<ChallengeItemsViewHolder> {
    Context context;
    ArrayList<HuntItems> huntItemsList;

    public ChallengeAdapter(Activity activity, ArrayList<HuntItems> huntItemsList) {
        this.context = activity;
        this.huntItemsList = huntItemsList;

    }

    @Override
    public ChallengeItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.challenge_items, null);
        ChallengeItemsViewHolder rcv = new ChallengeItemsViewHolder(layoutView);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        return rcv;
    }

    @Override
    public void onBindViewHolder(ChallengeItemsViewHolder holder, final int position) {
        if (huntItemsList.get(position).getUploaded_file_type().equalsIgnoreCase("image")) {
            if (!huntItemsList.get(position).getUploaded_file_path().equalsIgnoreCase("")) {
                Glide.with(context).load(huntItemsList.get(position).getUploaded_file_path()).into(holder.itemIV);
            }
        }

        holder.hunt_titleTV.setText(huntItemsList.get(position).getTitle());
        holder.pointValueTV.setText("Point Value : " + huntItemsList.get(position).getPoint_value());
        holder.mediaBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HuntItemDetailsActivity.class);
                intent.putExtra("title",huntItemsList.get(position).getTitle());
                intent.putExtra("hunt_id",String.valueOf(huntItemsList.get(position).getHunt_id()));
                intent.putExtra("hunt_item_id",String.valueOf(huntItemsList.get(position).getHunt_item_id()));
                intent.putExtra("description",huntItemsList.get(position).getDescription());
                intent.putExtra("pointValue",huntItemsList.get(position).getPoint_value());
                context.startActivity(intent);
                Constants.underDevelopment(context);
            }
        });

        holder.midLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.underDevelopment(context);
            }
        });


    }

    @Override
    public int getItemCount() {
        return huntItemsList.size();
    }
}
