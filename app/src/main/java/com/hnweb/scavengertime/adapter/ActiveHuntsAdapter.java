package com.hnweb.scavengertime.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.viewHolders.ActiveHuntsViewHolder;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.hostsActivities.HuntItemsActivity;
import com.hnweb.scavengertime.hostsActivities.PastHuntDeatilsActivity;
import com.hnweb.scavengertime.hostsActivities.WinnersActivity;
import com.hnweb.scavengertime.playersActivities.HuntDetailsActivity;
import com.hnweb.scavengertime.pojo.ActiveHunts;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by neha on 10/26/2016.
 */
public class ActiveHuntsAdapter extends RecyclerView.Adapter<ActiveHuntsViewHolder> {

    Context context;
    ArrayList<ActiveHunts> actList;
    String user_type;
    String status;
    int service, user_id;
    String invite = "";


    public ActiveHuntsAdapter(Activity activity, ArrayList<ActiveHunts> actList, String user_type, String status, int service, int user_id) {
        this.context = activity;
        this.actList = actList;
        this.user_type = user_type;
        this.status = status;
        this.service = service;
        this.user_id = user_id;
    }

    public ActiveHuntsAdapter(Activity activity, ArrayList<ActiveHunts> actList, String user_type, String status, int service, int user_id, String invite) {
        this.context = activity;
        this.actList = actList;
        this.user_type = user_type;
        this.status = status;
        this.service = service;
        this.user_id = user_id;
        this.invite = invite;
    }

    @Override
    public ActiveHuntsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_hunts_item, null);
        ActiveHuntsViewHolder rcv = new ActiveHuntsViewHolder(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(final ActiveHuntsViewHolder holder, final int position) {
        holder.huntNameTV.setText(actList.get(position).getHunt_title());
        String[] startDate = actList.get(position).getStart_date_time().split(" ");
        String[] endDate = actList.get(position).getEnd_date_time().split(" ");

        holder.huntStartOnTV.setText(startDate[0].toString());
        holder.huntEndsOnTV.setText(endDate[0].toString());
        String[] time = startDate[1].toString().split(":");
        if (Integer.parseInt(time[0].toString()) > 11) {
            holder.startTimeTV.setText(time[0] + ":" + time[1] + " pm");
        } else {
            holder.startTimeTV.setText(time[0] + ":" + time[1] + " am");
        }
        holder.endTimeTV.setText(endDate[1].toString());

        if (user_type.equalsIgnoreCase("HOST")) {
            holder.huntStartIV.setVisibility(View.VISIBLE);
            holder.huntStopIV.setVisibility(View.VISIBLE);
            if (status.equalsIgnoreCase("Active")) {
                if (service == 0) {
                    holder.huntCodeTV.setText(actList.get(position).getHunt_code());
                    holder.huntStartIV.setImageResource(R.drawable.start_hunt_button);
                    holder.huntStopIV.setImageResource(R.drawable.end_hunt_button);

                    if (actList.get(position).getIs_timed_hunt().equalsIgnoreCase("Yes")) {
                        holder.huntStopIV.setVisibility(View.GONE);
                    } else {
                        holder.huntStartIV.setVisibility(View.VISIBLE);
                    }
                    holder.huntStartIV.setVisibility(View.GONE);
                } else {
                    holder.huntCodeLL.setVisibility(View.GONE);
                    holder.huntStartIV.setVisibility(View.GONE);
                    holder.huntStopIV.setVisibility(View.GONE);
                }

            } else if (status.equalsIgnoreCase("UpComing")) {

                if (invite.equalsIgnoreCase("invite")){
                    holder.huntStartIV.setImageResource(R.drawable.invite_button);
                    holder.huntStopIV.setImageResource(R.drawable.edit_hunt_icon);
                    holder.huntStartIV.setVisibility(View.VISIBLE);
                    holder.huntStopIV.setVisibility(View.INVISIBLE);
                    holder.huntCodeLL.setVisibility(View.VISIBLE);
                    holder.huntCodeTV.setText(actList.get(position).getHunt_code());
                }else {
                    holder.huntStartIV.setImageResource(R.drawable.invite_button);
                    holder.huntStopIV.setImageResource(R.drawable.edit_hunt_icon);
                    holder.huntStartIV.setVisibility(View.VISIBLE);
                    holder.huntStopIV.setVisibility(View.VISIBLE);
                    holder.huntCodeLL.setVisibility(View.GONE);
                }


            } else if (status.equalsIgnoreCase("Past")) {
                holder.huntStartIV.setVisibility(View.GONE);
                holder.huntStopIV.setVisibility(View.GONE);
                holder.huntCodeLL.setVisibility(View.GONE);
//                holder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        if (service == 0) {
//                            if (actList.get(position).getIs_winner_declared().equalsIgnoreCase("No")) {
//                                Constants.myRanks = new ArrayList<Integer>(Arrays.asList(Constants.rankArr));
//                                Intent intent = new Intent(context, PastHuntDeatilsActivity.class);
//                                intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
//                                context.startActivity(intent);
//                            } else {
//                                Intent intent = new Intent(context, WinnersActivity.class);
//                                intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
//                                context.startActivity(intent);
//                            }
//
//                        } else {
//
//                        }
//
//
//                    }
//                });
            }


        } else if (user_type.equalsIgnoreCase("PLAYER")) {
            if (status.equalsIgnoreCase("UpComing")) {
                holder.huntStartIV.setImageResource(R.drawable.invite_button);
                holder.huntStopIV.setImageResource(R.drawable.edit_hunt_icon);
                holder.huntStartIV.setVisibility(View.VISIBLE);
                holder.huntStopIV.setVisibility(View.VISIBLE);
                holder.huntCodeLL.setVisibility(View.GONE);

            } else {
                holder.huntStartIV.setVisibility(View.GONE);
                holder.huntStopIV.setVisibility(View.GONE);
                holder.huntCodeLL.setVisibility(View.GONE);
            }


//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (status.equalsIgnoreCase("Active")) {
//////                        holder.huntStartIV.setVisibility(View.GONE);
//////                        holder.huntStopIV.setVisibility(View.GONE);
////                        Constants.underDevelopment(context);
////                        Intent intent = new Intent(context, HuntDetailsActivity.class);
////                        intent.putExtra("From", "Active");
////                        intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
////                        intent.putExtra("end_date", String.valueOf(actList.get(position).getEnd_date_time()));
////                        context.startActivity(intent);
////                        ((Activity) context).finish();
//                    } else if (status.equalsIgnoreCase("UpComing")) {
//                        if (service == 0) {
//
//                        } else if (service == 1) {
//
//                            Intent intent = new Intent(context, HuntDetailsActivity.class);
//                            intent.putExtra("From", "UpComing");
//                            intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
//                            context.startActivity(intent);
//                            ((Activity) context).finish();
//                        }
//
//                    } else if (user_type.equalsIgnoreCase("Past")) {
////
//                        if (service == 0) {
//                            if (actList.get(position).getIs_winner_declared().equalsIgnoreCase("No")) {
//                                Constants.myRanks = new ArrayList<Integer>(Arrays.asList(Constants.rankArr));
//                                Intent intent = new Intent(context, PastHuntDeatilsActivity.class);
//                                intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
//                                context.startActivity(intent);
//                            } else {
//                                Intent intent = new Intent(context, WinnersActivity.class);
//                                intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
//                                context.startActivity(intent);
//                            }
//
//                        } else {
//
//                        }
//                    }
//
//                }
//            });
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("Active")) {
////                        holder.huntStartIV.setVisibility(View.GONE);
////                        holder.huntStopIV.setVisibility(View.GONE);
//                        Constants.underDevelopment(context);
                    if (service == 0) {


                    } else if (service == 1) {
                        if (actList.get(position).getIs_finished().equalsIgnoreCase("Yes")) {
                            toastDialog("You already finished this hunt.");
//                            Toast.makeText(context, "You already finished this hunt.", Toast.LENGTH_SHORT).show();

                        } else {
                            Intent intent = new Intent(context, HuntDetailsActivity.class);
                            intent.putExtra("From", "Active");
                            intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
                            intent.putExtra("end_date", String.valueOf(actList.get(position).getEnd_date_time()));
                            context.startActivity(intent);
                            ((Activity) context).finish();
                        }

                    }

                } else if (status.equalsIgnoreCase("UpComing")) {
                    if (service == 0) {

                    } else if (service == 1) {

                        if (invite.equalsIgnoreCase("invite")){

                        }else {
                            Intent intent = new Intent(context, HuntDetailsActivity.class);
                            intent.putExtra("From", "UpComing");
                            intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
                            context.startActivity(intent);
                            ((Activity) context).finish();
                        }


                    }

                } else if (status.equalsIgnoreCase("Past")) {
//
                    if (service == 0) {
                        if (actList.get(position).getIs_winner_declared().equalsIgnoreCase("No")) {
                            Constants.myRanks = new ArrayList<Integer>(Arrays.asList(Constants.rankArr));
                            Intent intent = new Intent(context, PastHuntDeatilsActivity.class);
                            intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent(context, WinnersActivity.class);
                            intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
                            context.startActivity(intent);
                        }

                    } else if (service == 1) {

                        if (actList.get(position).getIs_winner_declared().equalsIgnoreCase("Yes")) {

                            Intent intent = new Intent(context, WinnersActivity.class);
                            intent.putExtra("hunt_id", String.valueOf(actList.get(position).getHunt_id()));
                            context.startActivity(intent);
                        } else {
                            toastDialog("Winners not declared yet.");
//                            Toast.makeText(context, "Winners not declared yet.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }
        });


        holder.huntStartIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("Active")) {
                    Constants.underDevelopment(context);
                } else if (status.equalsIgnoreCase("UpComing")) {
                    Constants.SMS = true;
                    Constants.HUNT_TITLE_SHARE = actList.get(position).getHunt_title();
                    Constants.HUNT_CODE_SHARE = actList.get(position).getHunt_code();
                    Intent contactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    contactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                    ((Activity) context).startActivityForResult(contactIntent, 200);

                }

            }
        });

        holder.huntStopIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("Active")) {
//                    Constants.underDevelopment(context);
                    endHuntwebService(actList.get(position).getHunt_id());

                } else if (status.equalsIgnoreCase("UpComing")) {
                    Intent intent = new Intent(context, HuntItemsActivity.class);
                    intent.putExtra("huntId", String.valueOf(actList.get(position).getHunt_id()));
                    intent.putExtra("huntTitle", actList.get(position).getHunt_title());
//                intent.putExtra("description",actList.get(position).getHunt_description());
//                intent.putExtra("pointValue",actList.get(position).getP)
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return actList.size();
    }


    public void toastDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
//                        if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
//                            Constants.OK = 0;
//                            Intent intent = new Intent(context, DashboardActivity.class);
////                                    intent.putExtras(bundle);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            Constants.OK = 0;
//                            Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
////                                    intent.putExtras(bundle);
//                            startActivity(intent);
//                            finish();
//                        }
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void endHuntwebService(final int hunt_id) {
        final ProgressDialog progressDialog = new ProgressDialog(context);

        progressDialog.setMessage(context.getResources().getString(R.string.wait));
        progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.endHuntAPI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");


                            if (Constants.MESSAGE_CODE == 1) {
//                                JSONArray jarr = jobj.getJSONArray("response");
//                                MyHunts_Activity ma = new MyHunts_Activity();
                                ((MyHunts_Activity) context).huntsWebservice(Urls.myHostedHunts, "Active", 0, (MyHunts_Activity) context);
//                                notifyDataSetChanged();

                            } else {

                                Constants.MESSAGE = jobj.getString("message");

                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                progressDialog.dismiss();
                Constants.toastDialog((Activity) context, "Please Check Internet Connection..!!!");
//                Toast.makeText(MyHunts_Activity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", String.valueOf(hunt_id));
                final TimeZone timeZone = TimeZone.getTimeZone("UTC");
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance(timeZone).getTime());

                params.put("end_date_time", date);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);
    }
}
