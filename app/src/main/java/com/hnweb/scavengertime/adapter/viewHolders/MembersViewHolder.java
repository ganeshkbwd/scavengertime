package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 5/1/2017.
 */

public class MembersViewHolder extends RecyclerView.ViewHolder {
    public TextView memberNameTV;

    public MembersViewHolder(View itemView) {
        super(itemView);

        memberNameTV = (TextView) itemView.findViewById(R.id.memberNameTV);

    }
}
