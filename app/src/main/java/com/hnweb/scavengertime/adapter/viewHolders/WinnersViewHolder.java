package com.hnweb.scavengertime.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnweb.scavengertime.R;

/**
 * Created by neha on 11/19/2016.
 */
public class WinnersViewHolder extends RecyclerView.ViewHolder {
    public ImageView proIV, prizeIV;
    public TextView nameTV, totalPointsTV, timeUsedTV;

    public WinnersViewHolder(View itemView) {
        super(itemView);
        proIV = (ImageView) itemView.findViewById(R.id.proIV);
        prizeIV = (ImageView) itemView.findViewById(R.id.prizeIV);
        nameTV = (TextView) itemView.findViewById(R.id.nameTV);
        totalPointsTV = (TextView) itemView.findViewById(R.id.totalPointsTV);
        timeUsedTV = (TextView) itemView.findViewById(R.id.timeUsedTV);
    }
}
