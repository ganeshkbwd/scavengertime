package com.hnweb.scavengertime;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.PermissionUtility;
import com.hnweb.scavengertime.hostsActivities.DashboardActivity;
import com.hnweb.scavengertime.playersActivities.JoinHuntActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String user_type;
    private PermissionUtility putility;
    private ArrayList<String> permission_list;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getApplication().setTheme(R.style.SplashTheme);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
//        changeStatusBarColor();
        putility = new PermissionUtility(this);
        permission_list = new ArrayList<String>();
//        permission_list.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permission_list.add(android.Manifest.permission.INTERNET);
        permission_list.add(android.Manifest.permission.ACCESS_WIFI_STATE);
        permission_list.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        permission_list.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permission_list.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        permission_list.add(Manifest.permission.READ_CONTACTS);
        permission_list.add(Manifest.permission.CAMERA);
        permission_list.add(Manifest.permission.SEND_SMS);
//        permission_list.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        putility.setListner(new PermissionUtility.OnPermissionCallback() {
            @Override
            public void OnComplete(boolean is_granted) {
                Log.i("OnPermissionCallback", "is_granted = " + is_granted);
                if (is_granted) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//go next

//                            goNextScreen();
                        }
                    }, 3000);

                } else {
                    putility.checkPermission(permission_list);
                }
            }
        });


        putility.checkPermission(permission_list);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        final TimeZone timeZone = TimeZone.getTimeZone("UTC");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat nsdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance(timeZone).getTime());
//        Date dateObj;
        String newDateFormat = null;
        try {
//            dateObj = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
            newDateFormat = String.valueOf(new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date)));
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        Log.e("TIME", date);
        Log.e("newDateFormat", newDateFormat);

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.white));
//            window.setStatusBarTextColor(getResources().getColor(Color.RED));

        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.createHuntBTN:
                if (login == 1 && user_type.equalsIgnoreCase("HOST")) {
//                    Constants.USER_TYPE = "HOST";
                    intent = new Intent(this, DashboardActivity.class);
//                    intent.putExtra("TYPE","HOST");
                    startActivity(intent);
                } else {
                    Constants.USER_TYPE = "HOST";
                    intent = new Intent(this, SignUpActivity.class);
                    startActivity(intent);
                }


                break;
            case R.id.startPlayBTN:
//                Constants.underDevelopment(this);
                if (login == 1 && user_type.equalsIgnoreCase("PLAYER")) {
                    intent = new Intent(this, JoinHuntActivity.class);
                    startActivity(intent);
                } else {
                    Constants.USER_TYPE = "PLAYER";
                    intent = new Intent(this, SignUpActivity.class);
                    startActivity(intent);
                }


                break;
        }
    }


}
