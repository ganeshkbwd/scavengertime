package com.hnweb.scavengertime;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.RoundImageTransform;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;
import com.hnweb.scavengertime.hostsActivities.DashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String user_type;
    ImageView myProfileIV;
    TextView myNameTV, phoneTV, emailTV;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
        setToolbarDrawer();
        init();
        myProfileWebservice();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void init() {
        myProfileIV = (ImageView) findViewById(R.id.myProfileIV);
        myNameTV = (TextView) findViewById(R.id.myNameTV);
        phoneTV = (TextView) findViewById(R.id.phoneTV);
        emailTV = (TextView) findViewById(R.id.emailTV);
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(MyProfileActivity.this, String.valueOf(user_id),iv,nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editProfileBTN:
                Intent intent = new Intent(this, EditProfileActivity.class);

                startActivity(intent);
                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.logoutBTN:
                Constants.logout_fun(this);
                break;
        }
    }

    public void myProfileWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.myProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

                                Constants.myProfile = jobj.getString("profile_photo");
                                Constants.myName = jobj.getString("full_name");
                                Constants.email = jobj.getString("email_address");
                                Constants.phone = jobj.getString("phone_number");
                                jobj.getString("balance_hunts_count");

                                if (Validations.strslength(Constants.myProfile))
                                    Glide.with(MyProfileActivity.this).load(Constants.myProfile).transform(new RoundImageTransform(MyProfileActivity.this)).into(myProfileIV);
                                myNameTV.setText(Constants.myName);
                                emailTV.setText(Constants.email);
                                phoneTV.setText(Constants.phone);


                                Constants.progressDialog.dismiss();


                            } else {

                                Constants.progressDialog.dismiss();//
                                Constants.toastDialog(MyProfileActivity.this, Constants.MESSAGE);
//                                Toast.makeText(MyProfileActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(MyProfileActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(MyProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "MyProfile Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.hnweb.scavengertime/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "MyProfile Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.hnweb.scavengertime/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (user_type.equalsIgnoreCase("HOST")) {
            Intent intent = new Intent(this, DashboardActivity.class);

            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, MyHunts_Activity.class);
            intent.putExtra("ComeFrom", "");
            startActivity(intent);
            finish();
        }

    }
}
