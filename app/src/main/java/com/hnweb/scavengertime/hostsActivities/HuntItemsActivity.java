package com.hnweb.scavengertime.hostsActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.HuntItemsAdapter;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.HuntItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class HuntItemsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String hunt_id, hunt_title;
    TextView titleTV;
    RecyclerView recyclerView;
    ArrayList<HuntItems> huntItemsList = new ArrayList<HuntItems>();
    HuntItemsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hunt_items);
        setToolbarDrawer();
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        hunt_id = getIntent().getExtras().getString("huntId");
        hunt_title = getIntent().getExtras().getString("huntTitle");
        init();


        huntItemsWebservice(hunt_id);
    }

    public void init() {
        titleTV = (TextView) findViewById(R.id.titleTV);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        initSwipe();

        titleTV.setText(hunt_title);
    }

    public void initSwipe() {

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    deleteHuntItemWebservice(hunt_id, huntItemsList.get(position).getHunt_item_id(), viewHolder.itemView, position);
//                    adapter.removeItem(position);

                } else {

//                    deleteHuntItemWebservice(hunt_id, huntItemsList.get(position).getHunt_item_id(), viewHolder.itemView, position);
//                    edit_position = position;
//                    alertDialog.setTitle("Edit Country");
//                    et_country.setText(countries.get(position));
//                    alertDialog.show();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    private void removeView(View itemView) {
        if (itemView.getParent() != null) {
            ((ViewGroup) itemView.getParent()).removeView(itemView);

        }
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(HuntItemsActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    public void huntItemsWebservice(final String hunt_id) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.huntItems,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
                                huntItemsList.clear();
                                JSONArray jarr = jobj.getJSONArray("hunt_items");
                                for (int i = 0; i < jarr.length(); i++) {
                                    HuntItems huntItems = new HuntItems();
                                    huntItems.setHunt_item_id(jarr.getJSONObject(i).getInt("hunt_item_id"));
                                    huntItems.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
                                    huntItems.setTitle(jarr.getJSONObject(i).getString("title"));
                                    huntItems.setDescription(jarr.getJSONObject(i).getString("description"));
                                    huntItems.setPoint_value(jarr.getJSONObject(i).getString("point_value"));
                                    huntItems.setUploaded_file_path(jarr.getJSONObject(i).getString("uploaded_file_path"));
                                    huntItems.setUploaded_file_type(jarr.getJSONObject(i).getString("uploaded_file_type"));
                                    huntItems.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    huntItems.setPrice_of_hunt_item(jarr.getJSONObject(i).getString("price_of_hunt_item"));
                                    huntItems.setPrebuild_hunt_item_id(jarr.getJSONObject(i).getString("prebuild_hunt_item_id"));
                                    huntItemsList.add(huntItems);
                                }
                                adapter = new HuntItemsAdapter(HuntItemsActivity.this, huntItemsList);
                                recyclerView.setAdapter(adapter);

                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(HuntItemsActivity.this, Constants.MESSAGE);
//                                Constants.progressDialog.dismiss();
                            }
                            Constants.dismissProgressDialog(HuntItemsActivity.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(HuntItemsActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(HuntItemsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", hunt_id);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void deleteHuntItemWebservice(final String hunt_id, final int hunt_item_id, final View itemView, final int position) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.deleteMyHuntItems,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {

                                adapter.removeItem(position);
                                removeView(itemView);
//                                huntItemsList.clear();
//                                JSONArray jarr = jobj.getJSONArray("hunt_items");
//                                for (int i = 0; i < jarr.length(); i++) {
//                                    HuntItems huntItems = new HuntItems();
//                                    huntItems.setHunt_item_id(jarr.getJSONObject(i).getInt("hunt_item_id"));
//                                    huntItems.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
//                                    huntItems.setTitle(jarr.getJSONObject(i).getString("title"));
//                                    huntItems.setDescription(jarr.getJSONObject(i).getString("description"));
//                                    huntItems.setPoint_value(jarr.getJSONObject(i).getString("point_value"));
//                                    huntItems.setUploaded_file_path(jarr.getJSONObject(i).getString("uploaded_file_path"));
//                                    huntItems.setUploaded_file_type(jarr.getJSONObject(i).getString("uploaded_file_type"));
//                                    huntItems.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
//                                    huntItems.setPrice_of_hunt_item(jarr.getJSONObject(i).getString("price_of_hunt_item"));
//                                    huntItems.setPrebuild_hunt_item_id(jarr.getJSONObject(i).getString("prebuild_hunt_item_id"));
//                                    huntItemsList.add(huntItems);
//                                }
//                                adapter = new HuntItemsAdapter(HuntItemsActivity.this, huntItemsList);
//                                recyclerView.setAdapter(adapter);

                            } else {

                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(HuntItemsActivity.this, Constants.MESSAGE);
                            }

                            Constants.dismissProgressDialog(HuntItemsActivity.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(HuntItemsActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(HuntItemsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", hunt_id);
                params.put("hunt_item_id", String.valueOf(hunt_item_id));
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(HuntItemsActivity.this, MyHunts_Activity.class);
        in.putExtra("ComeFrom", "");
        startActivity(in);
        finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.addMoreItemsBTN:
                intent = new Intent(this, AddHuntedItemsActivity.class);
                intent.putExtra("hunt_id", hunt_id);
                intent.putExtra("ComeFrom","HuntItem");
                startActivity(intent);
                finish();
                break;
            case R.id.hunIsReadyBTN:
                intent = new Intent(this, MyHunts_Activity.class);
                intent.putExtra("ComeFrom", "");
                startActivity(intent);
                finish();
                break;
        }
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }
}
