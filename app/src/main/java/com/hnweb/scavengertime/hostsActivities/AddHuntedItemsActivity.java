package com.hnweb.scavengertime.hostsActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.PickerUtils;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

public class AddHuntedItemsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_VIDEO = 3;
    private static final int REQUEST_CAMERA = 5;
    public static final int FROM_GALLARY = 103;
    private String selectedPath = "";
    EditText itemNameET, descriptionET, pointValueET;
    ImageView uploadThumbIV;
    String itemName, description, pointValue;
    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    LinearLayout settingDrawerLL;
    String hunt_id, hunt_title;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String comeFrom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hunted_items);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        setToolbarDrawer();

        hunt_id = getIntent().getExtras().getString("hunt_id");
        hunt_title = getIntent().getExtras().getString("hunt_title");
        comeFrom = getIntent().getExtras().getString("ComeFrom");
        init();


    }


    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(AddHuntedItemsActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }

    public void init() {
        itemNameET = (EditText) findViewById(R.id.itemNameET);
        descriptionET = (EditText) findViewById(R.id.descriptionET);
        pointValueET = (EditText) findViewById(R.id.pointValueET);
        uploadThumbIV = (ImageView) findViewById(R.id.uploadThumbIV);
    }

//    private void chooseVideo() {
//        Intent intent = new Intent();
//        intent.setType("video/*");
//
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select a Video "), SELECT_VIDEO);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == FROM_GALLARY) {

                System.out.println("SELECT_IMAGE");
                Uri uri = data.getData();
                selectedPath = PickerUtils.gallaryPath(AddHuntedItemsActivity.this, uri);
                Glide.with(this).load(selectedPath).asBitmap().override(100,100).into(uploadThumbIV);
//                try {
//                    Bitmap bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
//                    uploadThumbIV.setImageBitmap(bm);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

//                Glide.with(AddHuntedItemsActivity.this).load(new File(uri.getPath())).into(uploadThumbIV);
//                File file = new File(selectedPath);
//                uploadThumbIV.setImageURI(Uri.parse(selectedPath));


            } else if (requestCode == REQUEST_CAMERA) {

                System.out.println("REQUEST_CAMERA");
                selectedPath = PickerUtils.camerapath(uploadThumbIV);
                Glide.with(this).load(selectedPath).asBitmap().override(100,100).into(uploadThumbIV);

            } else if (requestCode == SELECT_VIDEO) {

                System.out.println("SELECT_VIDEO");
                Uri uri = data.getData();
                selectedPath = PickerUtils.getGalleryVideoPath(AddHuntedItemsActivity.this, uri);
                Bitmap bmThumbnail;

                // MICRO_KIND: 96 x 96 thumbnail
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(selectedPath,
                        MediaStore.Video.Thumbnails.MICRO_KIND);
                uploadThumbIV.setImageBitmap(bmThumbnail);

            }
        }
//        if (resultCode == RESULT_OK) {
//            if (requestCode == SELECT_VIDEO) {
//                System.out.println("SELECT_VIDEO");
//                Uri selectedImageUri = data.getData();
//                selectedPath = getPath(selectedImageUri);
////                textView.setText(selectedPath);
////                uploadVideo();
////                imageVideoUpload(selectedPath);
//            } else if (requestCode == SELECT_IMAGE) {
//                System.out.println("SELECT_IMAGE");
//                Uri selectedImageUri = data.getData();
//                selectedPath = getPath(selectedImageUri);
//            } else if (requestCode == REQUEST_CAMERA) {
//                System.out.println("REQUEST_CAMERA");
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                Uri selectedImageUri = getImageUri(getApplicationContext(), photo);
//                selectedPath = getImagePath(selectedImageUri);
//            } else if (requestCode == REQUEST_VIDEO) {
//                System.out.println("REQUEST_VIDEO");
//                Uri selectedImageUri = data.getData();
//                selectedPath = getPath(selectedImageUri);
//            }
//        }
    }

//    public String getPath(Uri uri) {
//        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//        cursor.moveToFirst();
//        String document_id = cursor.getString(0);
//        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
//        cursor.close();
//
//        cursor = getContentResolver().query(
//                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
//                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
//        cursor.moveToFirst();
//        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
//        cursor.close();
//
//        return path;
//    }
//
//    public String getImagePath(Uri uri) {
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cursor = managedQuery(uri, projection, null, null,
//                null);
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        cursor.moveToFirst();
//
//        String selectedImagePath = cursor.getString(column_index);
//
//        return selectedImagePath;
//    }
//
//
//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }


//    private void selectImage() {
//        final CharSequence[] items = {"Take Photo", "Take Video", "Choose Image from Gallery", "Choose Video from Gallery",
//                "Cancel"};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(AddHuntedItemsActivity.this);
//        builder.setTitle("Add Photo!");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (items[item].equals("Take Photo")) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, REQUEST_CAMERA);
//                } else if (items[item].equals("Take Video")) {
//                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                    startActivityForResult(intent, REQUEST_VIDEO);
//                } else if (items[item].equals("Choose Image from Gallery")) {
//                    Intent intent = new Intent(
//                            Intent.ACTION_PICK,
//                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    intent.setType("image/*");
//                    startActivityForResult(
//                            Intent.createChooser(intent, "Select File"),
//                            SELECT_IMAGE);
//
//                } else if (items[item].equals("Choose Video from Gallery")) {
////                    Intent intent = new Intent(
////                            Intent.ACTION_PICK,
////                            MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
////                    intent.setType("video/*");
////                    startActivityForResult(
////                            Intent.createChooser(intent, "Select File"),
////                            SELECT_VIDEO);
//
//                    chooseVideo();
//                } else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uploadBTN:
//                chooseVideo();
                PickerUtils.selectImage(AddHuntedItemsActivity.this, REQUEST_CAMERA, FROM_GALLARY, SELECT_VIDEO);
//                selectImage();
                break;
            case R.id.exist_huntBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.addAnotherItemBTN:
                callService();
                break;
            case R.id.finishedBTN:
                callService();
                break;
//            case R.id.logoutBTN:
//                Constants.logout_fun(this);
//                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
        }
    }

    public void callService() {
        getInput();
        if (Validations.strslength(itemName)) {
            if (Validations.strslength(description)) {
                if (Validations.strslength(pointValue)) {
//                    if (selectedPath.length() > 0) {
                    if (CheckConnectivity.checkInternetConnection(AddHuntedItemsActivity.this)) {
                        imageVideoUpload(selectedPath, hunt_id, itemName, description, pointValue);
                    }

//                    } else {
//                        Toast.makeText(AddHuntedItemsActivity.this, "Please select image or video", Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    pointValueET.setError("Please enter point value");
                }
            } else {
                descriptionET.setError("Please enter description");
            }
        } else {
            itemNameET.setError("Please enter item name");
        }
    }

    public void getInput() {
        itemName = itemNameET.getText().toString().trim();
        description = descriptionET.getText().toString().trim();
        pointValue = pointValueET.getText().toString().trim();
    }


    public void imageVideoUpload(String selectedPath1, final String hunt_id, final String itemName, String description, final String pointValue) {
        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, Urls.addHuntItems,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);

                        JSONObject jobj = null;
                        try {
                            jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");

                            Toast.makeText(AddHuntedItemsActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();

//                            Intent intent = new Intent(AddHuntedItemsActivity.this, DashboardActivity.class);
//                            startActivity(intent);
//                            finish();

                            if (comeFrom.equalsIgnoreCase("HuntItem")) {
                                Intent intent = new Intent(AddHuntedItemsActivity.this, HuntItemsActivity.class);
                                intent.putExtra("huntId", hunt_id);
                                intent.putExtra("huntTitle", hunt_title);
//                intent.putExtra("description",actList.get(position).getHunt_description());
//                intent.putExtra("pointValue",actList.get(position).getP)
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(AddHuntedItemsActivity.this, DashboardActivity.class);
//                                intent.putExtra("huntId", hunt_id);
//                                intent.putExtra("huntTitle", hunt_title);
//                intent.putExtra("description",actList.get(position).getHunt_description());
//                intent.putExtra("pointValue",actList.get(position).getP)
                                startActivity(intent);
                                finish();
                            }


                            itemNameET.setText("");
                            descriptionET.setText("");
                            pointValueET.setText("");
                            selectedPath = "";

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


//                        Toast.makeText(AddHuntedItemsActivity.this,)
                        Constants.progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(AddHuntedItemsActivity.this,"Please check your internet connection...!!!");
//                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

//        try {
//            smr.addFile("selectedPath", selectedPath1);
//            System.out.println("Arsh Op" + selectedPath1);
//        } catch (Exception e) {
//
//        }
        smr.addStringParam("hunt_id", hunt_id);
        smr.addStringParam("title", itemName);
        smr.addStringParam("description", description);
        smr.addStringParam("point_value", pointValue);
        smr.addFile("uploaded_file_path", selectedPath1);

        Log.e("PARAMS ", selectedPath1);
        Log.e("PARAMS ", smr.toString());

        queue.add(smr);

    }


}
