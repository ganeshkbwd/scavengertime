package com.hnweb.scavengertime.hostsActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;
import com.hnweb.scavengertime.pojo.HuntItems;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class EditPointValueActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    LinearLayout settingDrawerLL;
    EditText itemNameET, descriptionET, pointValueET;
    Button uploadBTN;
    ArrayList<HuntItems> huntItemsList = new ArrayList<HuntItems>();
    int position;
    ImageView uploadFileIV;
    String itemName, description, pointValue;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    int login, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hunts_item);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        position = getIntent().getExtras().getInt("position");
        huntItemsList = (ArrayList<HuntItems>) getIntent().getExtras().getSerializable("hunt_item");
        init(huntItemsList, position);
        setToolbarDrawer();


    }

    public void setData(ArrayList<HuntItems> huntItemsList, int position) {
        itemNameET.setText(huntItemsList.get(position).getTitle());
        descriptionET.setText(huntItemsList.get(position).getDescription());
        pointValueET.setText(huntItemsList.get(position).getPoint_value());
//        Glide.with(this).load(huntItemsList.get(position).getUploaded_file_path());
    }

    public void init(ArrayList<HuntItems> huntItemsList, int position) {
        itemNameET = (EditText) findViewById(R.id.itemNameET);
        descriptionET = (EditText) findViewById(R.id.descriptionET);
        pointValueET = (EditText) findViewById(R.id.pointValueET);
        uploadBTN = (Button) findViewById(R.id.uploadBTN);
        uploadFileIV = (ImageView) findViewById(R.id.uploadFileIV);
        setData(huntItemsList, position);
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(EditPointValueActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.logoutBTN:
//                Constants.logout_fun(this);
//                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.saveBTN:
                getData();

                break;
            case R.id.cancelBTN:
                super.onBackPressed();
                break;

        }
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }

    public void getData() {
        String hunt_item_id = String.valueOf(huntItemsList.get(position).getHunt_item_id());
        itemName = itemNameET.getText().toString().trim();
        description = descriptionET.getText().toString().trim();
        if (Validations.strslength(pointValueET.getText().toString().trim())) {
            pointValue = pointValueET.getText().toString().trim();
        } else {
            pointValueET.setError("Please enter point value");
        }
        if (CheckConnectivity.checkInternetConnection(this)) {
            editPointValueWebservice(hunt_item_id, itemName, description, pointValue);
        }

    }

    public void editPointValueWebservice(final String hunt_item_id, final String itemName, final String description, final String pointValue) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.editHuntItem,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
//                                JSONArray jarr = jobj.getJSONArray("hunt_items");
//                                for (int i = 0; i < jarr.length(); i++) {
////
//                                }
                                Constants.MESSAGE = jobj.getString("message");
                                Toast.makeText(EditPointValueActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.dismissProgressDialog(EditPointValueActivity.this);
                                Intent intent = new Intent(EditPointValueActivity.this, MyHunts_Activity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(EditPointValueActivity.this, Constants.MESSAGE);
                                Constants.progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(EditPointValueActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(EditPointValueActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("hunt_item_id", hunt_item_id);
                params.put("title", itemName);
                params.put("description", description);
                params.put("point_value", pointValue);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MyHunts_Activity.class);
        intent.putExtra("ComeFrom", "");
        Constants.SELECTED = "Active";
        startActivity(intent);
        finish();
    }
}
