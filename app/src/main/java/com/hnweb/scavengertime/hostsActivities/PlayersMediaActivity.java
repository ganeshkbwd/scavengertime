package com.hnweb.scavengertime.hostsActivities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.PlayersMediaAdapter;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;
import com.hnweb.scavengertime.pojo.PlayersMedia;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class PlayersMediaActivity extends AppCompatActivity implements View.OnClickListener {
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    Toolbar toolbar;
    Map<String, String> headerMap;
    TextView huntNameTV, startedOnTV, endedOnTV, starttimeTV, durationTV, huntStatusTV;
    RecyclerView recyclerView;
    ArrayList<PlayersMedia> pmList = new ArrayList<PlayersMedia>();
    ImageView proIV;
    TextView nameTV, collectedItemsTV, totalPointsTV, timeUsedTV;
    String join_hunt_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_media);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        headerMap = (Map<String, String>) getIntent().getExtras().getSerializable("Map");
        join_hunt_id = getIntent().getExtras().getString("join_hunt_id");

        setToolbarDrawer();
        init();

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

    }

    public void init() {
        huntNameTV = (TextView) findViewById(R.id.huntNameTV);
        startedOnTV = (TextView) findViewById(R.id.startedOnTV);
        endedOnTV = (TextView) findViewById(R.id.endedOnTV);
        starttimeTV = (TextView) findViewById(R.id.starttimeTV);
        durationTV = (TextView) findViewById(R.id.durationTV);
        huntStatusTV = (TextView) findViewById(R.id.huntStatusTV);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(PlayersMediaActivity.this));
        nameTV = (TextView) findViewById(R.id.nameTV);
        collectedItemsTV = (TextView) findViewById(R.id.collectedItemsTV);
        totalPointsTV = (TextView) findViewById(R.id.totalPointsTV);
        timeUsedTV = (TextView) findViewById(R.id.timeUsedTV);
        proIV = (ImageView) findViewById(R.id.proIV);
        setData();
    }

    public void setData() {
        huntNameTV.setText(headerMap.get("hunt_title"));
        startedOnTV.setText(headerMap.get("hunt_started_on"));
        endedOnTV.setText(headerMap.get("hunt_ended_on"));
        starttimeTV.setText(headerMap.get("start_time"));
        durationTV.setText(headerMap.get("total_duration"));
        huntStatusTV.setText(headerMap.get("hunt_status"));
        playerMediaWebservice();
    }


    public void playerMediaWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.playerMedia,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {

                                String hunt_id = jobj.getString("hunt_id");
                                String user_id = jobj.getString("user_id");
                                String total_points = jobj.getString("total_points");
                                String collected_items = jobj.getString("collected_items");
                                String time_used = jobj.getString("time_used");
                                String full_name = jobj.getString("full_name");
                                String profile_photo = jobj.getString("profile_photo");
                                String hunt_started_on = jobj.getString("hunt_started_on");
                                String hunt_ended_on = jobj.getString("hunt_ended_on");
                                String hunt_status = jobj.getString("hunt_status");
                                String total_duration = jobj.getString("total_duration");

                                nameTV.setText(full_name);
                                collectedItemsTV.setText("Collected Items: " + collected_items);
                                totalPointsTV.setText("Total Points: " + total_points);
                                timeUsedTV.setText("Time Used: " + time_used);
                                if (Validations.strslength(profile_photo))
                                    Glide.with(PlayersMediaActivity.this).load(profile_photo).into(proIV);

                                JSONArray jarr = jobj.getJSONArray("collected_hunt_items_list");

                                pmList.clear();
                                for (int i = 0; i < jarr.length(); i++) {
                                    PlayersMedia pm = new PlayersMedia();
                                    pm.setUmfphi_id(jarr.getJSONObject(i).getString("umfphi_id"));
                                    pm.setPlayer_user_id(jarr.getJSONObject(i).getString("player_user_id"));
                                    pm.setHunt_id(jarr.getJSONObject(i).getString("hunt_id"));
                                    pm.setHunt_item_id(jarr.getJSONObject(i).getString("hunt_item_id"));
                                    pm.setFile_path(jarr.getJSONObject(i).getString("file_path"));
                                    pm.setFile_type(jarr.getJSONObject(i).getString("file_type"));
                                    pm.setUploaded_datetime(jarr.getJSONObject(i).getString("uploaded_datetime"));
                                    pm.setPoint_value(jarr.getJSONObject(i).getString("point_value"));
                                    pm.setHunt_item_name(jarr.getJSONObject(i).getString("hunt_item_name"));

                                    pmList.add(pm);
                                }
                                recyclerView.setAdapter(new PlayersMediaAdapter(PlayersMediaActivity.this, pmList));

                            }

                            Constants.dismissProgressDialog(PlayersMediaActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.dismissProgressDialog(PlayersMediaActivity.this);
                toastDialog("Please Check Internet Connection..!!!");
//                Toast.makeText(PlayersMediaActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("join_hunt_id", join_hunt_id);
//                params.put("hunt_id", hunt_id);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.returnBTN:
                onBackPressed();
                break;
        }
    }

    public void toastDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
//                        if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
//                            Constants.OK = 0;
//                            Intent intent = new Intent(context, DashboardActivity.class);
////                                    intent.putExtras(bundle);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            Constants.OK = 0;
//                            Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
////                                    intent.putExtras(bundle);
//                            startActivity(intent);
//                            finish();
//                        }
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
