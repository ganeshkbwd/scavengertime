package com.hnweb.scavengertime.hostsActivities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.HuntItemsAdapter;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.HuntItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class AllHuntItemsActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView allHuntItemListRV;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    int login, user_id;
    ArrayList<HuntItems> huntItemsList = new ArrayList<HuntItems>();
    HuntItemsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_hunt_items);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        allHuntItemListRV = (RecyclerView) findViewById(R.id.allHuntItemListRV);
        allHuntItemListRV.setLayoutManager(new LinearLayoutManager(this));
        huntItemsWebservice();

    }


    public void huntItemsWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.allHuntItems,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
                                huntItemsList.clear();
                                JSONArray jarr = jobj.getJSONArray("All_User_Hunts_Items");
                                for (int i = 0; i < jarr.length(); i++) {
                                    HuntItems huntItems = new HuntItems();
                                    huntItems.setHunt_item_id(jarr.getJSONObject(i).getInt("hunt_item_id"));
                                    huntItems.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
                                    huntItems.setTitle(jarr.getJSONObject(i).getString("title"));
                                    huntItems.setDescription(jarr.getJSONObject(i).getString("description"));
                                    huntItems.setPoint_value(jarr.getJSONObject(i).getString("point_value"));
                                    huntItems.setUploaded_file_path(jarr.getJSONObject(i).getString("uploaded_file_path"));
                                    huntItems.setUploaded_file_type(jarr.getJSONObject(i).getString("uploaded_file_type"));
                                    huntItems.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    huntItems.setPrice_of_hunt_item(jarr.getJSONObject(i).getString("price_of_hunt_item"));
//                                    huntItems.setPrebuild_hunt_item_id(jarr.getJSONObject(i).getString("prebuild_hunt_item_id"));
                                    huntItemsList.add(huntItems);
                                }
                                adapter = new HuntItemsAdapter(AllHuntItemsActivity.this, huntItemsList);
                                allHuntItemListRV.setAdapter(adapter);

                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(AllHuntItemsActivity.this, Constants.MESSAGE);
//                                Constants.progressDialog.dismiss();
                            }
                            Constants.dismissProgressDialog(AllHuntItemsActivity.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(AllHuntItemsActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(HuntItemsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
//                params.put("hunt_id", hunt_id);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.createHuntItemBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.addMoreItemsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hunIsReadyBTN:
                Constants.underDevelopment(this);
                break;
        }
    }
}
