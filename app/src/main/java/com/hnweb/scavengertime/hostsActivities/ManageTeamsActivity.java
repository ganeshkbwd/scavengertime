package com.hnweb.scavengertime.hostsActivities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.MembersListAdapter;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.TeamMembers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class ManageTeamsActivity extends AppCompatActivity implements View.OnClickListener {

    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    ArrayList<TeamMembers> teamMemberseList = new ArrayList<>();
    RecyclerView membersListRV;
    String team_id;
    TextView noRecordFound;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_teams);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        membersListRV = (RecyclerView) findViewById(R.id.membersListRV);
        membersListRV.setLayoutManager(new LinearLayoutManager(ManageTeamsActivity.this));
        noRecordFound = (TextView) findViewById(R.id.noRecordTV);

        teamWebservice(user_id, ManageTeamsActivity.this);
    }

    public void teamWebservice(final int user_id, final Activity activity) {

        Constants.showProgress(activity);
        RequestQueue queue = Volley.newRequestQueue(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getTeamDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {
                                teamMemberseList.clear();
//                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
//                                Toast.makeText(CreateTeamActivity.this, "Team Created succesfully.Please add team members", Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();

                                team_id = jobj.getString("team_id");
                                String teamName = jobj.getString("team_name");
                                String team_photo = jobj.getString("team_photo");
                                String createdby_userid = jobj.getString("createdby_userid");

                                Object object = jobj.get("team_members");
                                if (object instanceof JSONArray) {
                                    JSONArray teamArr = jobj.getJSONArray("team_members");

                                    for (int i = 0; i < teamArr.length(); i++) {
                                        TeamMembers tm = new TeamMembers();
                                        tm.setTeam_member_id(teamArr.getJSONObject(i).getString("team_member_id"));
                                        tm.setTeam_id(teamArr.getJSONObject(i).getString("team_id"));
                                        tm.setCreatedby_userid(teamArr.getJSONObject(i).getString("createdby_userid"));
                                        tm.setTeam_member_name(teamArr.getJSONObject(i).getString("team_member_name"));
                                        teamMemberseList.add(tm);

                                    }
                                    noRecordFound.setVisibility(View.GONE);
                                    membersListRV.setVisibility(View.VISIBLE);
                                    membersListRV.setAdapter(new MembersListAdapter(ManageTeamsActivity.this, teamMemberseList));

                                } else {
                                    noRecordFound.setVisibility(View.VISIBLE);
                                    membersListRV.setVisibility(View.GONE);
                                    String team_members = jobj.getString("team_members");
                                    Log.e("team_members", team_members);
                                }

                            } else {
                                Toast.makeText(activity, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();//

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(activity, "Please Check Internet Connection..!!!");
//                Toast.makeText(ChangePasswordActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("createdby_userid", String.valueOf(user_id));
//                params.put("team_name", teamName);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addBTN:
                Intent intent = new Intent(this, AddTeamMemberActivity.class);
                intent.putExtra("team_id", team_id);
                intent.putExtra("team_member_name", "");
                intent.putExtra("team_member_id", "");
                startActivity(intent);

                break;
//            case R.id.continueBTN:
//
//                break;
        }
    }
}
