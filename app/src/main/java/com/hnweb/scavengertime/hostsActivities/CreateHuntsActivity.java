package com.hnweb.scavengertime.hostsActivities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.PickerUtils;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class CreateHuntsActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    EditText uniqueNameET, descriptionET, firstPrizeET, secondPrizeET, thirdPrizeET;

    CheckBox timedCB;
    TextView huntStrtTimeET, huntEndTimeET;
    String uniqueName, description, huntStrtTime,
            huntEndTime, firstPrize, secondPrize, thirdPrize, is_timed_hunt;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    private int mYear, mMonth, mDay, mHour, mMinute;
    private static final int SELECT_FILE = 11;
    private static final int FROM_GALLARY = 103;
    private static final int REQUEST_CAMERA = 5;
    private String encodedImage1 = "", encodedImage2 = "", encodedImage3 = "", encodedImage = "";
    public static int AVAIL = 0, IMAGESET = 0;
    private Bitmap profileImage;
    ImageView firstPrizeIV, secondPrizeIV, thirdPrizeIV;
    int IMAGE = 0, TIME = 0;
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextInputLayout uniqueNameTIL, descriptionTIL, firstPrizeTIL, secondPrizeTIL, thirdPrizeTIL, huntStrtTimeTIL, huntEndTimeTIL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_hunts);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        setToolbarDrawer();
        init();

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(CreateHuntsActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    public void init() {
        uniqueNameET = (EditText) findViewById(R.id.uniqueNameET);
        descriptionET = (EditText) findViewById(R.id.descriptionET);
//        uniqueNameET = (EditText) findViewById(R.id.uniqueNameET);
        huntStrtTimeET = (TextView) findViewById(R.id.huntStrtTimeET);
        huntEndTimeET = (TextView) findViewById(R.id.huntEndTimeET);
        firstPrizeET = (EditText) findViewById(R.id.firstPrizeET);
        secondPrizeET = (EditText) findViewById(R.id.secondPrizeET);
        thirdPrizeET = (EditText) findViewById(R.id.thirdPrizeET);
        timedCB = (CheckBox) findViewById(R.id.timedCB);
        firstPrizeIV = (ImageView) findViewById(R.id.firstPrizeIV);
        secondPrizeIV = (ImageView) findViewById(R.id.secondPrizeIV);
        thirdPrizeIV = (ImageView) findViewById(R.id.thirdPrizeIV);

//        uniqueNameTIL = (TextInputLayout) findViewById(R.id.uniqueNameTIL);
//        descriptionTIL = (TextInputLayout) findViewById(R.id.descriptionTIL);
//        huntStrtTimeTIL = (TextInputLayout) findViewById(R.id.huntStrtTimeTIL);
//        huntEndTimeTIL = (TextInputLayout) findViewById(R.id.huntEndTimeTIL);
//        firstPrizeTIL = (TextInputLayout) findViewById(R.id.firstPrizeTIL);
//        secondPrizeTIL = (TextInputLayout) findViewById(R.id.secondPrizeTIL);
//        thirdPrizeTIL = (TextInputLayout) findViewById(R.id.thirdPrizeTIL);

//        uniqueNameTIL = (EditText) findViewById(R.id.uniqueNameTIL);
//        uniqueNameTIL = (EditText) findViewById(R.id.uniqueNameTIL);
//        uniqueNameTIL = (EditText) findViewById(R.id.uniqueNameTIL);


        timedCB.setOnCheckedChangeListener(this);
        huntStrtTimeET.setOnClickListener(this);
        huntEndTimeET.setOnClickListener(this);

        firstPrizeIV.setOnClickListener(this);
        secondPrizeIV.setOnClickListener(this);
        thirdPrizeIV.setOnClickListener(this);
    }

    public void getUserInput() {

        uniqueName = uniqueNameET.getText().toString().trim();
        description = descriptionET.getText().toString().trim();
        huntStrtTime = huntStrtTimeET.getText().toString().trim();
        huntEndTime = huntEndTimeET.getText().toString().trim();

        try {
            huntStrtTime = String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(huntStrtTime)));
            huntEndTime = String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(huntEndTime)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        firstPrize = firstPrizeET.getText().toString().trim();
        secondPrize = secondPrizeET.getText().toString().trim();
        thirdPrize = thirdPrizeET.getText().toString().trim();

        if (timedCB.isChecked()) {

            is_timed_hunt = "Yes";
        } else {

            is_timed_hunt = "No";


        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.purchase_huntBTN:
//                Constants.underDevelopment(this);
                intent = new Intent(this, PreBuildHuntsActivity.class);
                startActivity(intent);
                break;
            case R.id.continueBTN:
                getUserInput();
                if (Validations.strslength(uniqueName)) {
                    if (Validations.strslength(description)) {
//                        if (Validations.strslength(huntStrtTime)) {
//                            if (Validations.strslength(huntEndTime)) {
//                                if (Validations.strslength(firstPrize)){
//                                    if (Validations.strslength(encodedImage1)){
                        if (CheckConnectivity.checkInternetConnection(this)) {
                            createHuntWebservice();
                        }
//                                    }else {
//                                        Toast.makeText(CreateHuntsActivity.this,"Please select prize image.",Toast.LENGTH_SHORT).show();
//                                    }
//                                }else {
//                                    firstPrizeET.setError("Please enter first prize.");
//                                }
//                            } else {
//                                huntEndTimeET.setError("Please select end time.");
//                            }
//                        } else {
//                            huntStrtTimeET.setError("Please select start time.");
//                        }
                    } else {
//                        descriptionTIL.setErrorEnabled(true);
//                        descriptionTIL.setError("Please enter description.");
                        descriptionET.setError("Please enter description.");
                    }
                } else {
//                    uniqueNameTIL.setErrorEnabled(true);
//                    uniqueNameTIL.setError("Please enter unique name.");
                    uniqueNameET.setError("Please enter unique name.");
//                    uniqueNameET.setBackgroundColor(Color.WHITE);
                }


                break;

            case R.id.huntStrtTimeET:
                dateTimeDialog(1);
                break;
            case R.id.huntEndTimeET:
                dateTimeDialog(2);
                break;
            case R.id.firstPrizeIV:
                IMAGE = 1;
                PickerUtils.selectImageDialog(CreateHuntsActivity.this, REQUEST_CAMERA, FROM_GALLARY);
//                selectImage();

                break;
            case R.id.secondPrizeIV:
                IMAGE = 2;
                PickerUtils.selectImageDialog(CreateHuntsActivity.this, REQUEST_CAMERA, FROM_GALLARY);
//                selectImage();
                break;
            case R.id.thirdPrizeIV:
                IMAGE = 3;
                PickerUtils.selectImageDialog(CreateHuntsActivity.this, REQUEST_CAMERA, FROM_GALLARY);
//                selectImage();
                break;
//            case R.id.logoutBTN:
//                Constants.logout_fun(this);
//                break;

        }
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }

//    public void createHuntWebservice() {
////        getUserInput();
//        Constants.showProgress(this);
//        RequestQueue queue = Volley.newRequestQueue(this);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.createHunt,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            Log.e("RESPONSE", response);
//                            JSONObject jobj = new JSONObject(response);
//                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
//                            Constants.MESSAGE = jobj.getString("message");
//                            String hunt_id = jobj.getString("hunt_id");
//                            if (Constants.MESSAGE_CODE==1){
//                                encodedImage1 = "";
//                                encodedImage2 = "";
//                                encodedImage3 = "";
//                                Intent intent = new Intent(CreateHuntsActivity.this, AddHuntedItemsActivity.class);
//                                intent.putExtra("hunt_id", hunt_id);
//                                startActivity(intent);
//                            }
//
//
//
//                            Constants.progressDialog.dismiss();
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Constants.progressDialog.dismiss();
//                Toast.makeText(CreateHuntsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
//            }
//
//
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                //Converting Bitmap to String
//                Map<String, String> params = new Hashtable<String, String>();
//
//                params.put("hunt_title", uniqueName);
//                params.put("hunt_description", description);
//                params.put("start_date_time", huntStrtTime);
//                params.put("end_date_time", huntEndTime);
//                params.put("first_prize_text", firstPrize);
//                params.put("first_prize_upload_file_path", encodedImage1);
//                params.put("second_prize_text", secondPrize);
//                params.put("second_prize_upload_file_path", encodedImage2);
//                params.put("third_prize_text", thirdPrize);
//                params.put("third_prize_upload_file_path", encodedImage3);
//                params.put("is_timed_hunt", is_timed_hunt);
//                params.put("user_id", String.valueOf(user_id));
//
//                Log.e("PARAMS", params.toString());
//                //returning parameters
//                return params;
//            }
//        };
//
//        queue.add(stringRequest);
//
//
//    }


    public void dateTimeDialog(final int i) {

// Get Current Date
        final TimeZone timeZone = TimeZone.getTimeZone("UTC");
        final Calendar c = Calendar.getInstance(timeZone);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Log.e("DATE", (monthOfYear + 1) + "-" + dayOfMonth + "-" + year);
//                        String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        String date = (monthOfYear + 1) + "-" + dayOfMonth + "-" + year;
                        timePicker(date, i, timeZone);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void timePicker(final String date, final int i, TimeZone timeZone) {
        // Get Current Time
        final TimeZone timeZone1 = TimeZone.getTimeZone("UTC");
        final Calendar c = Calendar.getInstance(timeZone1);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Log.e("Time ", hourOfDay + ":" + minute);
                        String Time = hourOfDay + ":" + minute;
                        Log.e("Time", date + " " + Time);
                        if (i == 1) {
                            huntStrtTimeET.setText(date + " " + Time + ":00");
                        } else if (i == 2) {
                            huntEndTimeET.setText(date + " " + Time + ":00");
                        }

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FROM_GALLARY) {

                System.out.println("SELECT_IMAGE");
                Uri uri = data.getData();
                if (IMAGE == 1) {
                    encodedImage1 = PickerUtils.gallaryPath(CreateHuntsActivity.this, uri);
                    Glide.with(this).load(encodedImage1).asBitmap().override(70, 70).into(firstPrizeIV);
//                    firstPrizeIV.setImageBitmap(base64toBitmap());
                } else if (IMAGE == 2) {
                    encodedImage2 = PickerUtils.gallaryPath(CreateHuntsActivity.this, uri);
                    Glide.with(this).load(encodedImage2).asBitmap().override(70, 70).into(secondPrizeIV);
//                    secondPrizeIV.setImageBitmap(base64toBitmap());
                } else if (IMAGE == 3) {
                    encodedImage3 = PickerUtils.gallaryPath(CreateHuntsActivity.this, uri);
                    Glide.with(this).load(encodedImage3).asBitmap().override(70, 70).into(thirdPrizeIV);
//                    thirdPrizeIV.setImageBitmap(base64toBitmap());
                }

            } else if (requestCode == REQUEST_CAMERA) {

                System.out.println("REQUEST_CAMERA");
                if (IMAGE == 1) {
                    encodedImage1 = PickerUtils.cameraPath();
                    Glide.with(this).load(encodedImage1).asBitmap().override(70, 70).into(firstPrizeIV);
//                    firstPrizeIV.setImageBitmap(base64toBitmap());
                } else if (IMAGE == 2) {
                    encodedImage2 = PickerUtils.cameraPath();
                    Glide.with(this).load(encodedImage2).asBitmap().override(70, 70).into(thirdPrizeIV);
//                    secondPrizeIV.setImageBitmap(base64toBitmap());
                } else if (IMAGE == 3) {
                    encodedImage3 = PickerUtils.cameraPath();
                    Glide.with(this).load(encodedImage3).asBitmap().override(70, 70).into(thirdPrizeIV);
//                    thirdPrizeIV.setImageBitmap(base64toBitmap());
                }
//                filePath = PickerUtils.camerapath(uploadIV);
//                onCaptureImageResult(data);

            }

        }
    }

//    public byte[] base64toBitmap() {
//        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
//        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//        return decodedString;
//    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            huntStrtTimeET.setEnabled(true);
            huntEndTimeET.setEnabled(true);
        } else {
            huntStrtTimeET.setText("");
            huntEndTimeET.setText("");
            huntStrtTimeET.setEnabled(false);
            huntEndTimeET.setEnabled(false);

        }
    }

    public void createHuntWebservice() {
        Constants.showProgress(this);

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, Urls.createHunt,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);

                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");
                            String hunt_id = jobj.getString("hunt_id");
                            if (Constants.MESSAGE_CODE == 1) {
                                encodedImage1 = "";
                                encodedImage2 = "";
                                encodedImage3 = "";
                                Intent intent = new Intent(CreateHuntsActivity.this, AddHuntedItemsActivity.class);
                                intent.putExtra("hunt_id", hunt_id);
                                intent.putExtra("hunt_title", uniqueName);
                                intent.putExtra("ComeFrom", "Create");
                                startActivity(intent);
                            } else if (Constants.MESSAGE_CODE == 0) {
                                Constants.toastDialog(CreateHuntsActivity.this, jobj.getString("message"));
//                                Toast.makeText(CreateHuntsActivity.this,Constants.MESSAGE,Toast.LENGTH_SHORT).show();

                            }


                            Constants.progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


//                        Toast.makeText(AddHuntedItemsActivity.this,)
                        Constants.progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(CreateHuntsActivity.this, "Please check your internet connection.");
//                Toast.makeText(CreateHuntsActivity.this, "Please check your internet connection.", Toast.LENGTH_LONG).show();
            }
        });

        smr.addStringParam("hunt_title", uniqueName);
        smr.addStringParam("hunt_description", description);
        if (TextUtils.isEmpty(huntStrtTime)) {
            final TimeZone timeZone = TimeZone.getTimeZone("UTC");
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance(timeZone).getTime());

            huntStrtTime = date;
        }
        smr.addStringParam("start_date_time", huntStrtTime);
        smr.addStringParam("end_date_time", huntEndTime);
        smr.addStringParam("first_prize_text", firstPrize);
        smr.addFile("first_prize_upload_file_path", encodedImage1.trim());
        smr.addStringParam("second_prize_text", secondPrize);
        smr.addFile("second_prize_upload_file_path", encodedImage2.trim());
        smr.addStringParam("third_prize_text", thirdPrize);
        smr.addFile("third_prize_upload_file_path", encodedImage3.trim());
        smr.addStringParam("is_timed_hunt", is_timed_hunt);
        smr.addStringParam("user_id", String.valueOf(user_id));


        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(smr);

    }


}


