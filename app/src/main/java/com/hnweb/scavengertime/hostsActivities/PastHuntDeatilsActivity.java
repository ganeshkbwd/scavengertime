package com.hnweb.scavengertime.hostsActivities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.ParticipantsAdapter;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.Participants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class PastHuntDeatilsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView huntNameTV, startedOnTV, endedOnTV, starttimeTV, durationTV, huntStatusTV;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String hunt_id;
    ArrayList<Participants> pList = new ArrayList<Participants>();
    RecyclerView recyclerView;
    HashMap<String, String> rWP = new HashMap<String, String>();
    String FIRST = "", SECOND = "", THIRD = "";
    //1::First##2::Second##3::Third
    String join_hunt_id_with_rank;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_hunt_deatils);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        setToolbarDrawer();
        hunt_id = getIntent().getExtras().getString("hunt_id");
        init();
//        Constants.addRanks();

        if (CheckConnectivity.checkInternetConnection(this))
            allParticipantsWebservice();
    }

    public void init() {
        huntNameTV = (TextView) findViewById(R.id.huntNameTV);
        startedOnTV = (TextView) findViewById(R.id.startedOnTV);
        endedOnTV = (TextView) findViewById(R.id.endedOnTV);
        starttimeTV = (TextView) findViewById(R.id.starttimeTV);
        durationTV = (TextView) findViewById(R.id.durationTV);
        huntStatusTV = (TextView) findViewById(R.id.huntStatusTV);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(PastHuntDeatilsActivity.this));
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.winnersBTN:

                if (huntStatusTV.getText().toString().trim().equalsIgnoreCase("NotStarted") && huntStatusTV.getText().toString().trim().equalsIgnoreCase("")) {

                    toastDialog("You can not declare winners," +
                            "hunt status is Not started.");
//                    Toast.makeText(PastHuntDeatilsActivity.this, "You can not declare winners," +
//                            "hunt status is Not started.", Toast.LENGTH_SHORT).show();

                } else {
                    for (Map.Entry<String, String> entry : rWP.entrySet()) {
                        System.out.printf("%s -> %s%n", entry.getKey(), entry.getValue());
                        System.out.printf("%s -> %s%n", "1", entry.getValue());

                    }
                    if (rWP.get("1") != null) {
                        FIRST = rWP.get("1");
                        Log.e("FIRST : ", rWP.get("1"));
                    }

                    if (rWP.get("2") != null) {
                        SECOND = rWP.get("2");
                        Log.e("SECOND : ", rWP.get("2"));
                    }

                    if (rWP.get("3") != null) {
                        THIRD = rWP.get("3");
                        Log.e("THIRD : ", rWP.get("3"));
                    }

                    //1::First##2::Second##3::Third
                    join_hunt_id_with_rank = "1::" + FIRST + "##2::" + SECOND + "##3::" + THIRD;
                    declareWinnerWebservice(join_hunt_id_with_rank);
//                Iterator myVeryOwnIterator = rWP.keySet().iterator();
//                while(myVeryOwnIterator.hasNext()) {
//                    String key=(String)myVeryOwnIterator.next();
//                    String value=(String)rWP.get(key);
//                    Toast.makeText(this, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
//                }
//                Constants.underDevelopment(this);
                }


                break;
        }
    }


    public void allParticipantsWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.participantsOfHunt,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            String hunt_id = jobj.getString("hunt_id");
                            String hunt_title = jobj.getString("hunt_title");
                            String hunt_started_on = jobj.getString("hunt_started_on");
                            String hunt_ended_on = jobj.getString("hunt_ended_on");
                            String hunt_status = jobj.getString("hunt_status");
                            String total_duration = jobj.getString("total_duration");

                            huntNameTV.setText(hunt_title);
                            String[] sdte = hunt_started_on.split(" ");
                            String[] time = sdte[1].split(":");
                            String[] edte = hunt_ended_on.split(" ");
                            startedOnTV.setText("Started On: " + sdte[0]);
                            endedOnTV.setText("Ended On: " + edte[0]);
                            String period;
                            if (Integer.parseInt(time[1]) > 12) {
                                period = "pm";
                            } else {
                                period = "am";
                            }
                            starttimeTV.setText("Start Time: " + time[1] + period);
                            durationTV.setText("Duration: " + total_duration);
                            huntStatusTV.setText("Hunt Status: " + hunt_status);


                            if (Constants.MESSAGE_CODE == 1) {
                                JSONArray jarr = jobj.getJSONArray("hunt_items");


                                pList.clear();
                                for (int i = 0; i < jarr.length(); i++) {
                                    Participants p = new Participants();
                                    p.setJoin_hunt_id(jarr.getJSONObject(i).getString("join_hunt_id"));
                                    p.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
                                    p.setHunt_id(jarr.getJSONObject(i).getString("hunt_id"));
                                    p.setUser_id(jarr.getJSONObject(i).getString("user_id"));
                                    p.setJoined_date_time(jarr.getJSONObject(i).getString("joined_date_time"));
                                    p.setTake_datetime(jarr.getJSONObject(i).getString("take_datetime"));
                                    p.setFinish_datetime(jarr.getJSONObject(i).getString("finish_datetime"));
                                    p.setTotal_points(jarr.getJSONObject(i).getString("total_points"));
                                    p.setCollected_items(jarr.getJSONObject(i).getString("collected_items"));
                                    p.setTime_used_days(jarr.getJSONObject(i).getString("time_used_days"));
                                    p.setTime_used_hours(jarr.getJSONObject(i).getString("time_used_hours"));
                                    p.setTime_used_minutes(jarr.getJSONObject(i).getString("time_used_minutes"));
                                    p.setWinner_level(jarr.getJSONObject(i).getString("winner_level"));
                                    p.setIs_winner(jarr.getJSONObject(i).getString("is_winner"));
                                    p.setFull_name(jarr.getJSONObject(i).getString("full_name"));
                                    p.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));

                                    pList.add(p);
                                }
//                                preBuildHuntsAdapter = new PreBuildHuntsAdapter(PreBuildHuntsActivity.this, pbhList);
                                Map<String, String> headerMap = new HashMap<String, String>();
                                headerMap.put("hunt_id", hunt_id);
                                headerMap.put("hunt_title", huntNameTV.getText().toString().trim());
                                headerMap.put("hunt_started_on", startedOnTV.getText().toString().trim());
                                headerMap.put("hunt_ended_on", endedOnTV.getText().toString().trim());
                                headerMap.put("start_time", starttimeTV.getText().toString().trim());
                                headerMap.put("hunt_status", huntStatusTV.getText().toString().trim());
                                headerMap.put("total_duration", durationTV.getText().toString().trim());

                                recyclerView.setAdapter(new ParticipantsAdapter(PastHuntDeatilsActivity.this, pList, rWP, headerMap));
////                                Bundle b = new Bundle();
////                                b.putSerializable("pbhl", pbhList);
////                                Intent intent = new Intent(PreBuildHuntsActivity.this, PurchaseHuntActivity.class);
////                                intent.putExtras(b);
////                                startActivity(intent);

                                Constants.progressDialog.dismiss();
                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(PastHuntDeatilsActivity.this, Constants.MESSAGE);
                                Constants.progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                toastDialog("Please Check Internet Connection..!!!");
//                Toast.makeText(PastHuntDeatilsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", hunt_id);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void declareWinnerWebservice(final String join_hunt_id_with_rank) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.huntWinners,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
                                Intent intent = new Intent(PastHuntDeatilsActivity.this, WinnersActivity.class);
                                intent.putExtra("hunt_id", hunt_id);
                                startActivity(intent);
                                finish();
                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(PastHuntDeatilsActivity.this, Constants.MESSAGE);
                            }
                            Constants.dismissProgressDialog(PastHuntDeatilsActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                toastDialog("Please Check Internet Connection..!!!");
//                Toast.makeText(PastHuntDeatilsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("join_hunt_id_with_rank", join_hunt_id_with_rank);
                params.put("hunt_id", hunt_id);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void toastDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
//                        if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
//                            Constants.OK = 0;
//                            Intent intent = new Intent(context, DashboardActivity.class);
////                                    intent.putExtras(bundle);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            Constants.OK = 0;
//                            Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
////                                    intent.putExtras(bundle);
//                            startActivity(intent);
//                            finish();
//                        }
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

//    @Override
//    public void onBackPressed() {
//
//    }
}
