package com.hnweb.scavengertime.hostsActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.fragments.ActiveHuntsFragment;
import com.hnweb.scavengertime.fragments.PastHuntsFragment;
import com.hnweb.scavengertime.fragments.UpComingHuntsFragment;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.playersActivities.JoinHuntActivity;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyHuntsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    RadioButton huntedRB;
    RadioGroup huntsRG;
    int selectedId;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_hunts);
        setToolbarDrawer();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        viewPager.setOffscreenPageLimit(2);
        setupViewPager(viewPager);
        viewPager.setCurrentItem(0);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        huntsRG = (RadioGroup) findViewById(R.id.huntRG);
//        selectedId = huntsRG.getCheckedRadioButtonId();
        huntsRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedId = huntsRG.indexOfChild(findViewById(checkedId));
                switch (selectedId) {
                    case 0:

                        Constants.HUNT_TYPE = 0;
                        break;
                    case 1:
                        Constants.HUNT_TYPE = 1;
                        Constants.underDevelopment(MyHuntsActivity.this);
                        break;
                }
            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MyHuntsActivity.this,CreateHuntsActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        huntedRB = (RadioButton) findViewById(selectedId);

//        tabSelection();
//        finish();
//        startActivity(getIntent());

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

//    public void tabSelection() {
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                if (tab.getPosition() == 0) {
//                    if (Constants.HUNT_TYPE == 0)
//
//
//                } else if (tab.getPosition() == 1) {
//
//                } else if (tab.getPosition() == 2) {
//
//                }
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
//    }


//        @Override
//    public void onRestart()
//    {
//        super.onRestart();
//        finish();
//        startActivity(getIntent());
//    }
    @Override
    protected void onResume() {
        super.onResume();

    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//
////        viewPager.setCurrentItem(1);
//    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
    }

    private void setupTabIcons() {
        XmlResourceParser parser = getResources().getXml(R.xml.tab_text_color);
        ColorStateList colors = null;
        try {
            colors = ColorStateList.createFromXml(getResources(), parser);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Active Hunts");
        tabOne.setTextColor(colors);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("UpComing Hunts");
        tabTwo.setTextColor(colors);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Past Hunts");
        tabThree.setTextColor(colors);
        tabLayout.getTabAt(2).setCustomView(tabThree);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ActiveHuntsFragment(), "Active Hunts");
        adapter.addFragment(new UpComingHuntsFragment(), "UpComing Hunts");
        adapter.addFragment(new PastHuntsFragment(), "Past Hunts");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        //Field to fill with the selected phone number
//        final EditText textPhone = (EditText) findViewById(R.id.Phone);
        if (reqCode == 200) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    //Get the phone number id from the Uri
                    String id = uri.getLastPathSegment();
                    //Query the phone numbers for the selected phone number id
                    Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone._ID + "=?", new String[]{id}, null);
                    int phoneIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    //get the only phone number
                    if (c.moveToFirst()) {
                        String phone_no = c.getString(phoneIdx);
                        Log.e("phone", c.getString(phoneIdx));
                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        sendIntent.putExtra("sms_body", "Please join the " + Constants.HUNT_TITLE_SHARE + ".Your Code is " + Constants.HUNT_CODE_SHARE);
                        sendIntent.putExtra("address", phone_no);
                        sendIntent.setType("vnd.android-dir/mms-sms");
                        startActivityForResult(sendIntent, 300);

                    } else {
                        //no result
                        Toast noResultFound = Toast.makeText(MyHuntsActivity.this, "No phone number found", Toast.LENGTH_SHORT);
                        noResultFound.show();
                    }
                    c.close();
                }

            }

        } else if (reqCode == 300) {
            Constants.HUNT_TYPE = 0;
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logoutBTN:
                Constants.logout_fun(this);
                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.fab:
                Intent intent = new Intent(MyHuntsActivity.this, CreateHuntsActivity.class);
                startActivity(intent);
//                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, JoinHuntActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
