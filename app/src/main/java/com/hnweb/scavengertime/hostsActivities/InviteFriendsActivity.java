package com.hnweb.scavengertime.hostsActivities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.ActiveHuntsAdapter;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.ActiveHunts;
import com.hnweb.scavengertime.pojo.UpComingHunts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class InviteFriendsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<UpComingHunts> pastList = new ArrayList<UpComingHunts>();
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String user_type;
    Toolbar toolbar;
    int service = 0;
    ActiveHuntsAdapter adapter;
    ArrayList<ActiveHunts> actList = new ArrayList<ActiveHunts>();
    TextView noRecordTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
        setToolbarDrawer();
        recyclerView = (RecyclerView) findViewById(R.id.invitListRV);
        recyclerView.setLayoutManager(new LinearLayoutManager(InviteFriendsActivity.this));
        noRecordTV = (TextView) findViewById(R.id.noRecordTV);

        service = 1;
        Constants.SELECTED = "UpComing";
        huntsWebservice(Urls.myHostedHunts, "UpComing", service, InviteFriendsActivity.this);

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);


    }


    public void huntsWebservice(String urlHunts, final String status, final int service, final Activity context) {

        Constants.showProgress(context);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlHunts,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
                                JSONArray jarr = jobj.getJSONArray("response");
                                actList.clear();

                                for (int i = 0; i < jarr.length(); i++) {
                                    ActiveHunts act = new ActiveHunts();
                                    act.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
                                    act.setHunt_title(jarr.getJSONObject(i).getString("hunt_title"));
                                    act.setHunt_description(jarr.getJSONObject(i).getString("hunt_description"));
                                    act.setIs_timed_hunt(jarr.getJSONObject(i).getString("is_timed_hunt"));
                                    act.setStart_date_time(jarr.getJSONObject(i).getString("start_date_time"));
                                    act.setEnd_date_time(jarr.getJSONObject(i).getString("end_date_time"));
                                    act.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));

                                    actList.add(act);

                                }
                                if (actList.size() == 0) {
                                    recyclerView.setVisibility(View.GONE);
                                    noRecordTV.setVisibility(View.VISIBLE);
                                } else {
                                    noRecordTV.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    adapter = new ActiveHuntsAdapter(InviteFriendsActivity.this, actList, user_type, status, service, user_id, "invite");
                                    recyclerView.setAdapter(adapter);
                                }


                            } else {
                                actList.clear();
                                recyclerView.setVisibility(View.GONE);
                                noRecordTV.setVisibility(View.VISIBLE);
                                Constants.MESSAGE = jobj.getString("message");

                            }
                            Constants.dismissProgressDialog(context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.dismissProgressDialog(context);
                Constants.toastDialog((Activity) context, "Please Check Internet Connection..!!!");
//                Toast.makeText(MyHunts_Activity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("filter_by", status);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (reqCode == 200) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    //Get the phone number id from the Uri
                    String id = uri.getLastPathSegment();
                    //Query the phone numbers for the selected phone number id
                    Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone._ID + "=?", new String[]{id}, null);
                    int phoneIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    //get the only phone number
                    if (c.moveToFirst()) {
                        String phone_no = c.getString(phoneIdx);
                        Log.e("phone", c.getString(phoneIdx));
                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        sendIntent.putExtra("sms_body", "Please join the " + Constants.HUNT_TITLE_SHARE + ".Your Code is " + Constants.HUNT_CODE_SHARE);
                        sendIntent.putExtra("address", phone_no);
                        sendIntent.setType("vnd.android-dir/mms-sms");
                        startActivityForResult(sendIntent, 300);

                    } else {
                        //no result
                        Toast noResultFound = Toast.makeText(InviteFriendsActivity.this, "No phone number found", Toast.LENGTH_SHORT);
                        noResultFound.show();
                    }
                    c.close();
                }

            }

        }
    }
}
