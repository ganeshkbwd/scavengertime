package com.hnweb.scavengertime.hostsActivities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.PreBuildHuntsAdapter;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.PreBuildHuntList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class PreBuildHuntsActivity extends AppCompatActivity implements View.OnClickListener {
    EditText searchView;
    RecyclerView recyclerView;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    ArrayList<PreBuildHuntList> pbhList = new ArrayList<PreBuildHuntList>();
    PreBuildHuntsAdapter preBuildHuntsAdapter;
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_build_hunts);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        setToolbarDrawer();
        searchView = (EditText) findViewById(R.id.searchView);
//        searchView.addTextChangedListener(this);
//        searchView.setIconifiedByDefault(false);


//        searchView.setLayoutParams(new ActionBar.LayoutParams(Gravity.RIGHT));
//        searchView.setIconifiedByDefault(false);
//        searchView.setIconified(false);
//        ImageView searchViewIcon =
//                (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
//
//        ViewGroup linearLayoutSearchView =
//                (ViewGroup) searchViewIcon.getParent();
//        linearLayoutSearchView.removeView(searchViewIcon);
//        linearLayoutSearchView.addView(searchViewIcon);
//        searchViewIcon.setAdjustViewBounds(true);
//        searchViewIcon.setMaxWidth(0);
//        searchViewIcon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//        searchViewIcon.setImageDrawable(null);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (CheckConnectivity.checkInternetConnection(this))
            preBuildHuntWebservice();


    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(PreBuildHuntsActivity.this, String.valueOf(user_id),iv,nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    public void preBuildHuntWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.preBuildHuntList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            JSONArray jarr = jobj.getJSONArray("response");

                            if (Constants.MESSAGE_CODE == 1) {
                                pbhList.clear();
                                for (int i = 0; i < jarr.length(); i++) {
                                    PreBuildHuntList pbhl = new PreBuildHuntList();
                                    pbhl.setPrebuilt_hunt_id(jarr.getJSONObject(i).getInt("prebuilt_hunt_id"));
                                    pbhl.setHunt_title(jarr.getJSONObject(i).getString("hunt_title"));
                                    pbhl.setHunt_description(jarr.getJSONObject(i).getString("hunt_description"));
                                    pbhl.setPrice(jarr.getJSONObject(i).getString("price"));
                                    pbhl.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    pbhl.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
                                    pbhList.add(pbhl);
                                }
                                preBuildHuntsAdapter = new PreBuildHuntsAdapter(PreBuildHuntsActivity.this, pbhList);
                                recyclerView.setAdapter(preBuildHuntsAdapter);
//                                Bundle b = new Bundle();
//                                b.putSerializable("pbhl", pbhList);
//                                Intent intent = new Intent(PreBuildHuntsActivity.this, PurchaseHuntActivity.class);
//                                intent.putExtras(b);
//                                startActivity(intent);

                                Constants.progressDialog.dismiss();
                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(PreBuildHuntsActivity.this, Constants.MESSAGE);
                                Constants.progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(PreBuildHuntsActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(PreBuildHuntsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.logoutBTN:
//                Constants.logout_fun(this);
//                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
        }
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }


//    @Override
//    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        this.preBuildHuntsAdapter.getFil
//    }
//
//    @Override
//    public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//    }
//
//    @Override
//    public void afterTextChanged(Editable s) {
//
//    }
}
