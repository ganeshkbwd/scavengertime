package com.hnweb.scavengertime.hostsActivities;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;

public class SelectedMediaActivity extends AppCompatActivity {
    ImageView imageIv;
    VideoView videoView;
    String imgPath, type;
    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_media);

        type = getIntent().getExtras().getString("type");
        imgPath = getIntent().getExtras().getString("Path");
        imageIv = (ImageView) findViewById(R.id.imageIv);
        videoView = (VideoView) findViewById(R.id.videoView);
        pDialog = new ProgressDialog(SelectedMediaActivity.this);


        if (type.equalsIgnoreCase("image")) {
            imageIv.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            Glide.with(this).load(imgPath).into(imageIv);
        } else {
            pDialog.setTitle("Please wait Audio Streaming");
            asyncVideo(Uri.parse(imgPath));
        }


    }


    public void asyncVideo(Uri uri) {
        // Set progressbar message
        pDialog.setMessage("Buffering...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();


        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    SelectedMediaActivity.this);
            mediacontroller.setAnchorView(videoView);
            // Get the URL from String VideoURL

            videoView.setVisibility(View.VISIBLE);
            imageIv.setVisibility(View.GONE);
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(uri);


        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
//            if (url.endsWith(".mp3")) {
//                Toast.makeText(this, "Can't play audio", Toast.LENGTH_SHORT).show();
//            } else {
                Toast.makeText(this, "Can't play video", Toast.LENGTH_SHORT).show();
//            }
        }

        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoView.start();
            }
        });
    }



}
