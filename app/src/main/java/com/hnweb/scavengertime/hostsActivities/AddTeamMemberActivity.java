package com.hnweb.scavengertime.hostsActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class AddTeamMemberActivity extends AppCompatActivity implements View.OnClickListener {
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    EditText memberNameET;
    Bundle bundle;
    String team_id, team_member_name, team_member_id;
    Button addBTN, continueBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team_member);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        bundle = getIntent().getExtras();
        team_id = bundle.getString("team_id");
        team_member_name = bundle.getString("team_member_name");
        team_member_id = bundle.getString("team_member_id");

        memberNameET = (EditText) findViewById(R.id.memberNameET);
        addBTN = (Button) findViewById(R.id.addBTN);
        continueBTN = (Button) findViewById(R.id.continueBTN);

        if (TextUtils.isEmpty(team_member_name)) {
            memberNameET.setText("");
            addBTN.setVisibility(View.VISIBLE);

        } else {
            memberNameET.setText(team_member_name);
            memberNameET.setSelection(team_member_name.length());
            addBTN.setVisibility(View.INVISIBLE);

        }



    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueBTN:
                if (TextUtils.isEmpty(team_member_name)) {
                    if (TextUtils.isEmpty(memberNameET.getText().toString().trim())) {
                        memberNameET.setError("Please enter Team member Name");
                    } else {
                        addTeamMemberWebService(user_id, team_id, memberNameET.getText().toString().trim(), 2);
                    }
                } else {
                    editTeamMemberWebService(memberNameET.getText().toString().trim(), team_member_id, 2);
                }


                break;
            case R.id.addBTN:
                if (TextUtils.isEmpty(memberNameET.getText().toString().trim())) {
                    memberNameET.setError("Please enter Team member Name");
                } else {
                    addTeamMemberWebService(user_id, team_id, memberNameET.getText().toString().trim(), 1);
                }
                break;
        }
    }

    public void addTeamMemberWebService(final int user_id, final String team_id, final String team_member_name, final int i) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.addTeamMember,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

//                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
                                Toast.makeText(AddTeamMemberActivity.this, "Team Created succesfully.Please add team members", Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();

                                memberNameET.setText("");

                                if (i == 2) {
                                    Intent intent = new Intent(AddTeamMemberActivity.this, ManageTeamsActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

//                                Intent intent = new Intent(CreateTeamActivity.this, AddTeamMemberActivity.class);
//
//                                startActivity(intent);
//                                finish();


                            } else {
                                Toast.makeText(AddTeamMemberActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();//
//                                Toast.makeText(ChangePasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(AddTeamMemberActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(ChangePasswordActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();

                params.put("createdby_userid", String.valueOf(user_id));
                params.put("team_id", team_id);
                params.put("team_member_name", team_member_name);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void editTeamMemberWebService(final String team_member_name, final String team_member_id, final int i) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.editMemberName,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

//                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
//                                Toast.makeText(AddTeamMemberActivity.this, "Team Created succesfully.Please add team members", Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();

                                memberNameET.setText("");

                                if (i == 2) {
                                    Intent intent = new Intent(AddTeamMemberActivity.this, ManageTeamsActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

//                                Intent intent = new Intent(CreateTeamActivity.this, AddTeamMemberActivity.class);
//
//                                startActivity(intent);
//                                finish();


                            } else {
                                Toast.makeText(AddTeamMemberActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();//
//                                Toast.makeText(ChangePasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(AddTeamMemberActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(ChangePasswordActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();

//                params.put("createdby_userid", String.valueOf(user_id));
                params.put("team_member_id", team_member_id);
                params.put("team_member_name", team_member_name);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
