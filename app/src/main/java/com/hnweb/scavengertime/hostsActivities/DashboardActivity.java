package com.hnweb.scavengertime.hostsActivities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.hnweb.scavengertime.ChangePasswordActivity;
import com.hnweb.scavengertime.EditProfileActivity;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.MyProfileActivity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.DashboardAdapter;
import com.hnweb.scavengertime.helper.Constants;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {
    GridLayoutManager gridLayoutManager;
    RecyclerView recyclerView;
    int[] dashboardIcons = {R.drawable.create_scavenger_hunt_button, R.drawable.manage_hunt_button, R.drawable.invite_friends_button,
            R.drawable.create_team_button, R.drawable.manage_team_button,  R.drawable.settings_button
            , R.drawable.my_profile_button, R.drawable.shop_for_pre_build_hunt_button, R.drawable.all_hunt_item_button, R.drawable.upgrade_scavengertime};
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String user_type;
    private GoogleApiClient mGoogleApiClient;


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
//        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        setToolbarDrawer();
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
//        recyclerView.setVisibility(View.GONE);


        DashboardAdapter rcAdapter = new DashboardAdapter(this, dashboardIcons);
        recyclerView.setAdapter(rcAdapter);


    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);

        //  settingDrawerLL
//        drawerLayout.setForegroundGravity(Gravity.RIGHT);

//        drawerLayout.openDrawer(Gravity.RIGHT);
//        drawerLayout.closeDrawer(Gravity.RIGHT);
//        drawerToggle.??
//        drawerToggle.
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Constants.profileWebservice(DashboardActivity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();


    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.logoutBTN:
//                Constants.logout_fun(this);
//                break;
//
//        }
    }

    public void onDrawerClick(View v) {
//        if (v.getId()==R.id.hometBTN){
//        Constants.drawerClick(this, v.getId());
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        Intent intent;
        switch (v.getId()) {
            case R.id.hometBTN:
                if (user_type.equalsIgnoreCase("Host")) {
                    intent = new Intent(this, DashboardActivity.class);
                    startActivity(intent);
                    finish();
                } else if (user_type.equalsIgnoreCase("Player")) {
                    intent = new Intent(this, MyHunts_Activity.class);
                    startActivity(intent);
                    finish();
                }


                break;
            case R.id.inviteBTN:
                intent = new Intent(this, InviteFriendsActivity.class);
//                intent.putExtra("ComeFrom", "Invite");
                startActivity(intent);
//                finish();
                break;
//            case R.id.noyifyBTN:
//                Constants.underDevelopment(this);
//                break;
            case R.id.myProfileBTN:
                intent = new Intent(this, MyProfileActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.settingsBTN:
//                Constants.underDevelopment(this);
                intent = new Intent(this, ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.logoutBTN:
                if (sharedPreferences.getInt("logWith", 0) == 1) {
                    signOut();
                } else if (sharedPreferences.getInt("logWith", 0) == 2) {
                    LoginManager.getInstance().logOut();
                }

                Constants.logout_fun(this);
                break;
            case R.id.editBTN:
                intent = new Intent(this, EditProfileActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nullRL:
//no action
                break;
            case R.id.proRL:
                //no action
                break;
        }
//        }else {
//            Toast.makeText(this,"You are already on home.",Toast.LENGTH_SHORT).show();
//        }

    }

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.e("Status", String.valueOf(status));
//                        updateUI(false);
                    }
                });
    }
}
