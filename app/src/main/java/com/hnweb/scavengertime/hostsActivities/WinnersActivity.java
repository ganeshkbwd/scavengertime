package com.hnweb.scavengertime.hostsActivities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.WinnersAdapter;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.Winners;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

public class WinnersActivity extends AppCompatActivity implements View.OnClickListener {

    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String hunt_id;
    Toolbar toolbar;
    ArrayList<Winners> wList = new ArrayList<Winners>();
    TextView startedOnTV, endedOnTV, huntNameTV;
    RecyclerView recyclerView;
    private CallbackManager mCallbackManager;
    private Profile profile;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    ByteArrayOutputStream bytearrayoutputstream;
    FileOutputStream fileoutputstream;
    Bitmap bitmap;
    LinearLayout shareViewLL;
    Button shareResultBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_winners);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        hunt_id = getIntent().getExtras().getString("hunt_id");
        setToolbarDrawer();
        init();
        winnersWebservice();
    }


    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

    }

    public void init() {
        huntNameTV = (TextView) findViewById(R.id.huntNameWTV);
        startedOnTV = (TextView) findViewById(R.id.startedOnTV);
        endedOnTV = (TextView) findViewById(R.id.endedOnTV);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(WinnersActivity.this));
        shareViewLL = (LinearLayout) findViewById(R.id.shareViewLL);
        shareResultBTN = (Button) findViewById(R.id.shareResultBTN);
    }


    public void winnersWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.winnersList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
                                String hunt_title = jobj.getString("hunt_title");
                                String hunt_started_on = jobj.getString("hunt_started_on");
                                String hunt_ended_on = jobj.getString("hunt_ended_on");

                                huntNameTV.setText(hunt_title);
                                startedOnTV.setText("Started On: " + hunt_started_on);
                                endedOnTV.setText("Ended On: " + hunt_ended_on);

                                wList.clear();
                                JSONArray jarr = jobj.getJSONArray("hunt_items");
                                for (int i = 0; i < jarr.length(); i++) {

                                    Winners w = new Winners();
                                    w.setJoin_hunt_id(jarr.getJSONObject(i).getString("join_hunt_id"));
                                    w.setHunt_code(jarr.getJSONObject(i).getString("hunt_code"));
                                    w.setHunt_id(jarr.getJSONObject(i).getString("hunt_id"));
                                    w.setUser_id(jarr.getJSONObject(i).getString("user_id"));
                                    w.setJoined_date_time(jarr.getJSONObject(i).getString("joined_date_time"));
                                    w.setTake_datetime(jarr.getJSONObject(i).getString("take_datetime"));
                                    w.setFinish_datetime(jarr.getJSONObject(i).getString("finish_datetime"));
                                    w.setTotal_points(jarr.getJSONObject(i).getString("total_points"));
                                    w.setCollected_items(jarr.getJSONObject(i).getString("collected_items"));
                                    w.setTime_used_days(jarr.getJSONObject(i).getString("time_used_days"));
                                    w.setTime_used_hours(jarr.getJSONObject(i).getString("time_used_hours"));
                                    w.setTime_used_minutes(jarr.getJSONObject(i).getString("time_used_minutes"));
                                    w.setWinner_level(jarr.getJSONObject(i).getString("winner_level"));
                                    w.setIs_winner(jarr.getJSONObject(i).getString("is_winner"));
                                    w.setFull_name(jarr.getJSONObject(i).getString("full_name"));
                                    w.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));

                                    wList.add(w);
                                }
                                recyclerView.setAdapter(new WinnersAdapter(WinnersActivity.this, wList));
                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(WinnersActivity.this, Constants.MESSAGE);
                            }

                            Constants.dismissProgressDialog(WinnersActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(WinnersActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", hunt_id);
//                params.put("hunt_id", hunt_id);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(this, MyHunts_Activity.class);
//        intent.putExtra("ComeFrom", "");
//        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shareResultBTN:
                takeScreenshot(view);


//                startActivityForResult(Intent.createChooser(intent, 1001));
//                fbadd();
                break;
        }
    }


    ///////// Facebook //////////////


    public void fbadd() {
        //        callbackManager = CallbackManager.Factory.create();
        /////////////////////////////////////////
        shareResultBTN.setVisibility(View.VISIBLE);
        keyHash();
        mCallbackManager = CallbackManager.Factory.create();
        //   LoginManager.getInstance().logOut();
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                //  textView.setText(displayMessage(profile1));
                // System.out.println(displayMessage(profile1));
            }
        };

        tokenTracker.startTracking();
        profileTracker.startTracking();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_friends"));
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(mCallbackManager, mFacebookCallback);


//        LoginManager.getInstance().ppublish(feed, true, onPublishListener);
        //////////////////////////////////////////////
    }

    public void keyHash() {
        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("com.hnweb.scavengertime", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            profile = Profile.getCurrentProfile();


            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.v("LoginActivity Response ", response.toString());
                            System.out.println("arshres" + response.toString());

                            try {
                                String id = object.getString("id");
                                String Name = object.getString("name");

                                String FEmail = object.getString("email");
                                Log.v("Email = ", id + "  " + FEmail + "  " + Name);
                                // Toast.makeText(getApplicationContext(), "Name " + Name+FEmail, Toast.LENGTH_LONG).show();
//                                if (CheckConnectivity.checkInternetConnection(WinnersActivity.this)) {
                                sharePhotoToFacebook();
//                                    fbSignInWebservice(id, Name, FEmail);
//                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();


            //textView.setText(displayMessage(profile));
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };


    private void sharePhotoToFacebook() {

//        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption("Give me my codez or I will ... you know, do that thing you don't like!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

//        ShareApi.share(content, null);
        ShareDialog.show(this, content);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 140) {
////            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
//        } else {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
//        }


    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        //   textView.setText(displayMessage(profile));
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            profileTracker.stopTracking();
            tokenTracker.stopTracking();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public Bitmap getBitmapOfView(View v) {
        View rootview = v.getRootView();
        View btnView = findViewById(R.id.shareResultBTN);
        ((ViewGroup) rootview).removeView(btnView);
        rootview.setDrawingCacheEnabled(true);
        Bitmap bmp = rootview.getDrawingCache();
        return bmp;
    }

    public void createImageFromBitmap(Bitmap bmp) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 40, bytes);
        File file = new File(Environment.getExternalStorageDirectory() +
                "/capturedscreen.png");
        try {
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            ostream.write(bytes.toByteArray());
            ostream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        File file1 = new File(image_path);

        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent .setType("image/*");
        intent .putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(intent );
    }


    public void takeScreenshot(View view) {
        shareResultBTN.setVisibility(View.INVISIBLE);
//        shareViewLL.setDrawingCacheEnabled(true);
        View content = findViewById(R.id.shareViewLL);
//        ((ViewGroup) view).removeView(shareResultBTN);
        bitmap = getBitmapOfView(content);


//        gif.setImageBitmap(bitmap);
        createImageFromBitmap(bitmap);
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
