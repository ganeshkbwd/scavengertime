package com.hnweb.scavengertime.hostsActivities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class PurchaseHuntActivity extends AppCompatActivity implements View.OnClickListener {
    Bundle bundle;
    int hunt_id;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String hunt_title, hunt_description, price;
    int prebuilt_hunt_id;
    TextView huntNameTV, descriptionTV, priceTV;
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_hunt);
        bundle = getIntent().getExtras();
        if (bundle != null)
            hunt_id = Integer.parseInt(bundle.getString("ID"));
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        init();
        setToolbarDrawer();
        preBuildHuntWebservice();

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(PurchaseHuntActivity.this, String.valueOf(user_id),iv,nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    public void init() {
        huntNameTV = (TextView) findViewById(R.id.huntNameTV);
        descriptionTV = (TextView) findViewById(R.id.descriptionTV);
        priceTV = (TextView) findViewById(R.id.priceTV);
    }

    public void preBuildHuntWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getPreBuildHuntPurchaseDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {
                                prebuilt_hunt_id = jobj.getInt("prebuilt_hunt_id");
                                hunt_title = jobj.getString("hunt_title");
                                hunt_description = jobj.getString("hunt_description");
                                price = jobj.getString("price");

                                huntNameTV.setText("\"" + hunt_title + "\"");
                                descriptionTV.setText(hunt_description);
                                priceTV.setText(price);

                                Constants.progressDialog.dismiss();
                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(PurchaseHuntActivity.this, Constants.MESSAGE);
                                Constants.progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(PurchaseHuntActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(PurchaseHuntActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("prebuilt_hunt_id", String.valueOf(hunt_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.logoutBTN:
//                Constants.logout_fun(this);
//                break;
            default:
                Constants.underDevelopment(this);
                break;
            case R.id.settingsBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.myProfileBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.noyifyBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.inviteBTN:
                Constants.underDevelopment(this);
                break;
            case R.id.hometBTN:
                Constants.underDevelopment(this);
                break;
        }

//        Constants.underDevelopment(this);
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }
}
