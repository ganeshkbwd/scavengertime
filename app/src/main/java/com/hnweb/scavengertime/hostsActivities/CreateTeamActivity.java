package com.hnweb.scavengertime.hostsActivities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class CreateTeamActivity extends AppCompatActivity implements View.OnClickListener {
    EditText uniqueNameET;
    TextInputLayout uniqueNameTIL;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    Button continueBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        uniqueNameTIL = (TextInputLayout) findViewById(R.id.uniqueNameTIL);
        uniqueNameET = (EditText) findViewById(R.id.uniqueNameET);
        continueBTN = (Button) findViewById(R.id.continueBTN);

        teamWebservice(user_id, CreateTeamActivity.this);

    }

    public void checkValid() {
        if (TextUtils.isEmpty(uniqueNameET.getText().toString().trim())) {
            uniqueNameET.setError("Pleae enter Team name.");
        } else {
            if (CheckConnectivity.checkInternetConnection(this)) {
                createTeamWebservice(user_id, uniqueNameET.getText().toString().trim());
            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueBTN:
                if (continueBTN.getText().toString().trim().equalsIgnoreCase("EDIT")) {

                    uniqueNameET.setEnabled(true);
                    uniqueNameET.setSelection(uniqueNameET.getText().toString().trim().length());
                    continueBTN.setText("CONTINUE");
                } else {
                    checkValid();
                }

                break;
        }
    }


    public void createTeamWebservice(final int user_id, final String teamName) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.addTeam,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

//                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
                                Toast.makeText(CreateTeamActivity.this, "Team Created succesfully.Please add team members", Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();

                                uniqueNameET.setText("");

//                                Intent intent = new Intent(CreateTeamActivity.this, AddTeamMemberActivity.class);
//
//                                startActivity(intent);
//                                finish();


                            } else {
                                Toast.makeText(CreateTeamActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();//
//                                Toast.makeText(ChangePasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(CreateTeamActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(ChangePasswordActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();

                params.put("createdby_userid", String.valueOf(user_id));
                params.put("team_name", teamName);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void teamWebservice(final int user_id, final Activity activity) {

        Constants.showProgress(activity);
        RequestQueue queue = Volley.newRequestQueue(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getTeamDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

//                                Constants.toastDialog(ChangePasswordActivity.this, Constants.MESSAGE);
//                                Toast.makeText(CreateTeamActivity.this, "Team Created succesfully.Please add team members", Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();

                                String team_id = jobj.getString("team_id");
                                String teamName = jobj.getString("team_name");
                                String team_photo = jobj.getString("team_photo");
                                uniqueNameET.setText(teamName);
                                continueBTN.setText("EDIT");
                                uniqueNameET.setEnabled(false);
//                                Intent intent = new Intent(CreateTeamActivity.this, AddTeamMemberActivity.class);
//
//                                startActivity(intent);
//                                finish();


                            } else {
                                Toast.makeText(activity, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();//
                                uniqueNameET.setText("");
                                uniqueNameET.setEnabled(true);
                                continueBTN.setText("CONTINUE");

//                                Toast.makeText(ChangePasswordActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(activity, "Please Check Internet Connection..!!!");
//                Toast.makeText(ChangePasswordActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();

                params.put("createdby_userid", String.valueOf(user_id));
//                params.put("team_name", teamName);

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
