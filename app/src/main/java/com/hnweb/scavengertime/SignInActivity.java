package com.hnweb.scavengertime;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;
import com.hnweb.scavengertime.hostsActivities.DashboardActivity;
import com.hnweb.scavengertime.playersActivities.JoinHuntActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, CheckBox.OnCheckedChangeListener {

    EditText usernameET, passwordET;
    CheckBox rememberCB;
    String username, password;
    Button signInBTN, signUpBTN;
    TextView fpBTN;
    SharedPreferences sharedPreferences, sharedPreferences1, getSharedPreferences;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    public static final String MYCREDENTIALS = "ScavengerTimeAppCred";
    String shareUserName, sharePassword;
    private CallbackManager mCallbackManager;
    private Profile profile;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_in);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences1 = getSharedPreferences(MYCREDENTIALS, Context.MODE_PRIVATE);
        getSharedPreferences = getApplicationContext().getSharedPreferences(MYCREDENTIALS, 0);
        shareUserName = getSharedPreferences.getString("user_name", "");
        sharePassword = getSharedPreferences.getString("password", "");
        init();
    }

    public void init() {
        usernameET = (EditText) findViewById(R.id.usernameET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        rememberCB = (CheckBox) findViewById(R.id.rememberCB);
        signInBTN = (Button) findViewById(R.id.loginBTN);
        signUpBTN = (Button) findViewById(R.id.signUpBTN);
        fpBTN = (TextView) findViewById(R.id.clickHereTV);
        rememberCB.setOnCheckedChangeListener(this);
        if (!shareUserName.equalsIgnoreCase("") && !sharePassword.equalsIgnoreCase("")) {
            usernameET.setText(shareUserName);
            passwordET.setText(sharePassword);
            rememberCB.setChecked(true);
        } else {
            usernameET.setText("");
            passwordET.setText("");
        }

    }

    public void getUserInputs() {
        username = usernameET.getText().toString().trim();
        password = passwordET.getText().toString().trim();
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.loginBTN:
                getUserInputs();
                if (Validations.strslength(username)) {
                    if (Validations.strslength(password)) {
                        if (CheckConnectivity.checkInternetConnection(this)) {
                            //callWebservice

                            callSignInWebservice(username, password);
                        }
                    } else {
                        passwordET.setError(getResources().getString(R.string.plz_pass));
                    }
                } else {
                    usernameET.setError(getResources().getString(R.string.plz_username));
                }
                break;
            case R.id.signUpBTN:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                break;
            case R.id.clickHereTV:
                intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.fbBTN:
                fbadd();
                break;

        }
    }


    public void forgotPasswordDialog() {
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.upgrade_dialog
                , null));
        settingsDialog.setCancelable(true);
        final EditText fppassTV = (EditText) settingsDialog.findViewById(R.id.fp_email_idET);
        Button submitBTN = (Button) settingsDialog.findViewById(R.id.fpsubmitBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = fppassTV.getText().toString().trim();
                if (Validations.emailCheck(email)) {
                    if (CheckConnectivity.checkInternetConnection(SignInActivity.this)) {
                        callForgotPasswordWebService(email, settingsDialog);
                    }
                } else {
                    fppassTV.setError(getResources().getString(R.string.plz_email));
                }
            }
        });

        settingsDialog.show();
    }

    public void callForgotPasswordWebService(final String email, final Dialog settingsDialog) {

        Constants.showProgress(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.forgotPassword,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            if (Constants.MESSAGE_CODE == 1) {
                                Constants.MESSAGE = jobj.getString("message");
                                Toast.makeText(SignInActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
                                settingsDialog.dismiss();

                            } else {

                                Constants.progressDialog.dismiss();
                                Toast.makeText(SignInActivity.this, "Your entered email id not registered.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email_address", email);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void callSignInWebservice(final String username, final String pass) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.loginUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {
//                                if (rememberCB.isChecked()) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putInt("login", 1);
                                editor.putInt("logWith", 0);
                                editor.putInt("user_id", user_id);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("user_type", Constants.USER_TYPE);
                                editor.commit();
//                                }

                                Toast.makeText(SignInActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                toastDialog(Constants.MESSAGE);
//                                if (Constants.OK == 1) {
//
//
//                                }
//                                Toast.makeText(SignInActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
//
//                                if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Review")) {
//
//                                    Bundle bundle = new Bundle();
//                                    bundle.putString("ComeFrom", "SignIn");
//                                    intent = new Intent(SignInActivity.this, ReviewCustomerSideActivity.class);
//                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                } else {
//                                    Bundle bundle = new Bundle();
//                                    bundle.putString("ComeFrom", "SignIn");
//
//
//                                }


                                Constants.progressDialog.dismiss();


                            } else {

                                Constants.progressDialog.dismiss();//
                                Constants.toastDialog(SignInActivity.this, Constants.MESSAGE);
//                                Toast.makeText(SignInActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();

                params.put("username", username);
                params.put("password", pass);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void toastDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
                            Constants.OK = 0;
                            Intent intent = new Intent(SignInActivity.this, DashboardActivity.class);
//                                    intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        } else {
                            Constants.OK = 0;
                            Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
//                                    intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        getUserInputs();
        if (b) {
            SharedPreferences.Editor editor = sharedPreferences1.edit();

            editor.putString("user_name", username);
            editor.putString("password", password);

            editor.commit();

        } else {

            SharedPreferences settings = getApplicationContext()
                    .getSharedPreferences(MYCREDENTIALS,
                            Context.MODE_PRIVATE);
            settings.edit().clear().commit();

        }
    }

    /////////// Facebook //////////////


    public void fbadd() {
        //        callbackManager = CallbackManager.Factory.create();
        /////////////////////////////////////////
        keyHash();
        mCallbackManager = CallbackManager.Factory.create();
        //   LoginManager.getInstance().logOut();
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                //  textView.setText(displayMessage(profile1));
                // System.out.println(displayMessage(profile1));
            }
        };

        tokenTracker.startTracking();
        profileTracker.startTracking();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_friends"));
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(mCallbackManager, mFacebookCallback);
//        LoginManager.getInstance().ppublish(feed, true, onPublishListener);
        //////////////////////////////////////////////
    }

    public void keyHash() {
        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("com.hnweb.scavengertime", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            profile = Profile.getCurrentProfile();


            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.v("LoginActivity Response ", response.toString());
                            System.out.println("arshres" + response.toString());

                            try {
                                String id = object.getString("id");
                                String Name = object.getString("name");

                                String FEmail = object.getString("email");
                                Log.v("Email = ", id + "  " + FEmail + "  " + Name);
                                // Toast.makeText(getApplicationContext(), "Name " + Name+FEmail, Toast.LENGTH_LONG).show();
                                if (CheckConnectivity.checkInternetConnection(SignInActivity.this)) {
//                                sharePhotoToFacebook();
                                    fbSignInWebservice(id, Name, FEmail);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();


            //textView.setText(displayMessage(profile));
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };


    private void sharePhotoToFacebook() {

        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("Give me my codez or I will ... you know, do that thing you don't like!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

//        ShareApi.share(content, null);
        ShareDialog.show(this, content);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 140) {
////            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
//        } else {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
//        }


    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        //   textView.setText(displayMessage(profile));
    }

    @Override
    public void onStop() {

        try {
            super.onStop();
            profileTracker.stopTracking();
            tokenTracker.stopTracking();
        } catch (Exception e) {
//            e.printStackTrace();
        }

    }


    public void fbSignInWebservice(final String id, final String name, final String email) {

        Constants.showProgress(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/scavengertime/api/login_with_facebook_credentials.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");
                            if (Constants.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putInt("login", 1);
                                editor.putInt("user_id", user_id);
                                editor.putInt("logWith", 1);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("user_type", Constants.USER_TYPE);
                                editor.commit();

//                                Toast.makeText(SignUpActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
//                                if (Constants.USER_TYPE.equalsIgnoreCase("HOST")) {
//                                    Constants.OK = 0;
//                                    Intent intent = new Intent(SignInActivity.this, DashboardActivity.class);
////                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                    finish();
//                                } else {
//                                    Constants.OK = 0;
//                                    Intent intent = new Intent(SignInActivity.this, JoinHuntActivity.class);
////                                    intent.putExtras(bundle);
//                                    startActivity(intent);
//                                    finish();
//                                }
                                toastDialog(Constants.MESSAGE);


                            } else {

                                Constants.progressDialog.dismiss();//
                                Toast.makeText(SignInActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, R.string.internetCheck, Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("facebook_user_id", id);
                params.put("email_address", email);
                params.put("full_name", name);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


}
