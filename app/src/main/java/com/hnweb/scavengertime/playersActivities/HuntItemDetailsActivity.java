package com.hnweb.scavengertime.playersActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.PickerUtils;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class HuntItemDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String hunt_id, hunt_item_id, hunt_title, hunt_description, point_value;
    ImageView uploadIV;
    TextView descTV, pvTV, huntNameTV;
    private static final int SELECT_VIDEO = 3;
    private static final int CAPTURE_VIDEO = 4;
    private static final int REQUEST_CAMERA = 5;
    public static final int FROM_GALLARY = 103;
    File destination;
    String imagePath;
    String filePath = "";
//    public Uri fileUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hunt_item_details);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        getFromBundle();
        setToolbarDrawer();
        init();



    }

    public void getFromBundle() {
        Bundle bundle = getIntent().getExtras();
        hunt_id = bundle.getString("hunt_id");
        hunt_title = bundle.getString("title");
        hunt_item_id = bundle.getString("hunt_item_id");
        hunt_description = bundle.getString("description");
        point_value = bundle.getString("pointValue");
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
////                Constants.profileWebservice(HuntItemDetailsActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }

    public void init() {
        huntNameTV = (TextView) findViewById(R.id.huntNameTV);
        descTV = (TextView) findViewById(R.id.descTV);
        pvTV = (TextView) findViewById(R.id.pvTV);
        uploadIV = (ImageView) findViewById(R.id.uploadIV);
        setData();
    }

    public void setData() {
        huntNameTV.setText(hunt_title);
        descTV.setText(hunt_description);
        pvTV.setText(point_value);
    }


//    ///////////////------- Photo Upload -----------///////////
//    private void selectImage() {
//        final CharSequence[] items = {"Take Photo", "Choose from Library",
//                "Cancel"};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Add Photo!");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (items[item].equals("Take Photo")) {
//
//                    PickerUtils.takePictureIntent(HuntItemDetailsActivity.this, REQUEST_CAMERA);
//
//                } else if (items[item].equals("Choose from Library")) {
//
//                    PickerUtils.galleryIntent(HuntItemDetailsActivity.this, FROM_GALLARY);
//
//                } else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == FROM_GALLARY) {

                System.out.println("SELECT_IMAGE");
                Uri uri = data.getData();
                filePath = PickerUtils.gallaryPath(HuntItemDetailsActivity.this, uri);
                Glide.with(this).load(new File(filePath)).into(uploadIV);

            } else if (requestCode == REQUEST_CAMERA) {

                System.out.println("REQUEST_CAMERA");
                filePath = PickerUtils.camerapath(uploadIV);

            } else if (requestCode == SELECT_VIDEO) {

                System.out.println("SELECT_VIDEO");
                Uri uri = data.getData();
                filePath = PickerUtils.getGalleryVideoPath(HuntItemDetailsActivity.this, uri);
                Glide.with(this).load(Uri.fromFile(new File(filePath))).asBitmap().override(150, 150).into(uploadIV);
            } else if (requestCode == Constants.CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {

                    // video successfully recorded
                    // launching upload activity
                    filePath = PickerUtils.getOutputMediaFile(Constants.MEDIA_TYPE_VIDEO).getAbsolutePath();
//                    filePath = Constants.fileUri.toString();

                    Glide.with(this).load(Constants.fileUri).asBitmap().override(150, 150).into(uploadIV);


                } else if (resultCode == RESULT_CANCELED) {

                    // user cancelled recording
                    Toast.makeText(getApplicationContext(),
                            "User cancelled video recording", Toast.LENGTH_SHORT)
                            .show();

                } else {
                    // failed to record video
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selectImgBTN:

                PickerUtils.selectImage1(HuntItemDetailsActivity.this, REQUEST_CAMERA, FROM_GALLARY, SELECT_VIDEO, CAPTURE_VIDEO);

                break;
            case R.id.uploadBTN:


//                Toast.makeText(this, "PATH : " + filePath, Toast.LENGTH_SHORT).show();
                Log.e("PATHHHHHHHHH : ", filePath);

                if (Validations.strslength(filePath)) {
                    imageVideoUpload(filePath, hunt_id, hunt_item_id, user_id);
                } else {
                    Constants.toastDialog(this, "Please select picture or video");
//                    Toast.makeText(this,"Please select picture or video",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.ttrBTN:
                onBackPressed();
                break;
        }
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }


    public void imageVideoUpload(final String picturePath, String hunt_id, String hunt_item_id, int user_id) {
        Constants.showProgress(this);

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, "http://designer321.com/johnilbwd/scavengertime/api/up_media_played_hunt_item.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);

                        JSONObject jobj = null;

                        try {
                            jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");


                            if (Constants.MESSAGE_CODE == 1) {

                                Constants.toastDialog(HuntItemDetailsActivity.this, Constants.MESSAGE);
                            } else {
                                Constants.toastDialog(HuntItemDetailsActivity.this, "Updated Successfully");
//                                Constants.toastDialog(HuntItemDetailsActivity.this, Constants.MESSAGE);
                            }

//                            Toast.makeText(HuntItemDetailsActivity.this, "Uploaded successfully", Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


//                        Toast.makeText(AddHuntedItemsActivity.this,)
                        Constants.progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
//                Constants.toastDialog(this.getApplicationContext(), error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

//        try {
//            smr.addFile("selectedPath", picturePath);
//            System.out.println("Arsh Op" + picturePath);
//        } catch (Exception e) {
//
//        }
        smr.addStringParam("player_user_id", String.valueOf(user_id));
        smr.addStringParam("hunt_id", hunt_id);
        smr.addStringParam("hunt_item_id", hunt_item_id);
        smr.addFile("media_file", picturePath);


        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(smr);

    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", Constants.fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        Constants.fileUri = savedInstanceState.getParcelable("file_uri");
    }

}
