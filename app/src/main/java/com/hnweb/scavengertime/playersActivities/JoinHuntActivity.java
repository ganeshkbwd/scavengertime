package com.hnweb.scavengertime.playersActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.helper.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class JoinHuntActivity extends AppCompatActivity implements View.OnClickListener {
    EditText huntCode;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_code);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        huntCode = (EditText) findViewById(R.id.huntCodeET);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitBTN:
                if (Validations.strslength(huntCode.getText().toString().trim())) {
//                    Intent intent = new Intent(JoinHuntActivity.this, MyHuntsActivity.class);
//                    startActivity(intent);
                    if (CheckConnectivity.checkInternetConnection(this))
                        joinHuntWebservice(huntCode.getText().toString().trim());
                } else {
                    Constants.toastDialog(this, "Please enter hunt code");
//                    Toast.makeText(this, "Please enter hunt code", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(JoinHuntActivity.this, MyHunts_Activity.class);
//                    intent.putExtra("ComeFrom","");
//                    startActivity(intent);
                }

                break;
        }
    }

    public void joinHuntWebservice(final String huntcode) {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.joinHunt,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");

                            if (Constants.MESSAGE_CODE == 1) {
                                Toast.makeText(JoinHuntActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
                                Intent intent = new Intent(JoinHuntActivity.this, MyHunts_Activity.class);
                                intent.putExtra("ComeFrom", "");
                                startActivity(intent);
                                finish();


                            } else {
                                if (Constants.MESSAGE_CODE == 0 && Constants.MESSAGE.equalsIgnoreCase("You have already joined this hunt.")) {
                                    Intent intent = new Intent(JoinHuntActivity.this, MyHunts_Activity.class);
                                    intent.putExtra("ComeFrom", "");
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Constants.toastDialog(JoinHuntActivity.this, Constants.MESSAGE);
//                                    Toast.makeText(JoinHuntActivity.this, Constants.MESSAGE, Toast.LENGTH_SHORT).show();

                                }

                            }
                            Constants.dismissProgressDialog(JoinHuntActivity.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(JoinHuntActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(JoinHuntActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("hunt_code", huntcode);
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
