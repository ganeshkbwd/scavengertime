package com.hnweb.scavengertime.playersActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.adapter.ChallengeAdapter;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;
import com.hnweb.scavengertime.pojo.HuntItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TakeChallengeActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView timerTV, hourTV, minuteTV, secondTV, huntNameTV;
    ArrayList<HuntItems> huntItemsList = new ArrayList<HuntItems>();
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    RecyclerView recyclerView;
    String endDateTime, hunttitle;
    long countDown;
    CountDownTimer timer;
    String huntId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_challenge);
        setToolbarDrawer();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        endDateTime = getIntent().getExtras().getString("ENDDATE");
        hunttitle = getIntent().getExtras().getString("HUNTNAME");
        huntId = getIntent().getExtras().getString("HUNTID");
//        endDateTime = "2016-12-02 12:30:54";


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        huntNameTV = (TextView) findViewById(R.id.huntNameTV);
        timerTV = (TextView) findViewById(R.id.timerTV);
        hourTV = (TextView) findViewById(R.id.hourTV);
        minuteTV = (TextView) findViewById(R.id.minuteTV);
        secondTV = (TextView) findViewById(R.id.secondTV);
        huntNameTV.setText(hunttitle);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date mDate = sdf.parse(endDateTime);
            long timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
            countDown = timeInMilliseconds - System.currentTimeMillis();
            Log.e("TIMESSS", countDown + "=" + timeInMilliseconds + "-" + System.currentTimeMillis());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        timer = new CountDownTimer(countDown, 1000) {
            public void onTick(long millisUntilFinished) {

                TimeUnit.SECONDS.toMillis(20);
                hourTV.setText("" + String.format("%d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished)));
                minuteTV.setText("" + String.format("%d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))));
                secondTV.setText("" + String.format("%d", TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//                timerTV.setText("" + String.format("%d:%d:%d",
//                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
//                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
//                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                timerTV.setText("Time Up");
            }
        };
        timer.start();
        if (CheckConnectivity.checkInternetConnection(this))
            huntItemsWebservice();
//        Alternative way
//
//        CountDownTimer timer1 = new CountDownTimer(countDown, 1000) {
//            public void onTick(long millisUntilFinished) {
//                int seconds = (int) (millisUntilFinished / 1000) % 60;
//                int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
//                int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
////                txtNewBetTimer.setText(String.format("%d:%d:%d",hours,minutes,seconds));
//                Log.e("TIMEEEEE", "HOUR" + hours + "Minute" + minutes + "Second" + seconds);
//            }
//
//            public void onFinish() {
////                txtNewBetTimer.setText("Time Up");
//            }
//        };
//        timer1.start();

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
//        final ImageView iv = (ImageView) settingDrawerLL.findViewById(R.id.myProfileIV);
//        final TextView nameTV = (TextView) settingDrawerLL.findViewById(R.id.nameTV);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Constants.profileWebservice(TakeChallengeActivity.this, String.valueOf(user_id), iv, nameTV);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();


    }


    public void huntItemsWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.huntItems,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {
                                huntItemsList.clear();
                                JSONArray jarr = jobj.getJSONArray("hunt_items");
                                for (int i = 0; i < jarr.length(); i++) {
                                    HuntItems huntItems = new HuntItems();
                                    huntItems.setHunt_item_id(jarr.getJSONObject(i).getInt("hunt_item_id"));
                                    huntItems.setHunt_id(jarr.getJSONObject(i).getInt("hunt_id"));
                                    huntItems.setTitle(jarr.getJSONObject(i).getString("title"));
                                    huntItems.setDescription(jarr.getJSONObject(i).getString("description"));
                                    huntItems.setPoint_value(jarr.getJSONObject(i).getString("point_value"));
                                    huntItems.setUploaded_file_path(jarr.getJSONObject(i).getString("uploaded_file_path"));
                                    huntItems.setUploaded_file_type(jarr.getJSONObject(i).getString("uploaded_file_type"));
                                    huntItems.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    huntItems.setPrice_of_hunt_item(jarr.getJSONObject(i).getString("price_of_hunt_item"));
                                    huntItems.setPrebuild_hunt_item_id(jarr.getJSONObject(i).getString("prebuild_hunt_item_id"));
                                    huntItemsList.add(huntItems);
                                }

//                                recyclerView.setAdapter(new HuntItemsAdapter(TakeChallengeActivity.this, huntItemsList));
                                recyclerView.setAdapter(new ChallengeAdapter(TakeChallengeActivity.this, huntItemsList));

                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(TakeChallengeActivity.this, Constants.MESSAGE);
                                Constants.progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(TakeChallengeActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(TakeChallengeActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", huntId);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.finishedBTN:
//                timer.cancel();

                if (CheckConnectivity.checkInternetConnection(this))
                    finishWebservice();
//                Constants.underDevelopment(this);
                break;
        }
    }

    public void finishWebservice() {

        Constants.showProgress(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.playerFinishHunt,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");

                            if (Constants.MESSAGE_CODE == 1) {

                                timer.cancel();
                                Intent intent = new Intent(TakeChallengeActivity.this, MyHunts_Activity.class);
                                intent.putExtra("ComeFrom", "");
                                startActivity(intent);
                                finish();

                            } else {


                            }
                            Constants.dismissProgressDialog(TakeChallengeActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(TakeChallengeActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(TakeChallengeActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("hunt_id", huntId);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }



//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }
}
