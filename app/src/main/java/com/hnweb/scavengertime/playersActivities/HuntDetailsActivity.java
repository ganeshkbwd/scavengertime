package com.hnweb.scavengertime.playersActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.hnweb.scavengertime.MyHunts_Activity;
import com.hnweb.scavengertime.R;
import com.hnweb.scavengertime.helper.CheckConnectivity;
import com.hnweb.scavengertime.helper.Constants;
import com.hnweb.scavengertime.helper.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class HuntDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    String hunt_id, hunt_title, hunt_description, is_timed_hunt, start_date_time,
            end_date_time, first_prize_text, first_prize_upload_file_path,
            second_prize_text, second_prize_upload_file_path, third_prize_text, third_prize_upload_file_path;
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView hunt_titleTV, huntDescriptionTV, firstPrizeTV, secondPrizeTV, thirdPrizeTV, timeLeftTV, startOnTV, startAtTV;
    Button takeChallengeBTN;
    ImageView firstPrizeIV, secondPrizeIV, thirdPrizeIV;
    int login, user_id;
    public static final String MyPREFERENCES = "ScavengerTimeAppPrefs";
    String user_type;
    String is_started;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hunt_details);
        String comeFrom = getIntent().getExtras().getString("From");
        String hunt_id = getIntent().getExtras().getString("hunt_id");
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        user_type = sharedPreferences.getString("user_type", null);
        init(comeFrom, hunt_id);
        setToolbarDrawer();


    }

    public void init(String comeFrom, String huntId) {
        hunt_titleTV = (TextView) findViewById(R.id.hunt_titleTV);
        huntDescriptionTV = (TextView) findViewById(R.id.huntDescriptionTV);
        firstPrizeTV = (TextView) findViewById(R.id.firstPrizeTV);
        secondPrizeTV = (TextView) findViewById(R.id.secondPrizeTV);
        thirdPrizeTV = (TextView) findViewById(R.id.thirdPrizeTV);
        timeLeftTV = (TextView) findViewById(R.id.timeLeftTV);
        startOnTV = (TextView) findViewById(R.id.startOnTV);
        startAtTV = (TextView) findViewById(R.id.startAtTV);
        takeChallengeBTN = (Button) findViewById(R.id.takeChallengeBTN);
        firstPrizeIV = (ImageView) findViewById(R.id.firstPrizeIV);
        secondPrizeIV = (ImageView) findViewById(R.id.secondPrizeIV);
        thirdPrizeIV = (ImageView) findViewById(R.id.thirdPrizeIV);

        if (comeFrom.equalsIgnoreCase("Active")) {
            takeChallengeBTN.setBackgroundResource(R.drawable.take_a_challange_enable);
            takeChallengeBTN.setEnabled(true);
        } else if (comeFrom.equalsIgnoreCase("UpComing")) {
            takeChallengeBTN.setBackgroundResource(R.drawable.take_a_challange_disable);
            takeChallengeBTN.setEnabled(false);
        }


        if (CheckConnectivity.checkInternetConnection(this)) {
            activeHuntsWebservice(huntId);
        }


    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//
//        };
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();
//
//        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
    }


    public void activeHuntsWebservice(final String hunt_id) {

        Constants.progressDialog = new ProgressDialog(this);
        Constants.progressDialog.setMessage("Please wait");
        Constants.progressDialog.show();
        Constants.progressDialog.setCancelable(false);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.joinHuntDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");


                            if (Constants.MESSAGE_CODE == 1) {
                                Constants.MESSAGE = jobj.getString("message");
                                HuntDetailsActivity.this.hunt_id = jobj.getString("hunt_id");
                                hunt_title = jobj.getString("hunt_title");
                                hunt_description = jobj.getString("hunt_description");
                                is_timed_hunt = jobj.getString("is_timed_hunt");
                                start_date_time = jobj.getString("start_date_time");
                                end_date_time = jobj.getString("end_date_time");
                                first_prize_text = jobj.getString("first_prize_text");
                                first_prize_upload_file_path = jobj.getString("first_prize_upload_file_path");
                                second_prize_text = jobj.getString("second_prize_text");
                                second_prize_upload_file_path = jobj.getString("second_prize_upload_file_path");
                                third_prize_text = jobj.getString("third_prize_text");
                                third_prize_upload_file_path = jobj.getString("third_prize_upload_file_path");
                                is_started = jobj.getString("is_started");

                                //set Data

                                hunt_titleTV.setText("\"" + hunt_title + "\"");
                                huntDescriptionTV.setText(hunt_description);
                                if (!first_prize_text.equalsIgnoreCase(""))
                                    firstPrizeTV.setText(first_prize_text);
                                if (!second_prize_text.equalsIgnoreCase(""))
                                    secondPrizeTV.setText(second_prize_text);
                                if (!third_prize_text.equalsIgnoreCase(""))
                                    thirdPrizeTV.setText(third_prize_text);
                                if (!first_prize_upload_file_path.equalsIgnoreCase(""))
                                    Glide.with(HuntDetailsActivity.this).load(first_prize_upload_file_path).override(100, 100).into(firstPrizeIV);
                                if (!second_prize_upload_file_path.equalsIgnoreCase(""))
                                    Glide.with(HuntDetailsActivity.this).load(second_prize_upload_file_path).override(100, 100).into(secondPrizeIV);
                                if (!third_prize_upload_file_path.equalsIgnoreCase(""))
                                    Glide.with(HuntDetailsActivity.this).load(third_prize_upload_file_path).override(100, 100).into(thirdPrizeIV);

                                String[] date = start_date_time.split(" ");
                                startOnTV.setText(date[0]);
                                startAtTV.setText(date[1]);

                            } else {
                                Constants.MESSAGE = jobj.getString("message");
                                Constants.toastDialog(HuntDetailsActivity.this, Constants.MESSAGE);

                            }
                            Constants.dismissProgressDialog(HuntDetailsActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Constants.toastDialog(HuntDetailsActivity.this, "Please Check Internet Connection..!!!");
//                Toast.makeText(HuntDetailsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("hunt_id", hunt_id);
                params.put("your_user_id", String.valueOf(user_id));


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.takeChallengeBTN:
                if (is_started.equalsIgnoreCase("Yes")) {
                    Intent intent = new Intent(HuntDetailsActivity.this, TakeChallengeActivity.class);
                    intent.putExtra("ENDDATE", end_date_time);
                    intent.putExtra("HUNTNAME", hunt_titleTV.getText().toString().trim());
                    intent.putExtra("HUNTID", HuntDetailsActivity.this.hunt_id);
                    startActivity(intent);
                } else {
                    if (CheckConnectivity.checkInternetConnection(this))
                        huntStartWebservice(HuntDetailsActivity.this.hunt_id);
                }


                break;
        }
    }

//    public void onDrawerClick(View v) {
//        Constants.drawerClick(this, v.getId());
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MyHunts_Activity.class);
        intent.putExtra("ComeFrom", "");
        startActivity(intent);
        finish();
    }


    public void huntStartWebservice(final String hunt_id) {

        Constants.progressDialog = new ProgressDialog(this);
        Constants.progressDialog.setMessage("Please wait");
        Constants.progressDialog.show();
        Constants.progressDialog.setCancelable(false);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.takeChallenge,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            Constants.MESSAGE_CODE = jobj.getInt("message_code");
                            Constants.MESSAGE = jobj.getString("message");

                            if (Constants.MESSAGE_CODE == 1) {
                                Intent intent = new Intent(HuntDetailsActivity.this, TakeChallengeActivity.class);
                                intent.putExtra("ENDDATE", end_date_time);
                                intent.putExtra("HUNTNAME", hunt_titleTV.getText().toString().trim());
                                intent.putExtra("HUNTID", HuntDetailsActivity.this.hunt_id);
                                startActivity(intent);
                            } else {

                            }
                            Constants.dismissProgressDialog(HuntDetailsActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(com.android.volley.error.VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(HuntDetailsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("hunt_id", hunt_id);
                params.put("user_id", String.valueOf(user_id));


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
